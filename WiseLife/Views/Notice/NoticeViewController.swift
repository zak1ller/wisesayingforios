//
//  NoticeViewController.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2021/01/25.
//

import Foundation
import UIKit

class NotiveViewController: UIViewController {
    let topView = TopView()
    let tableView = UITableView()
    
    var list: [Like] = []
    var page = 0
    var isLastPage = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
        updateList(isReload: false)
    }
    
    private func updateList(isReload: Bool) {
        if isReload {
            list.removeAll()
            page = 0
            isLastPage = true
        }
        
        DispatchQueue.global().async(execute: {
            Like.getLikes(page: self.page, result: {
                error, list, isLastPage  in
                self.isLastPage = isLastPage
                
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                } else if let list = list {
                    self.list.append(contentsOf: list)
                    DispatchQueue.main.async(execute: {
                        self.tableView.reloadData()
                    })
                }
            })
        })
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        
        topView.show(toView: view)
        topView.settitle(title: "Notice")
        
        let tableViewSet = {
            self.view.addSubview(self.tableView)
            self.tableView.translatesAutoresizingMaskIntoConstraints = false
            self.tableView.register(NoticeCell.self, forCellReuseIdentifier: "NoticeCell")
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.backgroundColor = UIColor.clear
            self.tableView.separatorStyle = .none
            self.tableView.rowHeight = UITableView.automaticDimension
            
            let leading = NSLayoutConstraint(item: self.tableView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.tableView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.tableView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.tableView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        tableViewSet()
    }
}

extension NotiveViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoticeCell", for: indexPath) as! NoticeCell
        cell.tag = indexPath.row
        cell.setData(data: list[indexPath.row])
        return cell
    }
}
