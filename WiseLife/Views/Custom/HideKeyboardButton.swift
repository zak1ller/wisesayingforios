//
//  HideKeyboardButton.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/07.
//

import Foundation
import UIKit

class HideKeyboardButton: UIButton {
    private weak var vc: UIViewController?
    private let hideImageView = UIImageView()
    
    convenience init(vc: UIViewController) {
        self.init()
        self.vc = vc
        setLayout()
    }
    
    private func setLayout() {
        let selfSet = {
            self.translatesAutoresizingMaskIntoConstraints = false
            let height = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button2)
            NSLayoutConstraint.activate([height])
        }
        selfSet()
        
        let hideImageViewSet = {
            self.addSubview(self.hideImageView)
            self.hideImageView.translatesAutoresizingMaskIntoConstraints = false
            self.hideImageView.image = UIImage(named: "hide.png")
            self.hideImageView.setImageColor(color: Colors.shared.textSub)
            
            let width = NSLayoutConstraint(item: self.hideImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
            let height = NSLayoutConstraint(item: self.hideImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
            let centerX = NSLayoutConstraint(item: self.hideImageView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)
            let centerY = NSLayoutConstraint(item: self.hideImageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([width, height, centerX, centerY])
        }
        hideImageViewSet()
    }
}
