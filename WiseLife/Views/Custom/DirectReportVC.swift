//
//  DirectReportVC.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/12/16.
//

import Foundation
import UIKit

class DirectReportVC: UIViewController {
    var loadingView: LoadingView!
    let topView = TopView()
    let contentView = UIView()
    let textView = UITextView()
    let submitButton = UIButton(type: .system)
    
    var reportGroup: Report.ReportGroup!
    var reportContent: Report.ReportContent!
    var tuid: String?
    
    var contentViewBottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadingView = LoadingView(view: view)
        loadingView.setColor(color: Colors.shared.invisible)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIWindow.keyboardWillShowNotification,
            object: nil)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: UIWindow.keyboardWillHideNotification,
            object: nil)
        
        setLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        isSubmitButtonEnable(false)
        textView.becomeFirstResponder()
    }
    
    @objc private func keyboardWillShow(_ notificaiton: Notification) {
        if let keyboardFrame: NSValue = notificaiton.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardHeight = keyboardFrame.cgRectValue.height
            contentViewBottomConstraint.constant = -keyboardHeight
        }
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        contentViewBottomConstraint.constant = 0
    }
    
    @objc private func pressSubmitButton(sender: UIButton) {
        let v = textView.text.trimmingCharacters(in: .whitespacesAndNewlines)
        if v.count < SizesForLegnth.reportDirectMessageMinimum {
            let alert = UIAlertController(title: NSLocalizedString("ContentShortMessage", comment: ""), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        } else if v.count > SizesForLegnth.reportDiecctMessageMaximum {
            let alert = UIAlertController(title: NSLocalizedString("ContentLongMessage", comment: ""), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        } else {
            loadingView.start()
            
            DispatchQueue.global().async(execute: {
                Report.send(pid: nil, cid: nil, tuid: self.tuid, group: self.reportGroup, content: self.reportContent, directMessage: v, result: {
                    error in
                    self.loadingView.stop()
                    
                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        let alert = UIAlertController(title: NSLocalizedString("SubmittedSuccessfully", comment: ""), message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: {
                            (_) in
                            DispatchQueue.main.async(execute: {
                                self.dismiss(animated: true, completion: nil)
                            })
                        }))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    }
                })
            })
        }
    }
    
    private func isSubmitButtonEnable(_ isEnable: Bool) {
        if isEnable {
            submitButton.backgroundColor = Colors.shared.point
        } else {
            submitButton.backgroundColor = Colors.shared.invisible
        }
        submitButton.isEnabled = isEnable
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        
        topView.show(toView: view)
        topView.settitle(title: NSLocalizedString("Report", comment: ""))
        topView.showBackButton()
        topView.backButtonDelegate = self
        
        let contentViewSet = {
            self.view.addSubview(self.contentView)
            self.contentView.translatesAutoresizingMaskIntoConstraints = false
            
            let leading = NSLayoutConstraint(item: self.contentView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.contentView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.contentView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: 0)
            self.contentViewBottomConstraint = NSLayoutConstraint(item: self.contentView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top, self.contentViewBottomConstraint])
        }
        contentViewSet()
        
        let submitButtonSet = {
            self.contentView.addSubview(self.submitButton)
            self.submitButton.translatesAutoresizingMaskIntoConstraints = false
            self.submitButton.addTarget(self, action: #selector(self.pressSubmitButton(sender:)), for: .touchUpInside)
            self.submitButton.setTitle(NSLocalizedString("Submit", comment: ""), for: .normal)
            self.submitButton.setTitleColor(UIColor.white, for: .normal)
            self.submitButton.backgroundColor = Colors.shared.point
            self.submitButton.titleLabel?.font = Fonts.normal
            
            let height = NSLayoutConstraint(item: self.submitButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
            let leading = NSLayoutConstraint(item: self.submitButton, attribute: .leadingMargin, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.submitButton, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.submitButton, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([height, leading, trailing, bottom])
        }
        submitButtonSet()
        
        let textViewSet = {
            self.contentView.addSubview(self.textView)
            self.textView.translatesAutoresizingMaskIntoConstraints = false
            self.textView.delegate = self
            self.textView.font = Fonts.normal
            self.textView.textColor = Colors.shared.text
            self.textView.textContainerInset = UIEdgeInsets.zero
            self.textView.textContainer.lineFragmentPadding = 0
            self.textView.backgroundColor = UIColor.clear
            
            let leading = NSLayoutConstraint(item: self.textView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.textView, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            let top = NSLayoutConstraint(item: self.textView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: Sizes.marginTop)
            let bottom = NSLayoutConstraint(item: self.textView, attribute: .bottom, relatedBy: .equal, toItem: self.submitButton, attribute: .top, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        textViewSet()
    }
}

extension DirectReportVC: TopViewBackButtonDelegate {
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
}

extension DirectReportVC: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.count < SizesForLegnth.reportDirectMessageMinimum {
            isSubmitButtonEnable(false)
        } else if textView.text.count > SizesForLegnth.reportDiecctMessageMaximum {
            // remove text untile get to maxsimum size
            while true {
                if textView.text.count <= SizesForLegnth.reportDiecctMessageMaximum {
                    break
                } else {
                    textView.text.removeLast()
                }
            }
            isSubmitButtonEnable(true)
        } else {
            isSubmitButtonEnable(true)
        }
    }
}
