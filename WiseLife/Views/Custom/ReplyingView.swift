//
//  ReplyingView.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/12/01.
//

import Foundation
import UIKit

protocol ReplyingViewDelegate: class {
    func replyingViewPressCloseButton()
}

class ReplyingView: UIView {
    private let closeButtonImageView = UIImageView()
    private let closeButton = UIButton(type: .system)
    private let contentLabel = UILabel()
    
    weak var delegate: ReplyingViewDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initCommon()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initCommon()
    }

    convenience init(item: Post.Comments) {
        self.init()
    }
    
    private func initCommon() {
        translatesAutoresizingMaskIntoConstraints = false
        setLayout()
    }
    
    @objc private func pressCloseButton(sender: UIButton) {
        delegate?.replyingViewPressCloseButton()
    }
    
    func setData(mentioned: String) {
        contentLabel.text = "Replying to \(mentioned)"
    }

    private func setLayout() {
        backgroundColor = Colors.shared.background
        
        let closeButtonImageViewSet = {
            self.addSubview(self.closeButtonImageView)
            self.closeButtonImageView.translatesAutoresizingMaskIntoConstraints = false
            self.closeButtonImageView.image = UIImage(named: "close_bold.png")
            self.closeButtonImageView.setImageColor(color: Colors.shared.text)
            
            let width = NSLayoutConstraint(item: self.closeButtonImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 12)
            let height = NSLayoutConstraint(item: self.closeButtonImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 12)
            let trailing = NSLayoutConstraint(item: self.closeButtonImageView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            let centerY = NSLayoutConstraint(item: self.closeButtonImageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([width, height, trailing, centerY])
        }
        closeButtonImageViewSet()
        
        let closeButtonSet = {
            self.addSubview(self.closeButton)
            self.closeButton.translatesAutoresizingMaskIntoConstraints = false
            self.closeButton.addTarget(self, action: #selector(self.pressCloseButton(sender:)), for: .touchUpInside)
            
            let width = NSLayoutConstraint(item: self.closeButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
            let height = NSLayoutConstraint(item: self.closeButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
            let centerX = NSLayoutConstraint(item: self.closeButton, attribute: .centerX, relatedBy: .equal, toItem: self.closeButtonImageView, attribute: .centerX, multiplier: 1, constant: 0)
            let centerY = NSLayoutConstraint(item: self.closeButton, attribute: .centerY, relatedBy: .equal, toItem: self.closeButtonImageView, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([width, height, centerX, centerY])
        }
        closeButtonSet()
        
        let contentLabelSet = {
            self.addSubview(self.contentLabel)
            self.contentLabel.translatesAutoresizingMaskIntoConstraints = false
            self.contentLabel.textColor = Colors.shared.invisible
            self.contentLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
            
            let leading = NSLayoutConstraint(item: self.contentLabel, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.contentLabel, attribute: .trailing, relatedBy: .equal, toItem: self.closeButtonImageView, attribute: .leading, multiplier: 1, constant: -Sizes.margin1)
            let top = NSLayoutConstraint(item: self.contentLabel, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: Sizes.margin1)
            let bottom = NSLayoutConstraint(item: self.contentLabel, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        contentLabelSet()
    }
}
