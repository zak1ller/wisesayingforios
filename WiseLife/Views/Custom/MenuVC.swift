//
//  MenuView.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/19.
//

import Foundation
import UIKit

protocol MenuVcDelegate: class  {
    func menuVcDidPress(id: String, i: Int)
}

class MenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource, MenuListCellDelegate {
    private let contentView = UIView()
    private let tableView = UITableView()
    private let menuDashImageView = UIImageView()
    
    weak var delegate: MenuVcDelegate?
    private var list: [MenuItem] = []
    
    struct MenuItem {
        var index = 0
        var id = ""
        var title = ""
        var image: UIImage?
    }
    
    deinit {
        print("Menu VC Deinited.")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
        contentView.transform = CGAffineTransform(translationX: 0, y: 512)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.3, animations: {
            self.contentView.transform = CGAffineTransform.identity
        })
    }
   
    @objc private func pressBackground() {
        UIView.animate(withDuration: 0.3, animations: {
            self.contentView.transform = CGAffineTransform(translationX: 0, y: 512)
        }, completion: {
            (_) in
            self.dismiss(animated: false, completion: nil)
        })
    }
    
    @objc private func pressContentView() {
        print("contentview")
    }
    
    func setList(list: [MenuItem]) {
        self.list = list
    }
    
    func menuListCellDidSelect(i: Int) {
        UIView.animate(withDuration: 0.33, animations: {
            self.contentView.transform = CGAffineTransform(translationX: 0, y: 512)
        }, completion: {
            (_) in
            self.dismiss(animated: false, completion: {
                self.delegate?.menuVcDidPress(id: self.list[i].id, i: self.list[i].index)
            })
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuListCell", for: indexPath) as! MenuListCell
        cell.tag = indexPath.row
        cell.delegate = self
        cell.setItem(item: list[indexPath.row])
        return cell
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundAlpha
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(pressBackground)))
        
        let contentViewSet = {
            self.view.addSubview(self.contentView)
            self.contentView.translatesAutoresizingMaskIntoConstraints = false
            self.contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.pressContentView)))
            self.contentView.backgroundColor = Colors.shared.backgroundSub
            self.contentView.layer.cornerRadius = 8
            
            let height = NSLayoutConstraint(item: self.contentView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 512)
            let leading = NSLayoutConstraint(item: self.contentView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.contentView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.contentView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 20)
            NSLayoutConstraint.activate([height, leading, trailing, bottom])
        }
        contentViewSet()
        
        let menuDashImageViewSet = {
            self.contentView.addSubview(self.menuDashImageView)
            self.menuDashImageView.translatesAutoresizingMaskIntoConstraints = false
            self.menuDashImageView.image = UIImage(named: "menu_dash.png")
            self.menuDashImageView.setImageColor(color: Colors.shared.invisible)
            
            let width = NSLayoutConstraint(item: self.menuDashImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button2)
            let height = NSLayoutConstraint(item: self.menuDashImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button2)
            let top = NSLayoutConstraint(item: self.menuDashImageView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: -5)
            let centerX = NSLayoutConstraint(item: self.menuDashImageView, attribute: .centerX, relatedBy: .equal, toItem: self.contentView, attribute: .centerX, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([width, height, top, centerX])
        }
        menuDashImageViewSet()
        
        let tableViewSet = {
            self.contentView.addSubview(self.tableView)
            self.tableView.translatesAutoresizingMaskIntoConstraints = false
            self.tableView.register(MenuListCell.self, forCellReuseIdentifier: "MenuListCell")
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.rowHeight = UITableView.automaticDimension
            self.tableView.separatorStyle = .none
            self.tableView.backgroundColor = UIColor.clear
            
            let leading = NSLayoutConstraint(item: self.tableView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.tableView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.tableView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: Sizes.marginTop-Sizes.margin1*0.5)
            let bottom = NSLayoutConstraint(item: self.tableView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        tableViewSet()
    }
}
