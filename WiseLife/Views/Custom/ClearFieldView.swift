//
//  ClearFieldView.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/07.
//

import Foundation
import UIKit

protocol ClearFieldViewDidChangeDelegate: class {
    func clearFieldViewDidChange(textField: UITextField)
}

class ClearFieldView: UIView, UITextFieldDelegate {
    private let underline = UIView()
    private let fieldStackView = UIStackView()
    private let fieldInputField = UITextField()
    
    private weak var vc: UIViewController?
    weak var didChangeDelegate: ClearFieldViewDidChangeDelegate?
    
    private var isActive = false
    
    convenience init(vc: UIViewController) {
        self.init()
        self.vc = vc
        
        setLayout()
    }
    
    @objc func pressView() {
        fieldInputField.becomeFirstResponder()
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        didChangeDelegate?.clearFieldViewDidChange(textField: textField)
    }
    
    func onBecome() {
        fieldInputField.becomeFirstResponder()
    }
    
    func getText() -> String {
        return fieldInputField.text ?? ""
    }
    
    func setPlaceHoder(text: String?) {
        fieldInputField.placeholder = text
    }
    
    func setText(text: String?) {
        fieldInputField.text = text
    }
    
    func setId(id: Int) {
        fieldInputField.tag = id
    }
    
    func setReturnType(returnType: UIReturnKeyType) {
        fieldInputField.returnKeyType = returnType
    }
    
    func onPasswordMode() {
        fieldInputField.isSecureTextEntry = true
    }
    
    func setKeyboardType(keyboardType: UIKeyboardType) {
        fieldInputField.keyboardType = keyboardType
    }
    
    func setTextFieldDelegate(delegate: UITextFieldDelegate) {
        fieldInputField.delegate = delegate
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        isActive = true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        isActive = false
    }
    
    private func setLayout() {
        let selfSet = {
            self.translatesAutoresizingMaskIntoConstraints = false
            self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.pressView)))
            let height = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
            NSLayoutConstraint.activate([height])
        }
        selfSet()
        
        let fieldStackViewSet = {
            self.addSubview(self.fieldStackView)
            self.fieldStackView.translatesAutoresizingMaskIntoConstraints = false
            self.fieldStackView.addArrangedSubview(self.fieldInputField)
            self.fieldStackView.axis = .horizontal
            self.fieldStackView.spacing = 10
            
            let leading = NSLayoutConstraint(item: self.fieldStackView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.fieldStackView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
            let centerY = NSLayoutConstraint(item: self.fieldStackView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, centerY])
        }
        fieldStackViewSet()
        
        let fieldInputFieldSet = {
            self.fieldInputField.delegate = self
            self.fieldInputField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
            self.fieldInputField.textColor = Colors.shared.text
            self.fieldInputField.font = Fonts.normal
            self.fieldInputField.clearButtonMode = .whileEditing
        }
        fieldInputFieldSet()
        
        let underLineSet = {
            self.addSubview(self.underline)
            self.underline.translatesAutoresizingMaskIntoConstraints = false
            self.underline.backgroundColor = Colors.shared.line
            
            let height = NSLayoutConstraint(item: self.underline, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 1)
            let leading = NSLayoutConstraint(item: self.underline, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.underline, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.underline, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([height, leading, trailing, bottom])
        }
        underLineSet()
    }
}
