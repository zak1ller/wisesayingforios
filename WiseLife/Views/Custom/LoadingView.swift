//
//  LoadingView.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/09.
//

import Foundation
import UIKit
import NVActivityIndicatorView

class LoadingView {
    enum style {
        case normal, search
    }
    
    private var loadingView = NVActivityIndicatorView(frame: CGRect.zero)
    private var backView = UIView()
    private var view: UIView

    private var size: CGFloat?

    init(view: UIView) {
        self.view = view
        backView.translatesAutoresizingMaskIntoConstraints = false
        loadingView.translatesAutoresizingMaskIntoConstraints = false
    }

    func setColor(color: UIColor) {
        loadingView.color = color
    }

    func setSize(size: CGFloat) {
        self.size = size
    }

    func start(style: style = .normal) {
        DispatchQueue.main.async(execute: {
            self.view.addSubview(self.backView)
            self.backView.translatesAutoresizingMaskIntoConstraints = false

            let top = NSLayoutConstraint(item: self.backView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.backView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.backView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.backView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([top, bottom, leading, trailing])

            self.backView.addSubview(self.loadingView)

            let width: NSLayoutConstraint
            let height: NSLayoutConstraint

            if let size = self.size  {
                width = NSLayoutConstraint(item: self.loadingView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: size)
                height = NSLayoutConstraint(item: self.loadingView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: size)
            } else{
                width = NSLayoutConstraint(item: self.loadingView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 57)
                height = NSLayoutConstraint(item: self.loadingView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 57)
            }

            let centerX = NSLayoutConstraint(item: self.loadingView, attribute: .centerX, relatedBy: .equal, toItem: self.backView, attribute: .centerX, multiplier: 1, constant: 0)
            let centerY = NSLayoutConstraint(item: self.loadingView, attribute: .centerY, relatedBy: .equal, toItem: self.backView, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([width, height, centerX, centerY])

            self.loadingView.startAnimating()
        })
    }

    func stop() {
        DispatchQueue.main.async(execute: {
            self.backView.removeFromSuperview()
            self.loadingView.stopAnimating()
        })
    }
}
