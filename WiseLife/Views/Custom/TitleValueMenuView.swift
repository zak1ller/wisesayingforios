//
//  TitleValueMenuView.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/12/21.
//

import Foundation
import UIKit

class TitleValueMenuView: UIView {
    var stackView = UIStackView()
    var titleLabel = UILabel()
    var valueLabel = UILabel()
    
    var item: Item!
  
    struct Item {
        var title = ""
        var value: String?
    }
    
    convenience init(item: Item) {
        self.init()
        self.item = item
        
        setLayout()
        ViewManager.appendUnderLine(toView: self, backgroundView: self, size: .normal, color: Colors.shared.border, leading: 100+Sizes.margin1)
        updateItem()
    }
    
    func updateItem() {
        titleLabel.text = item.title
        valueLabel.text = item.value
    }
    
    func setValue(value: String?) {
        item.value = value
        valueLabel.text = value
    }
    
    private func setLayout() {
        let stackViewSet = {
            self.addSubview(self.stackView)
            self.stackView.translatesAutoresizingMaskIntoConstraints = false
            self.stackView.addArrangedSubview(self.titleLabel)
            self.stackView.addArrangedSubview(self.valueLabel)
            self.stackView.axis = .horizontal
            self.stackView.distribution = .fill
            
            let leading = NSLayoutConstraint(item: self.stackView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.stackView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            let top = NSLayoutConstraint(item: self.stackView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: Sizes.margin1)
            let bottom = NSLayoutConstraint(item: self.stackView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        stackViewSet()
        
        let titleLabelSet = {
            self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
            self.titleLabel.textColor = Colors.shared.text
            self.titleLabel.font = Fonts.normal
            
            let width = NSLayoutConstraint(item: self.titleLabel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 100)
            NSLayoutConstraint.activate([width])
        }
        titleLabelSet()
        
        let valueLabelSet = {
            self.valueLabel.textColor = Colors.shared.text
            self.valueLabel.font = Fonts.normal
            self.valueLabel.numberOfLines = 5
        }
        valueLabelSet()
    }
}
