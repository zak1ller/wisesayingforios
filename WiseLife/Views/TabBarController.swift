//
//  TabBarController.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/10.
//

import Foundation
import UIKit

protocol UpdateSignInStateDelegate: class {
    func updateSignInCheckSignIn()
}
    
class TabBarController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initilize view controller of Colors
        Colors.shared.setVc(vc: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tabBar.barTintColor = Colors.shared.backgroundSub
        tabBar.backgroundColor = Colors.shared.backgroundSub
        tabBar.tintColor = Colors.shared.text
        tabBar.unselectedItemTintColor = Colors.shared.text
    }
}
