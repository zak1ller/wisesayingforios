//
//  PlaygroundListCell.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/12/31.
//

import Foundation
import UIKit

protocol PlaygroundListCellDelegate: class {
    func playgroundListCellDoubleTab(i: Int)
    func playgroundListCellPressUserPageButton(i: Int)
    func playgroundListCellPressDetailButton(i: Int)
    func playgroundListCellPressLikeButton(i: Int)
    func playgroundListCellLongPress(i: Int)
}

class PlaygroundListCell: UITableViewCell {
    let userImageView = UIImageView()
    let usernameLabel = UILabel()
    let userPageButton = UIButton(type: .system)
    let dateLabel = UILabel()
    let contentStackView = UIStackView()
    let contentLabel = UILabel()
    let bottomStackView = UIStackView()
    let detailLabel = UILabel()
    let detailButton = UIButton(type: .system)
    let likeView = UIView()
    let likeImageView = UIImageView()
    let likeLabel = UILabel()
    let likeButton = UIButton(type: .system)
    
    weak var delegate: PlaygroundListCellDelegate?
        
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.backgroundColor = Colors.shared.backgroundSub
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTab))
        tap.numberOfTapsRequired = 2
        contentView.addGestureRecognizer(tap)
        contentView.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(longPresss)))
        
        let userImageViewSet = {
            self.contentView.addSubview(self.userImageView)
            self.userImageView.translatesAutoresizingMaskIntoConstraints = false
            self.userImageView.layer.cornerRadius = 15
            self.userImageView.clipsToBounds = true
            
            let width = NSLayoutConstraint(item: self.userImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
            let height = NSLayoutConstraint(item: self.userImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
            let leading = NSLayoutConstraint(item: self.userImageView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let top = NSLayoutConstraint(item: self.userImageView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: Sizes.margin1)
            NSLayoutConstraint.activate([width, height, leading, top])
        }
        userImageViewSet()
        
        let usernameLabelSet = {
            self.contentView.addSubview(self.usernameLabel)
            self.usernameLabel.translatesAutoresizingMaskIntoConstraints = false
            self.usernameLabel.textColor = Colors.shared.text
            self.usernameLabel.font = UIFont.systemFont(ofSize: 14, weight: .bold)
            
            let leading = NSLayoutConstraint(item: self.usernameLabel, attribute: .leading, relatedBy: .equal, toItem: self.userImageView, attribute: .trailing, multiplier: 1, constant: 8)
            let top = NSLayoutConstraint(item: self.usernameLabel, attribute: .top, relatedBy: .equal, toItem: self.userImageView, attribute: .top, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, top])
        }
        usernameLabelSet()
        
        let dateLabelSet = {
            self.contentView.addSubview(self.dateLabel)
            self.dateLabel.translatesAutoresizingMaskIntoConstraints = false
            self.dateLabel.textColor = Colors.shared.textSub
            self.dateLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
            
            let leading = NSLayoutConstraint(item: self.dateLabel, attribute: .leading, relatedBy: .equal, toItem: self.usernameLabel, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.dateLabel, attribute: .top, relatedBy: .equal, toItem: self.userImageView, attribute: .top, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, top])
        }
        dateLabelSet()
        
        let userPageButtonSet = {
            self.contentView.addSubview(self.userPageButton)
            self.userPageButton.translatesAutoresizingMaskIntoConstraints = false
            self.userPageButton.addTarget(self, action: #selector(self.pressUserPageButton(sender:)), for: .touchUpInside)
            self.userPageButton.layer.cornerRadius = 4
            
            let leading = NSLayoutConstraint(item: self.userPageButton, attribute: .leading, relatedBy: .equal, toItem: self.userImageView, attribute: .trailing, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.userPageButton, attribute: .trailing, relatedBy: .equal, toItem: self.dateLabel, attribute: .leading, multiplier: 1, constant: 8)
            let top = NSLayoutConstraint(item: self.userPageButton, attribute: .top, relatedBy: .equal, toItem: self.userImageView, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.userPageButton, attribute: .bottom, relatedBy: .equal, toItem: self.userImageView, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        userPageButtonSet()
        
        let contentStackViewSet = {
            self.contentView.addSubview(self.contentStackView)
            self.contentStackView.translatesAutoresizingMaskIntoConstraints = false
            self.contentStackView.addArrangedSubview(self.contentLabel)
            self.contentStackView.addArrangedSubview(self.detailLabel)
            self.contentStackView.addArrangedSubview(self.likeView)
            self.contentStackView.axis = .vertical
            self.contentStackView.distribution = .fill
            self.contentStackView.spacing = Sizes.margin1*0.5
            
            let leading = NSLayoutConstraint(item: self.contentStackView, attribute: .leading, relatedBy: .equal, toItem: self.userImageView, attribute: .trailing, multiplier: 1, constant: 8)
            let trailing = NSLayoutConstraint(item: self.contentStackView, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            let top = NSLayoutConstraint(item: self.contentStackView, attribute: .top, relatedBy: .equal, toItem: self.usernameLabel, attribute: .bottom, multiplier: 1, constant: 4)
            let bottom = NSLayoutConstraint(item: self.contentStackView, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1*0.5)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        contentStackViewSet()
        
        let contentLabelSet = {
            self.contentLabel.textColor = Colors.shared.text
            self.contentLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        }
        contentLabelSet()
        
        let detailLabelSet = {
            self.detailLabel.text = NSLocalizedString("Details", comment: "")
            self.detailLabel.textColor = Colors.shared.pointSub
            self.detailLabel.font = UIFont.systemFont(ofSize: 12, weight: .medium)
        }
        detailLabelSet()
        
        let detailButtonSet = {
            self.contentView.addSubview(self.detailButton)
            self.detailButton.translatesAutoresizingMaskIntoConstraints = false
            self.detailButton.addTarget(self, action: #selector(self.pressDetailButton(sender:)), for: .touchUpInside)
            self.detailButton.layer.cornerRadius = 4
            
            let width = NSLayoutConstraint(item: self.detailButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 60)
            let top = NSLayoutConstraint(item: self.detailButton, attribute: .top, relatedBy: .equal, toItem: self.detailLabel, attribute: .top, multiplier: 1, constant: -10)
            let bottom = NSLayoutConstraint(item: self.detailButton, attribute: .bottom, relatedBy: .equal, toItem: self.detailLabel, attribute: .bottom, multiplier: 1, constant: 10)
            let leading = NSLayoutConstraint(item: self.detailButton, attribute: .leading, relatedBy: .equal, toItem: self.detailLabel, attribute: .leading, multiplier: 1, constant: -10)
            NSLayoutConstraint.activate([width, top, bottom, leading])
        }
        detailButtonSet()
        
        let likeImageViewSet = {
            self.likeView.addSubview(self.likeImageView)
            self.likeImageView.translatesAutoresizingMaskIntoConstraints = false
            self.likeImageView.image = UIImage(named: "like_empty.png")
            self.likeImageView.setImageColor(color: Colors.shared.invisible)
          
            let width = NSLayoutConstraint(item: self.likeImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15)
            let height = NSLayoutConstraint(item: self.likeImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15)
            let leading = NSLayoutConstraint(item: self.likeImageView, attribute: .leading, relatedBy: .equal, toItem: self.likeView, attribute: .leading, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.likeImageView, attribute: .top, relatedBy: .equal, toItem: self.likeView, attribute: .top, multiplier: 1, constant: 4)
            let bottom = NSLayoutConstraint(item: self.likeImageView, attribute: .bottom, relatedBy: .equal, toItem: self.likeView, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([width, height, leading, top, bottom])
        }
        likeImageViewSet()
        
        let likeLabelSet = {
            self.likeView.addSubview(self.likeLabel)
            self.likeLabel.translatesAutoresizingMaskIntoConstraints = false
            self.likeLabel.textColor = Colors.shared.text
            self.likeLabel.font = UIFont.systemFont(ofSize: 12, weight: .medium)
            
            let leading = NSLayoutConstraint(item: self.likeLabel, attribute: .leading, relatedBy: .equal, toItem: self.likeImageView, attribute: .trailing, multiplier: 1, constant: 4)
            let centerY = NSLayoutConstraint(item: self.likeLabel, attribute: .centerY, relatedBy: .equal, toItem: self.likeImageView, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, centerY])
        }
        likeLabelSet()
        
        let likeButtonSet = {
            self.contentView.addSubview(self.likeButton)
            self.likeButton.translatesAutoresizingMaskIntoConstraints = false
            self.likeButton.addTarget(self, action: #selector(self.pressLikeButton(sender:)), for: .touchUpInside)
            self.likeButton.layer.cornerRadius = 4
            
            let leading = NSLayoutConstraint(item: self.likeButton, attribute: .leadingMargin, relatedBy: .equal, toItem: self.likeImageView, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.likeButton, attribute: .trailing, relatedBy: .equal, toItem: self.likeLabel, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.likeButton, attribute: .top, relatedBy: .equal, toItem: self.likeView, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.likeButton, attribute: .bottom, relatedBy: .equal, toItem: self.likeView, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        likeButtonSet()
    }
    
    @objc private func doubleTab() {
        SoundManager.vibrate(intensity: .medium)
        delegate?.playgroundListCellDoubleTab(i: tag)
    }
    
    @objc private func pressUserPageButton(sender: UIButton) {
        delegate?.playgroundListCellPressUserPageButton(i: tag)
    }
    
    @objc private func pressDetailButton(sender: UIButton) {
        delegate?.playgroundListCellPressDetailButton(i: tag)
    }
    
    @objc private func pressLikeButton(sender: UIButton) {
        delegate?.playgroundListCellPressLikeButton(i: tag)
    }
    
    @objc private func longPresss() {
        SoundManager.vibrate(intensity: .medium)
        delegate?.playgroundListCellLongPress(i: tag)
    }
    
    func setData(data: PlaygroundPost) {
        usernameLabel.text = data.uname
        contentLabel.numberOfLines = 0
        contentLabel.text = data.content
        
        detailLabel.isHidden = true
        detailButton.isHidden = true
        
        if !data.isShowAllText {
            if contentLabel.getMaxLines() > 2 {
                contentLabel.numberOfLines = 2
                detailLabel.isHidden = false
                detailButton.isHidden = false
                detailLabel.text = NSLocalizedString("Details", comment: "")
            }
        } else {
            contentLabel.numberOfLines = 0
            detailLabel.isHidden = false
            detailButton.isHidden = false
            detailLabel.text = NSLocalizedString("Simply", comment: "")
        }
        
        if data.isLike {
            likeImageView.image = UIImage(named: "like_fill.png")
            likeImageView.setImageColor(color: Colors.shared.text)
        } else {
            likeImageView.image = UIImage(named: "like_empty.png")
            likeImageView.setImageColor(color: Colors.shared.invisible)
        }
        
        dateLabel.text = " • "+TimeManager.makePastTimeString(str: data.date)
        likeLabel.text = "\(data.likesCount)"
        
        DispatchQueue.global().async(execute: {
            UserProfileImage.downloadProfileImage(uid: data.uid, result: {
                error, image in
                if let error = error {
                    print(error)
                }
                
                if let image = image {
                    DispatchQueue.main.async(execute: {
                        self.userImageView.image = image
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        self.userImageView.image = UIImage(named: "user.png")
                        self.userImageView.setImageColor(color: Colors.shared.invisible)
                    })
                }
            })
        })
    }
}
