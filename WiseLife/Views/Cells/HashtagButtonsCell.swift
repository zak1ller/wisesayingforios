//
//  HashtagButtonsCell.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/12.
//

import Foundation
import UIKit

public protocol CollectionCellAutoLayout: class {
    var cachedSize: CGSize? { get set }
}

public extension CollectionCellAutoLayout where Self: UICollectionViewCell {
    func preferredLayoutAttributes(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        setNeedsLayout()
        layoutIfNeeded()
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var newFrame = layoutAttributes.frame
        newFrame.size.width = CGFloat(ceilf(Float(size.width)))
        layoutAttributes.frame = newFrame
        cachedSize = newFrame.size
        return layoutAttributes
    }
}

class CustomViewFlowLayout: UICollectionViewFlowLayout {
    let cellSpacing: CGFloat = 10
 
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        self.minimumLineSpacing = 10.0
        self.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0.0, right: 0)
        let attributes = super.layoutAttributesForElements(in: rect)
 
        var leftMargin = sectionInset.left
        var maxY: CGFloat = -1.0
        attributes?.forEach { layoutAttribute in
            if layoutAttribute.frame.origin.y >= maxY {
                leftMargin = sectionInset.left
            }
            layoutAttribute.frame.origin.x = leftMargin
            leftMargin += layoutAttribute.frame.width + cellSpacing
            maxY = max(layoutAttribute.frame.maxY, maxY)
        }
        return attributes
    }
}

class HashtagButotnsCell: UICollectionViewCell {
    private let hashtagButton = UIButton(type: .system)
    private let stackView = UIStackView()
    private let keywordLabel = UILabel()
    private let deleteImageView = UIImageView()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
        backgroundColor = Colors.shared.background
        
        let hashtagButtonSet = {
            self.addSubview(self.hashtagButton)
            self.hashtagButton.translatesAutoresizingMaskIntoConstraints = false
            self.hashtagButton.layer.cornerRadius = 4
            self.hashtagButton.backgroundColor = Colors.shared.backgroundSub
            
            let leading = NSLayoutConstraint(item: self.hashtagButton, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.hashtagButton, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.hashtagButton, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.hashtagButton, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        hashtagButtonSet()
        
        let stackViewSet = {
            self.hashtagButton.addSubview(self.stackView)
            self.stackView.translatesAutoresizingMaskIntoConstraints = false
            self.stackView.addArrangedSubview(self.keywordLabel)
            self.stackView.addArrangedSubview(self.deleteImageView)
            self.stackView.axis = .horizontal
            self.stackView.distribution = .fill
            self.stackView.spacing = 4
            
            let leading = NSLayoutConstraint(item: self.stackView, attribute: .leading, relatedBy: .equal, toItem: self.hashtagButton, attribute: .leading, multiplier: 1, constant: 12)
            let trailing = NSLayoutConstraint(item: self.stackView, attribute: .trailing, relatedBy: .equal, toItem: self.hashtagButton, attribute: .trailing, multiplier: 1, constant: -8)
            let top = NSLayoutConstraint(item: self.stackView, attribute: .top, relatedBy: .equal, toItem: self.hashtagButton, attribute: .top, multiplier: 1, constant: 8)
            let bottom = NSLayoutConstraint(item: self.stackView, attribute: .bottom, relatedBy: .equal, toItem: self.hashtagButton, attribute: .bottom, multiplier: 1, constant: -8)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        stackViewSet()
        
        let keywordLabelSet = {
            self.keywordLabel.textColor = Colors.shared.text
            self.keywordLabel.font = Fonts.normal
        }
        keywordLabelSet()
        
        let deleteImageViewSet = {
            self.deleteImageView.translatesAutoresizingMaskIntoConstraints = false
            self.deleteImageView.image = UIImage(named: "remove.png")
            self.deleteImageView.setImageColor(color: Colors.shared.textSub)
            
            let width = NSLayoutConstraint(item: self.deleteImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15)
            let height = NSLayoutConstraint(item: self.deleteImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15)
            NSLayoutConstraint.activate([width, height])
        }
        deleteImageViewSet()
    }
    
    func setKeyword(keyword: String) {
        keywordLabel.text = keyword
    }
}
