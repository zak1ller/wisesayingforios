//
//  MenuOneLineListCell.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/20.
//

import Foundation
import UIKit

class MenuOneLineListCell: UITableViewCell {
    private let titleLabel = UILabel()
    private let rightArrowImageView = UIImageView()
    
    struct MenuOneLineItem {
        var id = ""
        var title = ""
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.backgroundColor = Colors.shared.backgroundSub
        
        let rightArrowImageViewSet = {
            self.contentView.addSubview(self.rightArrowImageView)
            self.rightArrowImageView.translatesAutoresizingMaskIntoConstraints = false
            self.rightArrowImageView.image = UIImage(named: "arrow_right_bold.png")
            self.rightArrowImageView.setImageColor(color: Colors.shared.invisible)
            
            let width = NSLayoutConstraint(item: self.rightArrowImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15)
            let height = NSLayoutConstraint(item: self.rightArrowImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15)
            let trailing = NSLayoutConstraint(item: self.rightArrowImageView, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            let top = NSLayoutConstraint(item: self.rightArrowImageView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: Sizes.margin1)
            let bottom = NSLayoutConstraint(item: self.rightArrowImageView, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([width, height, trailing, top, bottom])
        }
        rightArrowImageViewSet()
        
        let titleLabelSet = {
            self.contentView.addSubview(self.titleLabel)
            self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
            self.titleLabel.textColor = Colors.shared.text
            self.titleLabel.font = Fonts.normal
            
            let leading = NSLayoutConstraint(item: self.titleLabel, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.titleLabel, attribute: .trailing, relatedBy: .equal, toItem: self.rightArrowImageView, attribute: .leading, multiplier: 1, constant: -Sizes.margin1)
            let centerY = NSLayoutConstraint(item: self.titleLabel, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, centerY])
        }
        titleLabelSet()
    }
    
    func setItem(item: MenuOneLineItem) {
        titleLabel.text = item.title
    }
}
