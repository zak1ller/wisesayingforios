//
//  MyCollectionListCell.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/15.
//

import Foundation
import UIKit

protocol MyCollectionListCellDelegate: class {
    func myCollectionListCellDidPressMenuButton(i: Int)
    func myCollectionListCellDidPressLeftButton(action: MyCollectionListCell.ButtonAction)
    func myCollectionListCellDidPressRightButotn(action: MyCollectionListCell.ButtonAction)
    func myCollectionListCellDidPressFollowers(followers: [User.Follow])
    func myCollectionListCellDidPressFollowings(folowings: [User.Follow])
}

class MyCollectionListCell: UITableViewCell {
    private let contentStackView = UIStackView()
    private let contentBackgroundView = UIView()
    private let contentUserImageView = UIImageView()
    private let contentUsernameLabel = UILabel()
    private let menuButton = UIButton(type: .system)
    private let contentLabel = UILabel()
    private let menuImageView = UIImageView()
    
    private let contentBottomStackView = UIStackView()
    private let hashtagLabel = UILabel()
    private let dateLabel = UILabel()
    
    private let userContentView = UIView()
    private let userImageView = UIImageView()
    
    private let userCountingStackView = UIStackView()
    private let userPostsStackView = UIStackView()
    private let userPostsCountLabel = UILabel()
    private let userPostsTitleLabel = UILabel()
    private let userFollowerStackView = UIStackView()
    private let userFollowerCountLabel = UILabel()
    private let userFollowerTitleLabel = UILabel()
    private let userFollowingStackView = UIStackView()
    private let userFollowingCountLabel = UILabel()
    private let userFollowingTitleLabel = UILabel()
    private let followersButton = UIButton(type: .system)
    private let followingsButton = UIButton(type: .system)
    
    private let userBottomStackView = UIStackView()
    private let userBioLabel = UILabel()
    private let userButtonStackView = UIStackView()
    private let userLeftButton = UIButton(type: .custom)
    private let userRightButton = UIButton(type: .custom)
    
    weak var delegate: MyCollectionListCellDelegate?
    var isUserContent = false
    var isBeBlocked = false
    var isIBlocked = false
    var isFollow = false
    var uid = ""
    var followers: [User.Follow] = []
    var followings: [User.Follow] = []
    
    enum ButtonAction {
        case editProfile
        case follow
        case unfollow
        case collections
        case beBlocked
        case iBlocked
    }
    
    enum CellMode {
        case user, list
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.backgroundColor = Colors.shared.backgroundSub
        
        let contentStackViewSet = {
            self.contentView.addSubview(self.contentStackView)
            self.contentStackView.translatesAutoresizingMaskIntoConstraints = false
            self.contentStackView.addArrangedSubview(self.userContentView)
            self.contentStackView.addArrangedSubview(self.contentBackgroundView)
            self.contentStackView.axis = .vertical
            self.contentStackView.distribution = .fill
            
            let leading = NSLayoutConstraint(item: self.contentStackView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.contentStackView, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            let top = NSLayoutConstraint(item: self.contentStackView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.contentStackView, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        contentStackViewSet()
                    
        let userImageViewSet = {
            self.userContentView.addSubview(self.userImageView)
            self.userImageView.translatesAutoresizingMaskIntoConstraints = false
            self.userImageView.image = UIImage(named: "user.png")
            self.userImageView.setImageColor(color: Colors.shared.invisible)
            self.userImageView.layer.cornerRadius = 86*0.5
            self.userImageView.layer.borderWidth = 1
            self.userImageView.layer.borderColor = Colors.shared.border.cgColor
            self.userImageView.clipsToBounds = true
            
            let width = NSLayoutConstraint(item: self.userImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 86)
            let height = NSLayoutConstraint(item: self.userImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 86)
            let leading = NSLayoutConstraint(item: self.userImageView, attribute: .leading, relatedBy: .equal, toItem: self.userContentView, attribute: .leading, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.userImageView, attribute: .top, relatedBy: .equal, toItem: self.userContentView, attribute: .top, multiplier: 1, constant: Sizes.margin1)
            NSLayoutConstraint.activate([width, height, leading, top])
        }
        userImageViewSet()
        
        let userCountingStackViewSet = {
            self.userContentView.addSubview(self.userCountingStackView)
            self.userCountingStackView.translatesAutoresizingMaskIntoConstraints = false
            self.userCountingStackView.addArrangedSubview(self.userPostsStackView)
            self.userCountingStackView.addArrangedSubview(self.userFollowerStackView)
            self.userCountingStackView.addArrangedSubview(self.userFollowingStackView)
            self.userCountingStackView.axis = .horizontal
            self.userCountingStackView.distribution = .fillEqually
            self.userCountingStackView.spacing = Sizes.margin1*2
            
            let leading = NSLayoutConstraint(item: self.userCountingStackView, attribute: .leading, relatedBy: .equal, toItem: self.userImageView, attribute: .trailing, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.userCountingStackView, attribute: .trailing, relatedBy: .equal, toItem: self.userContentView, attribute: .trailing, multiplier: 1, constant: 0)
            let centerY = NSLayoutConstraint(item: self.userCountingStackView, attribute: .centerY, relatedBy: .equal, toItem: self.userImageView, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, centerY])
        }
        userCountingStackViewSet()
        
        let userContentValueStackViewSet = {
            self.userFollowingStackView.addArrangedSubview(self.userFollowingCountLabel)
            self.userFollowingStackView.addArrangedSubview(self.userFollowingTitleLabel)
            self.userFollowingStackView.axis = .vertical
            self.userFollowingStackView.distribution = .fill
            self.userFollowingStackView.spacing = 0
        }
        userContentValueStackViewSet()
        
        let userContentInpluenceCountLabelSet = {
            self.userFollowingCountLabel.textColor = Colors.shared.text
            self.userFollowingCountLabel.font = Fonts.normal
            self.userFollowingCountLabel.text = "0"
            self.userFollowingCountLabel.textAlignment = .center
        }
        userContentInpluenceCountLabelSet()
        
        let userContentInpluenceTitleLabelSet = {
            self.userFollowingTitleLabel.textColor = Colors.shared.text
            self.userFollowingTitleLabel.font = UIFont.systemFont(ofSize: 12, weight: .medium)
            self.userFollowingTitleLabel.text = NSLocalizedString("Following", comment: "")
            self.userFollowingTitleLabel.textAlignment = .center
        }
        userContentInpluenceTitleLabelSet()
        
        let userFollowerStackViewSet = {
            self.userFollowerStackView.addArrangedSubview(self.userFollowerCountLabel)
            self.userFollowerStackView.addArrangedSubview(self.userFollowerTitleLabel)
            self.userFollowerStackView.axis = .vertical
            self.userFollowerStackView.distribution = .fill
            self.userFollowerStackView.spacing = 0
        }
        userFollowerStackViewSet()
        
        let userFollowerCountLabelSet = {
            self.userFollowerCountLabel.textColor = Colors.shared.text
            self.userFollowerCountLabel.font = Fonts.normal
            self.userFollowerCountLabel.text = "0"
            self.userFollowerCountLabel.textAlignment = .center
        }
        userFollowerCountLabelSet()
        
        let userFollowerTitleLabelSet = {
            self.userFollowerTitleLabel.textColor = Colors.shared.text
            self.userFollowerTitleLabel.font = UIFont.systemFont(ofSize: 12, weight: .medium)
            self.userFollowerTitleLabel.text = NSLocalizedString("Followers", comment: "")
            self.userFollowerTitleLabel.textAlignment = .center
        }
        userFollowerTitleLabelSet()
        
        let followerButtonSet = {
            self.userContentView.addSubview(self.followersButton)
            self.followersButton.translatesAutoresizingMaskIntoConstraints = false
            self.followersButton.addTarget(self, action: #selector(self.pressFollowersButton(sender:)), for: .touchUpInside)
            
            let leading = NSLayoutConstraint(item: self.followersButton, attribute: .leading, relatedBy: .equal, toItem: self.userFollowerStackView, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.followersButton, attribute: .trailing, relatedBy: .equal, toItem: self.userFollowerStackView, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.followersButton, attribute: .top, relatedBy: .equal, toItem: self.userFollowerStackView, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.followersButton, attribute: .bottom, relatedBy: .equal, toItem: self.userFollowerStackView, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        followerButtonSet()
        
        let followingsButtonSet = {
            self.userContentView.addSubview(self.followingsButton)
            self.followingsButton.translatesAutoresizingMaskIntoConstraints = false
            self.followingsButton.addTarget(self, action: #selector(self.pressFollowingsButton(sender:)), for: .touchUpInside)
            
            let leading = NSLayoutConstraint(item: self.followingsButton, attribute: .leading, relatedBy: .equal, toItem: self.userFollowingStackView, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.followingsButton, attribute: .trailing, relatedBy: .equal, toItem: self.userFollowingStackView, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.followingsButton, attribute: .top, relatedBy: .equal, toItem: self.userFollowingStackView, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.followingsButton, attribute: .bottom, relatedBy: .equal, toItem: self.userFollowingStackView, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        followingsButtonSet()
        
        let userPostsStackViewSet = {
            self.userPostsStackView.addArrangedSubview(self.userPostsCountLabel)
            self.userPostsStackView.addArrangedSubview(self.userPostsTitleLabel)
            self.userPostsStackView.axis = .vertical
            self.userPostsStackView.distribution = .fill
            self.userPostsStackView.spacing = 0
        }
        userPostsStackViewSet()
        
        let userPostsCountLabelSet = {
            self.userPostsCountLabel.textColor = Colors.shared.text
            self.userPostsCountLabel.font = Fonts.normal
            self.userPostsCountLabel.text = "0"
            self.userPostsCountLabel.textAlignment = .center
        }
        userPostsCountLabelSet()
        
        let userPostsTitleLabelSet = {
            self.userPostsTitleLabel.textColor = Colors.shared.text
            self.userPostsTitleLabel.font = UIFont.systemFont(ofSize: 12, weight: .medium)
            self.userPostsTitleLabel.text = NSLocalizedString("Posts", comment: "")
            self.userPostsTitleLabel.textAlignment = .center
        }
        userPostsTitleLabelSet()
        
        let userBottomStackViewSet = {
            self.userContentView.addSubview(self.userBottomStackView)
            self.userBottomStackView.translatesAutoresizingMaskIntoConstraints = false
            self.userBottomStackView.addArrangedSubview(self.userBioLabel)
            self.userBottomStackView.addArrangedSubview(self.userButtonStackView)
            self.userBottomStackView.axis = .vertical
            self.userBottomStackView.distribution = .fill
            self.userBottomStackView.spacing = Sizes.margin1
            
            let leading = NSLayoutConstraint(item: self.userBottomStackView, attribute: .leading, relatedBy: .equal, toItem: self.userContentView, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.userBottomStackView, attribute: .trailing, relatedBy: .equal, toItem: self.userContentView, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.userBottomStackView, attribute: .top, relatedBy: .equal, toItem: self.userImageView, attribute: .bottom, multiplier: 1, constant: Sizes.margin1)
            let bottom = NSLayoutConstraint(item: self.userBottomStackView, attribute: .bottom, relatedBy: .equal, toItem: self.userContentView, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        userBottomStackViewSet()
        
        let userBioLabelSet = {
            self.userBioLabel.textColor = Colors.shared.text
            self.userBioLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
            self.userBioLabel.numberOfLines = 5
        }
        userBioLabelSet()
        
        let userButtonStackViewSet = {
            self.userButtonStackView.translatesAutoresizingMaskIntoConstraints = false
            self.userButtonStackView.addArrangedSubview(self.userLeftButton)
            self.userButtonStackView.addArrangedSubview(self.userRightButton)
            self.userButtonStackView.axis = .horizontal
            self.userButtonStackView.distribution = .fillEqually
            self.userButtonStackView.spacing = Sizes.margin1*0.5
            
            let height = NSLayoutConstraint(item: self.userButtonStackView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 34)
            NSLayoutConstraint.activate([height])
        }
        userButtonStackViewSet()
        
        let userButtonSet = {
            self.userLeftButton.addTarget(self, action: #selector(self.pressLeftButton(sender:)), for: .touchUpInside)
            self.userLeftButton.layer.cornerRadius = 4
            self.userLeftButton.layer.borderColor = Colors.shared.border.cgColor
            self.userLeftButton.layer.borderWidth = 1.25
            self.userLeftButton.setTitleColor(Colors.shared.text, for: .normal)
            self.userLeftButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .bold)
            
            self.userRightButton.addTarget(self, action: #selector(self.pressRightButton(sender:)), for: .touchUpInside)
            self.userRightButton.layer.cornerRadius = 4
            self.userRightButton.layer.borderColor = Colors.shared.border.cgColor
            self.userRightButton.layer.borderWidth = 1.25
            self.userRightButton.setTitleColor(Colors.shared.text, for: .normal)
            self.userRightButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        }
        userButtonSet()
        
        let contentBackgroundViewSet = {
            self.contentBackgroundView.backgroundColor = Colors.shared.backgroundSub
        }
        contentBackgroundViewSet()
        
        let contentUserImageViewSet = {
            self.contentBackgroundView.addSubview(self.contentUserImageView)
            self.contentUserImageView.translatesAutoresizingMaskIntoConstraints = false
            self.contentUserImageView.clipsToBounds = true
            self.contentUserImageView.layer.cornerRadius = 28*0.5
            self.contentUserImageView.layer.borderColor = Colors.shared.border.cgColor
            self.contentUserImageView.layer.borderWidth = 0.75
            
            let width = NSLayoutConstraint(item: self.contentUserImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 28)
            let height = NSLayoutConstraint(item: self.contentUserImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 28)
            let leading = NSLayoutConstraint(item: self.contentUserImageView, attribute: .leading, relatedBy: .equal, toItem: self.contentBackgroundView, attribute: .leading, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.contentUserImageView, attribute: .top, relatedBy: .equal, toItem: self.contentBackgroundView, attribute: .top, multiplier: 1, constant: Sizes.margin1)
            NSLayoutConstraint.activate([width, height, leading, top])
        }
        contentUserImageViewSet()
        
        let menuButtonSet = {
            self.contentBackgroundView.addSubview(self.menuButton)
            self.menuButton.translatesAutoresizingMaskIntoConstraints = false
            self.menuButton.addTarget(self, action: #selector(self.pressMenuButton(sender:)), for: .touchUpInside)
            self.menuButton.setImage(UIImage(named: "menu.png"), for: .normal)
            self.menuButton.tintColor = Colors.shared.text
            
            let width = NSLayoutConstraint(item: self.menuButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 18)
            let height = NSLayoutConstraint(item: self.menuButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 18)
            let trailing = NSLayoutConstraint(item: self.menuButton, attribute: .trailing, relatedBy: .equal, toItem: self.contentBackgroundView, attribute: .trailing, multiplier: 1, constant: 0)
            let centerY = NSLayoutConstraint(item: self.menuButton, attribute: .centerY, relatedBy: .equal, toItem: self.contentUserImageView, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([width, height, trailing, centerY])
        }
        menuButtonSet()
        
        let contentUsernameLabelSet = {
            self.contentBackgroundView.addSubview(self.contentUsernameLabel)
            self.contentUsernameLabel.translatesAutoresizingMaskIntoConstraints = false
            self.contentUsernameLabel.textColor = Colors.shared.text
            self.contentUsernameLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
            
            let leading = NSLayoutConstraint(item: self.contentUsernameLabel, attribute: .leading, relatedBy: .equal, toItem: self.contentUserImageView, attribute: .trailing, multiplier: 1, constant: 8)
            let centerY = NSLayoutConstraint(item: self.contentUsernameLabel, attribute: .centerY, relatedBy: .equal, toItem: self.contentUserImageView, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, centerY])
        }
        contentUsernameLabelSet()
        
        let contentLabelSet = {
            self.contentBackgroundView.addSubview(self.contentLabel)
            self.contentLabel.translatesAutoresizingMaskIntoConstraints = false
            self.contentLabel.textColor = Colors.shared.text
            self.contentLabel.font = UIFont.systemFont(ofSize: 15, weight: .medium)
            self.contentLabel.backgroundColor = UIColor.clear
            self.contentLabel.isUserInteractionEnabled = false
            self.contentLabel.numberOfLines = 5
            
            let leading = NSLayoutConstraint(item: self.contentLabel, attribute: .leading, relatedBy: .equal, toItem: self.contentBackgroundView, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.contentLabel, attribute: .trailing, relatedBy: .equal, toItem: self.contentBackgroundView, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.contentLabel, attribute: .top, relatedBy: .equal, toItem: self.contentUserImageView, attribute: .bottom, multiplier: 1, constant: Sizes.margin1*0.75)
            NSLayoutConstraint.activate([leading, trailing, top])
        }
        contentLabelSet()
        
        let contentBottomStacViewSet = {
            self.contentBackgroundView.addSubview(self.contentBottomStackView)
            self.contentBottomStackView.translatesAutoresizingMaskIntoConstraints = false
//            self.contentBottomStackView.addArrangedSubview(self.hashtagLabel)
            self.contentBottomStackView.addArrangedSubview(self.dateLabel)
            self.contentBottomStackView.axis = .vertical
            self.contentBottomStackView.distribution = .fill
            self.contentBottomStackView.spacing = Sizes.margin1*0.5
            
            let leading = NSLayoutConstraint(item: self.contentBottomStackView, attribute: .leading, relatedBy: .equal, toItem: self.contentBackgroundView, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.contentBottomStackView, attribute: .trailing, relatedBy: .equal, toItem: self.contentBackgroundView, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.contentBottomStackView, attribute: .top, relatedBy: .equal, toItem: self.contentLabel, attribute: .bottom, multiplier: 1, constant: 8)
            let bottom = NSLayoutConstraint(item: self.contentBottomStackView, attribute: .bottom, relatedBy: .equal, toItem: self.contentBackgroundView, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        contentBottomStacViewSet()
        
        let hashtagLabelSet = {
            self.hashtagLabel.textColor = Colors.shared.textSub
            self.hashtagLabel.font = UIFont.systemFont(ofSize: 12, weight: .medium)
        }
        hashtagLabelSet()
        
        let dateLabelset = {
            self.dateLabel.textColor = Colors.shared.textSub
            self.dateLabel.font = UIFont.systemFont(ofSize: 12, weight: .medium)
        }
        dateLabelset()
    }
    
    @objc private func pressMenuButton(sender: UIButton) {
        delegate?.myCollectionListCellDidPressMenuButton(i: tag)
    }
    
    @objc private func pressFollowersButton(sender: UIButton) {
        delegate?.myCollectionListCellDidPressFollowers(followers: followers)
    }
    
    @objc private func pressFollowingsButton(sender: UIButton) {
        delegate?.myCollectionListCellDidPressFollowings(folowings: followings)
    }
    
    @objc private func pressLeftButton(sender: UIButton) {
        if uid == FirebaseManager.shared.getUserData(dataType: .uid) ?? "" {
            delegate?.myCollectionListCellDidPressLeftButton(action: .editProfile)
        } else {
            if isBeBlocked {
                delegate?.myCollectionListCellDidPressLeftButton(action: .beBlocked)
            } else if isIBlocked {
                delegate?.myCollectionListCellDidPressLeftButton(action: .iBlocked)
            } else if isFollow {
                delegate?.myCollectionListCellDidPressLeftButton(action: .unfollow)
            } else {
                delegate?.myCollectionListCellDidPressLeftButton(action: .follow)
            }
        }
    }
    
    @objc private func pressRightButton(sender: UIButton) {
        if uid == FirebaseManager.shared.getUserData(dataType: .uid) ?? "" {
            delegate?.myCollectionListCellDidPressRightButotn(action: .collections)
        }
    }
    
    func updateCellMode(mode: CellMode) {
        switch mode {
        case .user:
            userContentView.isHidden = false
            contentBackgroundView.isHidden = true
        case .list:
            userContentView.isHidden = true
            contentBackgroundView.isHidden = false
        }
    }
    
    func setUserData(uid: String, result: @escaping (_ callback: String?) -> ()) {
        isIBlocked = false
        isBeBlocked = false
        isFollow = false
        
        self.uid = uid
        if uid == FirebaseManager.shared.getUserData(dataType: .uid) ?? "" {
            userLeftButton.setTitle(NSLocalizedString("EditProfile", comment: ""), for: .normal)
            userRightButton.setTitle(NSLocalizedString("Collections", comment: ""), for: .normal)
        }
        
        DispatchQueue.global().async(execute: {
            User.getUserDatas(uid: uid, searchText: nil, result: {
                error, data in
                if let error = error {
                    result(error)
                } else if let data = data {
                    DispatchQueue.main.async(execute: {
                        self.followers = data.followers
                        self.followings = data.followings
                        self.userBioLabel.text = data.bio
                        self.userBioLabel.isHidden = data.bio == nil
                        self.userPostsCountLabel.text = "\(data.posts)"
                        self.userFollowerCountLabel.text = "\(data.followers.count)"
                        self.userFollowingCountLabel.text = "\(data.followings.count)"
                        
                        if uid != FirebaseManager.shared.getUserData(dataType: .uid) ?? "" {
                            var isFollow = false
                            for value in data.followers {
                                if value.uid == FirebaseManager.shared.getUserData(dataType: .uid) {
                                    isFollow = true
                                    break
                                }
                            }
                            
                            if data.iBlock {
                                self.isIBlocked = true
                                self.userLeftButton.setTitle(NSLocalizedString("Unblock", comment: ""), for: .normal)
                                self.userLeftButton.setTitleColor(Colors.shared.text, for: .normal)
                                self.userLeftButton.backgroundColor = Colors.shared.backgroundSub
                                self.userLeftButton.layer.borderColor = Colors.shared.border.cgColor
                            } else if data.blockMe {
                                self.isBeBlocked = true
                                self.userLeftButton.setTitle(NSLocalizedString("Private", comment: ""), for: .normal)
                                self.userLeftButton.setTitleColor(Colors.shared.text, for: .normal)
                                self.userLeftButton.backgroundColor = Colors.shared.backgroundSub
                                self.userLeftButton.layer.borderColor = Colors.shared.border.cgColor
                            } else {
                                self.isFollow = isFollow
                                
                                if isFollow {
                                    self.userLeftButton.setTitle(NSLocalizedString("Following", comment: ""), for: .normal)
                                    self.userLeftButton.setTitleColor(Colors.shared.text, for: .normal)
                                    self.userLeftButton.backgroundColor = Colors.shared.backgroundSub
                                    self.userLeftButton.layer.borderColor = Colors.shared.border.cgColor
                                } else {
                                    self.userLeftButton.setTitle(NSLocalizedString("Follow", comment: ""), for: .normal)
                                    self.userLeftButton.setTitleColor(UIColor.white, for: .normal)
                                    self.userLeftButton.backgroundColor = Colors.shared.point
                                    self.userLeftButton.layer.borderColor = UIColor.clear.cgColor
                                }
                            }
                        }
                        result(error)
                    })
                }
            })
        })
        
        DispatchQueue.global().async(execute: {
            UserProfileImage.downloadProfileImage(uid: uid, result: {
                error, image in
                if let error = error {
                    print(error)
                }
                
                if let image = image {
                    DispatchQueue.main.async(execute: {
                        self.userImageView.image = image
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        self.userImageView.image = UIImage(named: "user.png")
                        self.userImageView.setImageColor(color: Colors.shared.invisible)
                    })
                }
            })
        })
    }
    
    func setData(data: Post) {
        contentLabel.text = data.content
        contentUsernameLabel.text = data.username
        dateLabel.text = TimeManager.makePastTimeString(str: data.date)
        
        if let hashtag = data.hashtags {
            hashtagLabel.text = StringManager.makeHashtagString(hashtags: hashtag)
            hashtagLabel.isHidden = false
        } else {
            hashtagLabel.isHidden = true
        }
        
        DispatchQueue.global().async(execute: {
            UserProfileImage.downloadProfileImage(uid: data.uid, result: {
                error, image in
                if let error = error {
                    print(error)
                }
                
                if let image = image {
                    DispatchQueue.main.async(execute: {
                        self.contentUserImageView.image = image
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        self.contentUserImageView.image = UIImage(named: "user.png")
                        self.contentUserImageView.setImageColor(color: Colors.shared.invisible)
                    })
                }
            })
        })
    }
}
