//
//  CommentListCell.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/28.
//

import Foundation
import UIKit

protocol CommentListCellDelegate: class {
    func commentListCellPressReplyButton(i: Int)
    func commentListCellPressUsername(uid: String)
}

class CommentListCell: UITableViewCell, UITextViewDelegate {
    private let backView = UIView()
    private let userImageView = UIImageView()
    private let commentTextView = UITextView()
    private let registeredDateLabel = UILabel()
    private let replyButton = UIButton(type: .system)
    
    weak var delegate: CommentListCellDelegate?
    
    private var username: String = ""
    private var backViewLeadingConstraint: NSLayoutConstraint!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.backgroundColor = Colors.shared.backgroundSub
        
        let backViewSet = {
            self.contentView.addSubview(self.backView)
            self.backView.translatesAutoresizingMaskIntoConstraints = false
            
            self.backViewLeadingConstraint = NSLayoutConstraint(item: self.backView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.backView, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.backView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.backView, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([self.backViewLeadingConstraint, trailing, top, bottom])
        }
        backViewSet()
        
        let userImageViewSet = {
            self.backView.addSubview(self.userImageView)
            self.userImageView.translatesAutoresizingMaskIntoConstraints = false
            self.userImageView.image = UIImage(named: "user.png")
            self.userImageView.setImageColor(color: Colors.shared.invisible)
            
            let width = NSLayoutConstraint(item: self.userImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
            let height = NSLayoutConstraint(item: self.userImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
            let leading = NSLayoutConstraint(item: self.userImageView, attribute: .leading, relatedBy: .equal, toItem: self.backView, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let top = NSLayoutConstraint(item: self.userImageView, attribute: .top, relatedBy: .equal, toItem: self.backView, attribute: .top, multiplier: 1, constant: Sizes.margin1)
            NSLayoutConstraint.activate([width, height, leading, top])
        }
        userImageViewSet()
        
        let commentLabelSet = {
            self.backView.addSubview(self.commentTextView)
            self.commentTextView.translatesAutoresizingMaskIntoConstraints = false
            self.commentTextView.delegate = self
            self.commentTextView.textColor = Colors.shared.text
            self.commentTextView.font = UIFont.systemFont(ofSize: 15, weight: .medium)
            self.commentTextView.backgroundColor = UIColor.clear
        
            self.commentTextView.textContainerInset = UIEdgeInsets.zero
            self.commentTextView.textContainer.lineFragmentPadding = 0
            self.commentTextView.isScrollEnabled = false
            self.commentTextView.isEditable = false
            self.commentTextView.isSelectable = true
            
            let leading = NSLayoutConstraint(item: self.commentTextView, attribute: .leading, relatedBy: .equal, toItem: self.userImageView, attribute: .trailing, multiplier: 1, constant: Sizes.margin1*0.5)
            let trailing = NSLayoutConstraint(item: self.commentTextView, attribute: .trailing, relatedBy: .equal, toItem: self.backView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            let top = NSLayoutConstraint(item: self.commentTextView, attribute: .top, relatedBy: .equal, toItem: self.backView, attribute: .top, multiplier: 1, constant: Sizes.margin1)
            NSLayoutConstraint.activate([leading, trailing, top])
        }
        commentLabelSet()
        
        let registeredDateLabelSet = {
            self.backView.addSubview(self.registeredDateLabel)
            self.registeredDateLabel.translatesAutoresizingMaskIntoConstraints = false
            self.registeredDateLabel.textColor = Colors.shared.textSub
            self.registeredDateLabel.font = UIFont.systemFont(ofSize: 11, weight: .medium)
            
            let leading = NSLayoutConstraint(item: self.registeredDateLabel, attribute: .leading, relatedBy: .equal, toItem: self.commentTextView, attribute: .leading, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.registeredDateLabel, attribute: .top, relatedBy: .equal, toItem: self.commentTextView, attribute: .bottom, multiplier: 1, constant: Sizes.margin1*0.75)
            let bottom = NSLayoutConstraint(item: self.registeredDateLabel, attribute: .bottom, relatedBy: .equal, toItem: self.backView, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([leading, top, bottom])
        }
        registeredDateLabelSet()
        
        let replyButtonSet = {
            self.backView.addSubview(self.replyButton)
            self.replyButton.translatesAutoresizingMaskIntoConstraints = false
            self.replyButton.addTarget(self, action: #selector(self.pressReplyButton(sender:)), for: .touchUpInside)
            self.replyButton.setTitle(NSLocalizedString("Reply", comment: ""), for: .normal)
            self.replyButton.setTitleColor(Colors.shared.textSub, for: .normal)
            self.replyButton.titleLabel?.font = UIFont.systemFont(ofSize: 11, weight: .bold)
            
            let leading = NSLayoutConstraint(item: self.replyButton, attribute: .leading, relatedBy: .equal, toItem: self.registeredDateLabel, attribute: .trailing, multiplier: 1, constant: Sizes.margin1)
            let centerY = NSLayoutConstraint(item: self.replyButton, attribute: .centerY, relatedBy: .equal, toItem: self.registeredDateLabel, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, centerY])
        }
        replyButtonSet()
    }
        
    @objc private func pressReplyButton(sender:UIButton) {
        delegate?.commentListCellPressReplyButton(i: tag)
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        delegate?.commentListCellPressUsername(uid: URL.absoluteString)
        return false
    }
    
    func setItem(comment: Post.Comments) {
        registeredDateLabel.text = TimeManager.makePastTimeString(str: comment.date)
        let text = "\(comment.username) \(comment.content)"
    
        let nameFont = UIFont.systemFont(ofSize: 15, weight: .bold)
        let mentionedFont = UIFont.systemFont(ofSize: 15, weight: .medium)
        let commentFont = UIFont.systemFont(ofSize: 15, weight: .regular)
        
        let attributedStr = NSMutableAttributedString(string: text)
        attributedStr.addAttribute(NSAttributedString.Key.link, value: comment.uid, range: (text as NSString).range(of: comment.username))
        attributedStr.addAttribute(NSAttributedString.Key(rawValue: kCTFontAttributeName as String), value: nameFont, range: (text as NSString).range(of: comment.username))
        attributedStr.addAttribute(NSAttributedString.Key.foregroundColor, value: Colors.shared.text, range: (text as NSString).range(of:comment.username))
        
        if let mentionUsers = StringManager.getMentionUsers(text: text) {
            for value in mentionUsers {
                attributedStr.addAttribute(NSAttributedString.Key(rawValue: kCTFontAttributeName as String), value: mentionedFont, range: (text as NSString).range(of: value))
                attributedStr.addAttribute(NSAttributedString.Key.foregroundColor, value: Colors.shared.text, range: (text as NSString).range(of: value))
                
                for value in comment.content.getArrayAfterRegex(regex: "[^\(value)]+") {
                    attributedStr.addAttribute(NSAttributedString.Key(rawValue: kCTFontAttributeName as String), value: commentFont, range: (text as NSString).range(of: value))
                    attributedStr.addAttribute(NSAttributedString.Key.foregroundColor, value: Colors.shared.text, range: (text as NSString).range(of: value))
                }
            }
        } else {
            attributedStr.addAttribute(NSAttributedString.Key(rawValue: kCTFontAttributeName as String), value: commentFont, range: (text as NSString).range(of: comment.content))
            attributedStr.addAttribute(NSAttributedString.Key.foregroundColor, value: Colors.shared.text, range: (text as NSString).range(of:comment.content))
        }
        
        commentTextView.linkTextAttributes = [NSAttributedString.Key.foregroundColor: Colors.shared.text]
        commentTextView.attributedText = attributedStr
        
        if comment.cid == nil {
            backViewLeadingConstraint.constant = 0
        } else {
            backViewLeadingConstraint.constant = Sizes.margin2
        }
    }
}
