//
//  FollowerListCell.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/12/24.
//

import Foundation
import UIKit

protocol FollowListCellRemoveButtonDelegate: class {
    func followListCellPresssedRemoveButton(mode: FollowListCell.RemoveButtonMode, i: Int)
}

protocol FollowListCellFollowButtonDelegate: class {
    func followListCellPressFollowButton(i: Int)
}

protocol FollowListCellUserButtonDelegate: class {
    func followListCellPressUserButton(i: Int)
}

class FollowListCell: UITableViewCell {
    private let userImageView = UIImageView()
    private let removeButton = UIButton(type: .custom)
    private let usernameStackView = UIStackView()
    private let usernameLabel = UILabel()
    private let followButton = UIButton(type: .system)
    
    enum RemoveButtonMode {
        case follow, unfollow, remove, unblock
    }
    
    var uid = ""
    var removeButtonMode: RemoveButtonMode?
    
    weak var removeButtonDelegate: FollowListCellRemoveButtonDelegate?
    weak var followButtonDelegate: FollowListCellFollowButtonDelegate?
    weak var userButtonDelegate: FollowListCellUserButtonDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.backgroundColor = Colors.shared.backgroundSub
        
        let userImageViewSet = {
            self.contentView.addSubview(self.userImageView)
            self.userImageView.translatesAutoresizingMaskIntoConstraints = false
            self.userImageView.layer.borderWidth = 1
            self.userImageView.layer.borderColor = Colors.shared.border.cgColor
            self.userImageView.layer.cornerRadius = 42*0.5
            self.userImageView.image = UIImage(named: "user.png")
            self.userImageView.setImageColor(color: Colors.shared.invisible)
            self.userImageView.clipsToBounds = true
            
            let width = NSLayoutConstraint(item: self.userImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 42)
            let height = NSLayoutConstraint(item: self.userImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 42)
            let leading = NSLayoutConstraint(item: self.userImageView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let top = NSLayoutConstraint(item: self.userImageView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.userImageView, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([width, height, leading, top, bottom])
        }
        userImageViewSet()
        
        let removeButtonSet = {
            self.contentView.addSubview(self.removeButton)
            self.removeButton.translatesAutoresizingMaskIntoConstraints = false
            self.removeButton.addTarget(self, action: #selector(self.pressRemoveButton(sender:)), for: .touchUpInside)
            self.removeButton.layer.borderWidth = 0.75
            self.removeButton.layer.cornerRadius = 4
            self.removeButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .bold)
            
            let height = NSLayoutConstraint(item: self.removeButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 28)
            let trailing = NSLayoutConstraint(item: self.removeButton, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            let centerY = NSLayoutConstraint(item: self.removeButton, attribute: .centerY, relatedBy: .equal, toItem: self.userImageView, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([height, trailing, centerY])
        }
        removeButtonSet()
        
        let usernameStackViewSet = {
            self.contentView.addSubview(self.usernameStackView)
            self.usernameStackView.translatesAutoresizingMaskIntoConstraints = false
            self.usernameStackView.addArrangedSubview(self.usernameLabel)
            self.usernameStackView.addArrangedSubview(self.followButton)
            self.usernameStackView.axis = .horizontal
            self.usernameStackView.distribution = .fill
            self.usernameStackView.spacing = Sizes.margin1
                    
            let centerY = NSLayoutConstraint(item: self.usernameStackView, attribute: .centerY, relatedBy: .equal, toItem: self.userImageView, attribute: .centerY, multiplier: 1, constant: 0)
            let leading = NSLayoutConstraint(item: self.usernameStackView, attribute: .leading, relatedBy: .equal, toItem: self.userImageView, attribute: .trailing, multiplier: 1, constant: Sizes.margin1*0.5)
            let trailing = NSLayoutConstraint(item: self.usernameStackView, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: self.removeButton, attribute: .leading, multiplier: 1, constant: -Sizes.margin1*0.5)
            NSLayoutConstraint.activate([centerY, leading, trailing])
        }
        usernameStackViewSet()
        
        let usernameLabelSet = {
            self.usernameLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.pressUsername)))
            self.usernameLabel.isUserInteractionEnabled = true
            self.usernameLabel.textColor = Colors.shared.text
            self.usernameLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        }
        usernameLabelSet()
        
        let followButtonSet = {
            self.followButton.addTarget(self, action: #selector(self.pressFollowButton(sender:)), for: .touchUpInside)
            self.followButton.setTitle(NSLocalizedString("Follow", comment: ""), for: .normal)
            self.followButton.setTitleColor(Colors.shared.point, for: .normal)
            self.followButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        }
        followButtonSet()
    }
    
    @objc private func pressUsername() {
        userButtonDelegate?.followListCellPressUserButton(i: tag)
    }
    
    @objc private func pressFollowButton(sender: UIButton) {
        followButtonDelegate?.followListCellPressFollowButton(i: tag)
    }
    
    @objc private func pressRemoveButton(sender: UIButton) {
        switch removeButtonMode {
        case .follow:
            removeButtonDelegate?.followListCellPresssedRemoveButton(mode: .follow, i: tag)
        case .unfollow:
            removeButtonDelegate?.followListCellPresssedRemoveButton(mode: .unfollow, i: tag)
        case .remove:
            removeButtonDelegate?.followListCellPresssedRemoveButton(mode: .remove, i: tag)
        case .unblock:
            removeButtonDelegate?.followListCellPresssedRemoveButton(mode: .unblock, i: tag)
        case .none:
            break
        }
    }
    
    func setData(infoUid: String, data: User.Follow, mode: FollowListVC.Mode) {
        followButton.isHidden = true
        
        let dataUid: String
        switch mode {
        case .follower:
            dataUid = data.uid
        case .following:
            dataUid = data.tuid
        }
        
        DispatchQueue.global().async(execute: {
            UserProfileImage.downloadProfileImage(uid: dataUid, result: {
                error, image in
                if let error = error {
                    print(error)
                }
                
                if let image = image {
                    DispatchQueue.main.async(execute: {
                        self.userImageView.image = image
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        self.userImageView.image = UIImage(named: "user.png")
                        self.userImageView.setImageColor(color: Colors.shared.invisible)
                    })
                }
            })
        })
        
        DispatchQueue.global().async(execute: {
            User.getUserDatas(uid: dataUid, searchText: nil, result: {
                error, userData in
                if let error = error {
                    DispatchQueue.main.async(execute: {
                        self.usernameLabel.text = error
                    })
                    return
                }
                
                if let userData = userData {
                    DispatchQueue.main.async(execute: {
                        self.usernameLabel.text = userData.username
                    })
                    
                    // 팔로워 리스트에 있는 유저가 자신인 경우 아무런 버튼도 보여주지 않는다.
                    if dataUid == FirebaseManager.shared.getUserData(dataType: .uid) ?? "" {
                        DispatchQueue.main.async(execute: {
                            self.removeButton.isHidden = true
                        })
                        return
                    } 
                    
                    // infoUid를 통해 팔로워 리스트가 나의 것인지 다른 유저의 것인지 구분한다.
                    if infoUid == FirebaseManager.shared.getUserData(dataType: .uid) {
                        User.isFollow(uid: FirebaseManager.shared.getUserData(dataType: .uid) ?? "", tuid: dataUid, result: {
                            error, isFollow, isBlock in
                            if let error = error {
                                DispatchQueue.main.async(execute: {
                                    self.usernameLabel.text = error
                                })
                                return
                            }
                            
                            self.removeButtonMode = .remove
                            
                            if isFollow {
                                DispatchQueue.main.async(execute: {
                                    self.followButton.isHidden = true
                                })
                            } else {
                                DispatchQueue.main.async(execute: {
                                    self.followButton.isHidden = false
                                })
                            }
                            
                            DispatchQueue.main.async(execute: {
                                self.removeButton.isHidden = false
                                self.removeButton.setTitle("        \(NSLocalizedString("RemoveFollower", comment: ""))        ", for: .normal)
                                self.removeButton.setTitleColor(Colors.shared.text, for: .normal)
                                self.removeButton.backgroundColor = Colors.shared.backgroundSub
                                self.removeButton.layer.borderColor = Colors.shared.border.cgColor
                            })
                        })
                    } else {
                        User.isFollow(uid: FirebaseManager.shared.getUserData(dataType: .uid) ?? "", tuid: dataUid, result: {
                            error, isFollow , isBlock  in
                            if let error = error {
                                DispatchQueue.main.async(execute: {
                                    self.usernameLabel.text = error
                                })
                                return
                            }
                            
                            if isBlock {
                                self.removeButtonMode = .unblock
                                
                                DispatchQueue.main.async(execute: {
                                    self.removeButton.isHidden = false
                                    self.removeButton.setTitle("        \(NSLocalizedString("Unblock", comment: ""))        ", for: .normal)
                                    self.removeButton.setTitleColor(Colors.shared.text, for: .normal)
                                    self.removeButton.backgroundColor = Colors.shared.backgroundSub
                                    self.removeButton.layer.borderColor = Colors.shared.border.cgColor
                                })
                            } else if isFollow {
                                self.removeButtonMode = .unfollow
                                
                                DispatchQueue.main.async(execute: {
                                    self.removeButton.isHidden = false
                                    self.removeButton.setTitle("        \(NSLocalizedString("Following", comment: ""))        ", for: .normal)
                                    self.removeButton.setTitleColor(Colors.shared.text, for: .normal)
                                    self.removeButton.backgroundColor = Colors.shared.backgroundSub
                                    self.removeButton.layer.borderColor = Colors.shared.border.cgColor
                                })
                            } else {
                                self.removeButtonMode = .follow
                                
                                DispatchQueue.main.async(execute: {
                                    self.removeButton.setTitle("        \(NSLocalizedString("Follow", comment: ""))        ", for: .normal)
                                    self.removeButton.setTitleColor(UIColor.white, for: .normal)
                                    self.removeButton.backgroundColor = Colors.shared.point
                                    self.removeButton.layer.borderColor = UIColor.clear.cgColor
                                })
                            }
                        })
                    }
                }
            })
        })
    }
}
