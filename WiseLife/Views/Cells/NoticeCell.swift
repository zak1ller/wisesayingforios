//
//  NoticeCell.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2021/01/25.
//

import Foundation
import UIKit

protocol NoticeCellDelegate: class {
    func noticeCellPressUsername(uid: String)
}

class NoticeCell: UITableViewCell, UITextViewDelegate {
    let userImageView = UIImageView()
    let contentLabel = UITextView()
    
    weak var delegate: NoticeCellDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.backgroundColor = Colors.shared.backgroundSub
        
        let userImageViewSet = {
            self.contentView.addSubview(self.userImageView)
            self.userImageView.translatesAutoresizingMaskIntoConstraints = false
            self.userImageView.layer.cornerRadius = 42*0.5
            self.userImageView.clipsToBounds = true
            
            let width = NSLayoutConstraint(item: self.userImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 42)
            let height = NSLayoutConstraint(item: self.userImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 42)
            let leading = NSLayoutConstraint(item: self.userImageView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let top = NSLayoutConstraint(item: self.userImageView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: Sizes.margin1)
            let bottom = NSLayoutConstraint(item: self.userImageView, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1*0.5)
            NSLayoutConstraint.activate([width, height, leading, top, bottom])
        }
        userImageViewSet()
        
        let contentLabelSet = {
            self.contentView.addSubview(self.contentLabel)
            self.contentLabel.translatesAutoresizingMaskIntoConstraints = false
            self.contentLabel.delegate = self
            self.contentLabel.textContainerInset = UIEdgeInsets.zero
            self.contentLabel.textContainer.lineFragmentPadding = 0
            self.contentLabel.isScrollEnabled = false
            self.contentLabel.isEditable = false
            
            let leading = NSLayoutConstraint(item: self.contentLabel, attribute: .leading, relatedBy: .equal, toItem: self.userImageView, attribute: .trailing, multiplier: 1, constant: Sizes.margin1*0.5)
            let trailing = NSLayoutConstraint(item: self.contentLabel, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            let centerY = NSLayoutConstraint(item: self.contentLabel, attribute: .centerY, relatedBy: .equal, toItem: self.userImageView, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, centerY])
        }
        contentLabelSet()
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        delegate?.noticeCellPressUsername(uid: URL.absoluteString)
        return true
    }
    
    func setData(data: Like) {
        DispatchQueue.global().async(execute: {
            UserProfileImage.downloadProfileImage(uid: data.uid, result: {
                error, image in
                if let error = error {
                    print(error)
                }
                
                if let image = image {
                    DispatchQueue.main.async(execute: {
                        self.userImageView.image = image
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        self.userImageView.image = UIImage(named: "user.png")
                        self.userImageView.setImageColor(color: Colors.shared.invisible)
                    })
                }
            })
        })
        
        let text = "\(data.uname) \(NSLocalizedString("LikedYourPlaygroundPost", comment: "")) \(TimeManager.makePastTimeString(str: data.date))"
        let nameFont = UIFont.systemFont(ofSize: 13, weight: .bold)
        let contantFont = UIFont.systemFont(ofSize: 13, weight: .regular)
        let dateFont = UIFont.systemFont(ofSize: 13, weight: .medium)

        let attributedStr = NSMutableAttributedString(string: text)
        attributedStr.addAttribute(NSAttributedString.Key.link, value: data.uid, range: (text as NSString).range(of: data.uname))
        attributedStr.addAttribute(NSAttributedString.Key(rawValue: kCTFontAttributeName as String), value: nameFont, range: (text as NSString).range(of: data.uname))
        attributedStr.addAttribute(NSAttributedString.Key.foregroundColor, value: Colors.shared.text, range: (text as NSString).range(of:data.uname))
        
        attributedStr.addAttribute(NSAttributedString.Key(rawValue: kCTFontAttributeName as String), value: contantFont, range: (text as NSString).range(of: NSLocalizedString("LikedYourPlaygroundPost", comment: "")))
        attributedStr.addAttribute(NSAttributedString.Key.foregroundColor, value: Colors.shared.text, range: (text as NSString).range(of:NSLocalizedString("LikedYourPlaygroundPost", comment: "")))
        
        attributedStr.addAttribute(NSAttributedString.Key(rawValue: kCTFontAttributeName as String), value: dateFont, range: (text as NSString).range(of: TimeManager.makePastTimeString(str: data.date)))
        attributedStr.addAttribute(NSAttributedString.Key.foregroundColor, value: Colors.shared.textSub, range: (text as NSString).range(of:TimeManager.makePastTimeString(str: data.date)))
        
        contentLabel.linkTextAttributes = [NSAttributedString.Key.foregroundColor: Colors.shared.text]
        contentLabel.attributedText = attributedStr
    }
}
