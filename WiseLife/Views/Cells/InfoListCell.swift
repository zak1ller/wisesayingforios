//
//  InfoListCell.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/20.
//

import Foundation
import UIKit

class InfoListCell: UITableViewCell {
    private let stackView = UIStackView()
    private let titleLabel = UILabel()
    private let contentLabel = UILabel()
    private let editImageView = UIImageView()
    private let contentLabelUnderLine = UIView()
    
    struct InfoListItem {
        var id = ""
        var title = ""
        var content = ""
        var isEdit = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.backgroundColor = Colors.shared.backgroundSub
        
        let stackViewSet = {
            self.contentView.addSubview(self.stackView)
            self.stackView.translatesAutoresizingMaskIntoConstraints = false
            self.stackView.addArrangedSubview(self.titleLabel)
            self.stackView.addArrangedSubview(self.contentLabel)
            self.stackView.addArrangedSubview(self.editImageView)
            self.stackView.axis = .horizontal
            self.stackView.distribution = .fill
            
            let leading = NSLayoutConstraint(item: self.stackView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.stackView, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            let top = NSLayoutConstraint(item: self.stackView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: Sizes.margin1)
            let bottom = NSLayoutConstraint(item: self.stackView, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        stackViewSet()
        
        let titleLabelSet = {
            self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
            self.titleLabel.textColor = Colors.shared.text
            self.titleLabel.font = Fonts.normal
            
            let width = NSLayoutConstraint(item: self.titleLabel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 96)
            NSLayoutConstraint.activate([width])
        }
        titleLabelSet()
        
        let contentLabelSet = {
            self.contentLabel.textColor = Colors.shared.text
            self.contentLabel.font = Fonts.normal
        }
        contentLabelSet()
        
        let editImageViewSet = {
            self.editImageView.translatesAutoresizingMaskIntoConstraints = false
            self.editImageView.image = UIImage(named: "edit.png")
            self.editImageView.setImageColor(color: Colors.shared.invisible)
            
            let width = NSLayoutConstraint(item: self.editImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 18)
            let height = NSLayoutConstraint(item: self.editImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 18)
            NSLayoutConstraint.activate([width, height])
        }
        editImageViewSet()
        
        let underLineSet = {
            self.contentView.addSubview(self.contentLabelUnderLine)
            self.contentLabelUnderLine.translatesAutoresizingMaskIntoConstraints = false
            self.contentLabelUnderLine.backgroundColor = Colors.shared.line
            
            let height = NSLayoutConstraint(item: self.contentLabelUnderLine, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0.75)
            let leading = NSLayoutConstraint(item: self.contentLabelUnderLine, attribute: .leading, relatedBy: .equal, toItem: self.contentLabel, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.contentLabelUnderLine, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            let bottom = NSLayoutConstraint(item: self.contentLabelUnderLine, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([height, leading, trailing, bottom])
        }
        underLineSet()
    }
    
    func setItem(item: InfoListItem) {
        if item.isEdit {
            editImageView.isHidden = false
        } else {
            editImageView.isHidden = true
        }
        
        titleLabel.text = item.title
        contentLabel.text = item.content
    }
}
