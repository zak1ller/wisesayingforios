//
//  MenuListCell.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/19.
//

import Foundation
import UIKit

protocol MenuListCellDelegate: class {
    func menuListCellDidSelect(i: Int)
}

class MenuListCell: UITableViewCell {
    private let menuStackView = UIStackView()
    private let menuImaageView = UIImageView()
    private let menuTitle = UILabel()
    private let underLine = UIView()
    
    weak var delegate: MenuListCellDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.backgroundColor = Colors.shared.backgroundSub
        
        contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(pressContentView)))
        let menuStackViewSet = {
            self.contentView.addSubview(self.menuStackView)
            self.menuStackView.translatesAutoresizingMaskIntoConstraints = false
            self.menuStackView.addArrangedSubview(self.menuImaageView)
            self.menuStackView.addArrangedSubview(self.menuTitle)
            self.menuStackView.addArrangedSubview(UIView())
            self.menuStackView.axis = .horizontal
            self.menuStackView.distribution = .fill
            self.menuStackView.spacing = Sizes.margin1
            self.menuStackView.isUserInteractionEnabled = false
            
            let leading = NSLayoutConstraint(item: self.menuStackView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.menuStackView, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            let top = NSLayoutConstraint(item: self.menuStackView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: Sizes.margin1)
            let bottom = NSLayoutConstraint(item: self.menuStackView, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        menuStackViewSet()
        
        let underLineSet = {
            self.contentView.addSubview(self.underLine)
            self.underLine.translatesAutoresizingMaskIntoConstraints = false
            self.underLine.backgroundColor = Colors.shared.line
            
            let height = NSLayoutConstraint(item: self.underLine, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0.75)
            let leading = NSLayoutConstraint(item: self.underLine, attribute: .leading, relatedBy: .equal, toItem: self.menuTitle, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.underLine, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.underLine, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([height, leading, trailing, bottom])
        }
        underLineSet()
        
        let menuImageViewSet = {
            self.menuImaageView.translatesAutoresizingMaskIntoConstraints = false
            
            let width = NSLayoutConstraint(item: self.menuImaageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
            let height = NSLayoutConstraint(item: self.menuImaageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
            NSLayoutConstraint.activate([width, height])
        }
        menuImageViewSet()
        
        let menuTitleSet = {
            self.menuTitle.textColor = Colors.shared.text
            self.menuTitle.font = Fonts.normal
        }
        menuTitleSet()
    }
    
    @objc private func pressContentView() {
        delegate?.menuListCellDidSelect(i: tag)
    }
    
    func setItem(item: MenuVC.MenuItem) {
        if let image = item.image {
            menuImaageView.isHidden = false
            menuImaageView.image = image
            menuImaageView.setImageColor(color: Colors.shared.text)
        } else {
            menuImaageView.isHidden = true
        }
        
        menuTitle.text = item.title
    }
}
