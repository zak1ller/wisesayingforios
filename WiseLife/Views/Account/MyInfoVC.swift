//
//  MyInfoVC.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/20.
//

import Foundation
import UIKit

class MyInfoVC: UIViewController, UITableViewDelegate, UITableViewDataSource, TopViewBackButtonDelegate, EditUsernameVcDelegate {
    private let topView = TopView()
    private let tableView = UITableView()
    
    private var list: [InfoListCell.InfoListItem] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        list.append(InfoListCell.InfoListItem(id: "user_name", title: NSLocalizedString("Username", comment: ""), content: "", isEdit: true))
        list.append(InfoListCell.InfoListItem(id: "email", title: NSLocalizedString("Email", comment: ""), content: ""))
        list.append(InfoListCell.InfoListItem(id: "provide", title: NSLocalizedString("SigninProvide", comment: ""), content: ""))
        setLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateUsername()
        updateEmail()
        updateProvide()
    }
    
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func editUsernameVcDidChangeUsername(username: String) {
        list[0].content = username
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InfoListCell", for: indexPath) as! InfoListCell
        cell.tag = indexPath.row
        cell.setItem(item: list[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = list[indexPath.row]
        if item.id == "user_name" {
            let vc = EditUsernameVC()
            vc.delegate = self
            vc.setBeforeUsername(username: item.content)
            DispatchQueue.main.async(execute: {
                self.present(vc, animated: true, completion: nil)
            })
        }
    }
    
    private func updateUsername() {
        DispatchQueue.global().async(execute: {
            User.getUsername(uid: FirebaseManager.shared.getUserData(dataType: .uid) ?? "", result: {
                error, name in
                if let error = error {
                    DispatchQueue.main.async(execute: {
                        self.list[0].content = error
                    })
                } else if let name = name {
                    DispatchQueue.main.async(execute: {
                        self.list[0].content = name
                    })
                }
                DispatchQueue.main.async(execute: {
                    self.tableView.reloadData()
                })
            })
        })
    }
    
    private func updateEmail() {
        list[1].content = FirebaseManager.shared.getUserData(dataType: .email) ?? "no email"
    }
    
    private func updateProvide() {
        let provider = FirebaseManager.shared.getUserData(dataType: .provider) ?? "no data"
        if provider == "password" {
            list[2].content = NSLocalizedString("PasswordProvider", comment: "")
        } else {
            list[2].content = provider
        }
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        
        topView.showOnSubView(toView: view)
        topView.settitle(title: NSLocalizedString("MyInformation", comment: ""))
        topView.showBackButton()
        topView.backButtonDelegate = self
        
        let tableViewSet = {
            self.view.addSubview(self.tableView)
            self.tableView.translatesAutoresizingMaskIntoConstraints = false
            self.tableView.register(InfoListCell.self, forCellReuseIdentifier: "InfoListCell")
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.rowHeight = UITableView.automaticDimension
            self.tableView.separatorStyle = .none
            self.tableView.backgroundColor = UIColor.clear
            
            let leading = NSLayoutConstraint(item: self.tableView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.tableView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.tableView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            let bottom = NSLayoutConstraint(item: self.tableView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        tableViewSet()

    }
}
