//
//  ForgotPasswordVC.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/09.
//

import Foundation
import UIKit

class ForgotPasswordVC: UIViewController, TopViewBackButtonDelegate, UITextFieldDelegate {
    private var loadingView: LoadingView!
    private let topView = TopView()
    private let contentView = UIStackView()
    private let supportLabel = UILabel()
    private var emailFieldView: ClearFieldView!
    private let sendButton = UIButton(type: .system)
    
    private var contentCenterYConstraint: NSLayoutConstraint!
    private var sendButtonBottomConstraint: NSLayoutConstraint!
    
    deinit {
        print("Forgot Password VC Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadingView = LoadingView(view: view)
        loadingView.setColor(color: Colors.shared.invisible)
        
        emailFieldView = ClearFieldView(vc: self)
        emailFieldView.setReturnType(returnType: .send)
        emailFieldView.setKeyboardType(keyboardType: .emailAddress)
        emailFieldView.setTextFieldDelegate(delegate: self)
        
        sendButton.isHidden = true
        
        setLayout()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIWindow.keyboardWillShowNotification,
            object: nil)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: UIWindow.keyboardWillHideNotification,
            object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        emailFieldView.onBecome()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        send()
        return false
    }
    
    @objc func keyboardWillShow(_ notificaiton: Notification) {
        if let keyboardFrame: NSValue = notificaiton.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardHeight = keyboardFrame.cgRectValue.height
            contentCenterYConstraint.constant = -(keyboardHeight/2)
            sendButtonBottomConstraint.constant = -(keyboardHeight+20)
            sendButton.isHidden = false
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        contentCenterYConstraint.constant = 0
        sendButtonBottomConstraint.constant = 0
        sendButton.isHidden = true
    }
    
    @objc func pressSendButton(sender: UIButton) {
        send()
    }
    
    private func send() {
        view.endEditing(true)
        
        let e = emailFieldView.getText()
        if e.count == 0 {
            emailFieldView.onBecome()
        } else {
            loadingView.start()
            
            DispatchQueue.global().async(execute: {
                FirebaseManager.shared.sendUpdatePasswordEmail(email: e, result: {
                    error in
                    self.loadingView.stop()
                    
                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: {
                            (_) in
                            DispatchQueue.main.async(execute: {
                                self.emailFieldView.onBecome()
                            })
                        }))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        let alert = UIAlertController(title: NSLocalizedString("SentSuccessEmailMessage", comment: ""), message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: {
                            (_) in
                            DispatchQueue.main.async(execute: {
                                self.dismiss(animated: true, completion: nil)
                            })
                        }))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    }
                })
            })
        }
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.background
        
        topView.show(toView: view)
        topView.settitle(title: NSLocalizedString("FindPassword", comment: ""))
        topView.showBackButton()
        topView.backButtonDelegate = self
        
        let contentViewSet = {
            self.view.addSubview(self.contentView)
            self.contentView.translatesAutoresizingMaskIntoConstraints = false
            self.contentView.addArrangedSubview(self.supportLabel)
            self.contentView.addArrangedSubview(self.emailFieldView)
            self.contentView.axis = .vertical
            self.contentView.distribution = .fill
            self.contentView.spacing = 15
            
            let leading = NSLayoutConstraint(item: self.contentView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let trailing  = NSLayoutConstraint(item: self.contentView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            self.contentCenterYConstraint = NSLayoutConstraint(item: self.contentView, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading,trailing, self.contentCenterYConstraint])
        }
        contentViewSet()
        
        let supportLabelSet = {
            self.supportLabel.text = NSLocalizedString("FindPasswordSupportText", comment: "")
            self.supportLabel.textColor = Colors.shared.text
            self.supportLabel.textAlignment = .center
            self.supportLabel.font = Fonts.normal
            self.supportLabel.numberOfLines = 0
        }
        supportLabelSet()
        
        let sendButtonSet = {
            self.view.addSubview(self.sendButton)
            self.sendButton.translatesAutoresizingMaskIntoConstraints = false
            self.sendButton.addTarget(self, action: #selector(self.pressSendButton(sender:)), for: .touchUpInside)
            self.sendButton.setTitle(NSLocalizedString("SendEmailButton", comment: ""), for: .normal)
            self.sendButton.setTitleColor(UIColor.white, for: .normal)
            self.sendButton.titleLabel?.font = Fonts.normal
            self.sendButton.backgroundColor = Colors.shared.point
            self.sendButton.layer.cornerRadius = Sizes.button2*0.5
            
            let height = NSLayoutConstraint(item: self.sendButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button2)
            let leading = NSLayoutConstraint(item: self.sendButton, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leadingMargin, multiplier: 1, constant: Sizes.margin3)
            let trailing = NSLayoutConstraint(item: self.sendButton, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            self.sendButtonBottomConstraint = NSLayoutConstraint(item: self.sendButton, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([height, leading, trailing, self.sendButtonBottomConstraint])
        }
        sendButtonSet()
    }
    
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
}
