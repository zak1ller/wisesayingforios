//
//  SignUpStepEmailVC.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/12/26.
//

import Foundation
import UIKit

class SignUpStepEmailVC: UIViewController {
    var loadingView: LoadingView!
    let topView = TopView()
    let emailTitleLabel = UILabel()
    let emailTextField = UITextField()
    let doneButton = UIButton(type: .system)
    
    var doneButtonBottomConstraint: NSLayoutConstraint!
    var isAuth = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIWindow.keyboardWillShowNotification,
            object: nil)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: UIWindow.keyboardWillHideNotification,
            object: nil)
        
        loadingView = LoadingView(view: view)
        loadingView.setColor(color: Colors.shared.invisible)
        
        setLayout()
    }
    
    deinit {
        if !isAuth {
            FirebaseManager.shared.deleteUserForPassword(password: "wswsws", result: {
                error, isDelete  in
                print(FirebaseManager.shared.signOut() ?? "")
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        emailTextField.becomeFirstResponder()
    }
    
    @objc private func keyboardWillShow(_ notificaiton: Notification) {
        if let keyboardFrame: NSValue = notificaiton.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardHeight = keyboardFrame.cgRectValue.height
            doneButtonBottomConstraint.constant = -keyboardHeight
            doneButton.isHidden = false
        }
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        doneButtonBottomConstraint.constant = 0
        doneButton.isHidden = true
    }
    
    @objc private func pressDoneButton(sender: UIButton) {
        checkEmail()
    }
    
    private func checkEmail() {
        let email = emailTextField.text ?? "".trimmingCharacters(in: .whitespacesAndNewlines)
        
        loadingView.start()
        
        DispatchQueue.global().async(execute: {
            User.isVaildEmailAddress(email: email, result: {
                error, isVaild in
                self.loadingView.stop()
                
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                    return
                }
                
                if !isVaild {
                    let alert = UIAlertController(title: NSLocalizedString("InvalidEmailAddress", comment: ""), message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                    return
                }
                
                self.loadingView.start()
                
                FirebaseManager.shared.createEmailAccount(email: email, password: "wswsws", result: {
                    error in
                    self.loadingView.stop()
                    
                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                        return
                    }
                    
                    self.loadingView.start()
                    
                    FirebaseManager.shared.sendAuthEmail(result: {
                        error in
                        self.loadingView.stop()
                        
                        if let error = error {
                            let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                            DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true, completion: nil)
                            })
                        } else {
                            DispatchQueue.main.async(execute: {
                                let vc = EmailAuthVC()
                                vc.modalPresentationStyle = .overFullScreen
                                vc.delegate = self
                                self.present(vc, animated: true, completion: nil)
                            })
                        }
                    })
                })
            })
        })
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        
        topView.show(toView: view)
        topView.settitle(title: NSLocalizedString("SignUp", comment: ""))
        topView.showBackButton()
        topView.backButtonDelegate = self
        
        let emailTitleLabelSet = {
            self.view.addSubview(self.emailTitleLabel)
            self.emailTitleLabel.translatesAutoresizingMaskIntoConstraints = false
            self.emailTitleLabel.text = NSLocalizedString("TypeEmailAddressForSignUp", comment: "")
            self.emailTitleLabel.textColor = Colors.shared.text
            self.emailTitleLabel.textAlignment = .center
            self.emailTitleLabel.font = Fonts.normal
            self.emailTitleLabel.numberOfLines = 0
            
            let leading = NSLayoutConstraint(item: self.emailTitleLabel, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.emailTitleLabel, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            let top = NSLayoutConstraint(item: self.emailTitleLabel, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            NSLayoutConstraint.activate([leading, trailing, top])
        }
        emailTitleLabelSet()
        
        let emailTextFieldSet = {
            self.view.addSubview(self.emailTextField)
            self.emailTextField.translatesAutoresizingMaskIntoConstraints = false
            self.emailTextField.delegate = self
            self.emailTextField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
            self.emailTextField.textColor = Colors.shared.text
            self.emailTextField.font = Fonts.normal
            self.emailTextField.textAlignment = .center
            self.emailTextField.layer.borderWidth = 1
            self.emailTextField.layer.borderColor = Colors.shared.border.cgColor
            self.emailTextField.layer.cornerRadius = 4
            self.emailTextField.returnKeyType = .done
            
            let height = NSLayoutConstraint(item: self.emailTextField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
            let leading = NSLayoutConstraint(item: self.emailTextField, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.emailTextField, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            let top = NSLayoutConstraint(item: self.emailTextField, attribute: .top, relatedBy: .equal, toItem: self.emailTitleLabel, attribute: .bottom, multiplier: 1, constant: Sizes.margin1)
            NSLayoutConstraint.activate([height, leading, trailing, top])
        }
        emailTextFieldSet()
        
        let doneButtonSet = {
            self.view.addSubview(self.doneButton)
            self.doneButton.translatesAutoresizingMaskIntoConstraints = false
            self.doneButton.addTarget(self, action: #selector(self.pressDoneButton(sender:)), for: .touchUpInside)
            self.doneButton.setTitle(NSLocalizedString("SendAuthMail", comment: ""), for: .normal)
            self.doneButton.setTitleColor(UIColor.white, for: .normal)
            self.doneButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
            self.doneButton.backgroundColor = Colors.shared.invisible
            self.doneButton.isEnabled = false
            
            let height = NSLayoutConstraint(item: self.doneButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 56)
            let leading = NSLayoutConstraint(item: self.doneButton, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.doneButton, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            self.doneButtonBottomConstraint = NSLayoutConstraint(item: self.doneButton, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([height, leading, trailing, self.doneButtonBottomConstraint])
        }
        doneButtonSet()
    }
}

extension SignUpStepEmailVC: TopViewBackButtonDelegate {
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
}

extension SignUpStepEmailVC: UITextFieldDelegate {
    @objc func textFieldDidChange(_ textField: UITextField) {
        let text = textField.text ?? ""
        if text.count > 0 {
            doneButton.isEnabled = true
            doneButton.backgroundColor = Colors.shared.point
        } else {
            doneButton.isEnabled = false
            doneButton.backgroundColor = Colors.shared.invisible
        }
    }
}

extension SignUpStepEmailVC: EmailAuthVcDelegate {
    func emailAuthVcAuthSuccess() {
        let vc = PasswordSetVC()
        vc.modalPresentationStyle = .fullScreen
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
}

extension SignUpStepEmailVC: PasswordSetVcDelegate {
    func passwordSetVcCompleteSignUp() {
        isAuth = true
        dismiss(animated: false, completion: nil)
    }
}
