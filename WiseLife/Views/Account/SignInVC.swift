//
//  ViewController.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/07.
//

import UIKit

protocol SignInVcDelegate: class {
    func signInVcSignedIn()
}

class SignInVC: UIViewController, UITextFieldDelegate {
    private var loadingView: LoadingView!
    private var hideKeyboardButton: HideKeyboardButton!
    private let centerContentView = UIView()
    private let idPasswordFiledStackView = UIStackView()
    private var idFieldView: ClearFieldView!
    private var pwFieldView: ClearFieldView!
    private var passwordForgotButton = UIButton(type: .system)
    private let signInButton = UIButton(type: .system)
    private let orSignInLabel = UILabel()
    private let bottomStackView = UIStackView()
    private let flatformSignInContentView = UIView()
    private let flatFormSignInStackView = UIStackView()
    private let facebookSignInButton = UIButton(type: .system)
    private let googleSignInButton = UIButton(type: .system)
    private let appleSignInButton = UIButton(type: .system)
    
    private let bottomContentView = UIView()
    private let bottomSignUpStackView = UIStackView()
    private let bottomSignUpLeftLabel = UILabel()
    private let bottomSignUpRightButton = UIButton(type: .system)
    
    private var centerContentCenterConstraint: NSLayoutConstraint!
    private var keyboardHideBottomConstraint: NSLayoutConstraint!
    
    private let KEY_ID_FIELD = 1
    private let KEY_PW_FIELD = 2
    
    weak var delegate: SignInVcDelegate?
    
    deinit {
        print("Signin VC Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadingView = LoadingView(view: view)
        loadingView.setColor(color: Colors.shared.invisible)
        
        idFieldView = ClearFieldView(vc: self)
        idFieldView.setTextFieldDelegate(delegate: self)
        idFieldView.setId(id: KEY_ID_FIELD)
        idFieldView.setPlaceHoder(text: NSLocalizedString("Email", comment: ""))
        idFieldView.setReturnType(returnType: .next)
        idFieldView.setKeyboardType(keyboardType: .emailAddress)
      
        pwFieldView = ClearFieldView(vc: self)
        pwFieldView.setTextFieldDelegate(delegate: self)
        pwFieldView.setId(id: KEY_PW_FIELD)
        pwFieldView.setPlaceHoder(text: NSLocalizedString("Password", comment: ""))
        pwFieldView.setReturnType(returnType: .done)
        pwFieldView.setKeyboardType(keyboardType: .emailAddress)
        pwFieldView.onPasswordMode()
        
        hideKeyboardButton = HideKeyboardButton(vc: self)
        hideKeyboardButton.isHidden = true
            
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIWindow.keyboardWillShowNotification,
            object: nil)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: UIWindow.keyboardWillHideNotification,
            object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        switch FirebaseManager.shared.isSignedIn() {
        case .success:
            successSignedIn()
        case .failure:
            setLayout()
        case .noauth:
            setLayout()
        }
    }
    
    @objc func keyboardWillShow(_ notificaiton: Notification) {
        if let keyboardFrame: NSValue = notificaiton.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardHeight = keyboardFrame.cgRectValue.height
            
            DispatchQueue.main.async(execute: {
                self.orSignInLabel.isHidden = true
                self.flatformSignInContentView.isHidden = true
                self.hideKeyboardButton.isHidden = false
            })
            
            keyboardHideBottomConstraint.constant = -keyboardHeight
            centerContentCenterConstraint.constant = -((keyboardHeight/2)-15)
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        DispatchQueue.main.async(execute: {
            self.orSignInLabel.isHidden = false
            self.flatformSignInContentView.isHidden = false
            self.hideKeyboardButton.isHidden = true
        })
        
        keyboardHideBottomConstraint.constant = 0
        centerContentCenterConstraint.constant = 0
    }
    
    @objc func pressHideKeyboard(sender: UIButton) {
        view.endEditing(true)
    }
    
    @objc func pressSignInButton(sender: UIButton) {
        trySignIn()
    }
    
    @objc func pressPasswordForgotButton(sender: UIButton) {
        let vc = ForgotPasswordVC()
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    
    @objc func pressFacebookSignInButton(sender: UIButton) {
        
    }
    
    @objc func pressGoogleSignInButton(sender: UIButton) {
        
    }
    
    @objc func pressAppleSignInButton(sender: UIButton) {
        
    }
    
    @objc func pressSignUpButton(sender: UIButton) {
        let vc = SignUpStepEmailVC()
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == KEY_ID_FIELD {
            pwFieldView.onBecome()
        } else if textField.tag == KEY_PW_FIELD {
            trySignIn()
        }
        return false
    }
    
    private func trySignIn() {
        view.endEditing(true)
        
        let e = idFieldView.getText()
        let p = pwFieldView.getText()
        if e.count == 0 {
            idFieldView.onBecome()
        } else if p.count == 0 {
            pwFieldView.onBecome()
        } else {
            loadingView.start()
            
            DispatchQueue.global().async(execute: {
                FirebaseManager.shared.signIn(email: e, password: p, result: {
                    error in
                    self.loadingView.stop()
                    
                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        DispatchQueue.main.async(execute: {
                            self.successSignedIn()
                        })
                    }
                })
            })
        }
    }
    
    private func successSignedIn() {
        dismiss(animated: true, completion: {
            self.delegate?.signInVcSignedIn()
        })
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.background
        
        let centerContentViewSet = {
            self.view.addSubview(self.centerContentView)
            self.centerContentView.translatesAutoresizingMaskIntoConstraints = false
            
            let leading = NSLayoutConstraint(item: self.centerContentView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let trailing = NSLayoutConstraint(item: self.centerContentView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            self.centerContentCenterConstraint = NSLayoutConstraint(item: self.centerContentView, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, self.centerContentCenterConstraint])
        }
        centerContentViewSet()
        
        let idPasswordFieldStackViewSet = {
            self.centerContentView.addSubview(self.idPasswordFiledStackView)
            self.idPasswordFiledStackView.translatesAutoresizingMaskIntoConstraints = false
            self.idPasswordFiledStackView.addArrangedSubview(self.idFieldView)
            self.idPasswordFiledStackView.addArrangedSubview(self.pwFieldView)
            self.idPasswordFiledStackView.addArrangedSubview(self.passwordForgotButton)
            self.idPasswordFiledStackView.axis = .vertical
            self.idPasswordFiledStackView.distribution = .fillEqually
            
            let leading = NSLayoutConstraint(item: self.idPasswordFiledStackView, attribute: .leading, relatedBy: .equal, toItem: self.centerContentView, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.idPasswordFiledStackView, attribute: .trailing, relatedBy: .equal, toItem: self.centerContentView, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.idPasswordFiledStackView, attribute: .top, relatedBy: .equal, toItem: self.centerContentView, attribute: .top, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top])
        }
        idPasswordFieldStackViewSet()
        
        let passwordForgotButtonSet = {
            self.passwordForgotButton.addTarget(self, action: #selector(self.pressPasswordForgotButton(sender:)), for: .touchUpInside)
            self.passwordForgotButton.setTitle(NSLocalizedString("ForgotPasswordButton", comment: ""), for: .normal)
            self.passwordForgotButton.setTitleColor(Colors.shared.textSub, for: .normal)
            self.passwordForgotButton.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .medium)
            self.passwordForgotButton.contentHorizontalAlignment = .right
        }
        passwordForgotButtonSet()
        
        let signInButtonSet = {
            self.centerContentView.addSubview(self.signInButton)
            self.signInButton.translatesAutoresizingMaskIntoConstraints = false
            self.signInButton.addTarget(self, action: #selector(self.pressSignInButton(sender:)), for: .touchUpInside)
            self.signInButton.setTitle(NSLocalizedString("SignIn", comment: ""), for: .normal)
            self.signInButton.setTitleColor(Colors.shared.text, for: .normal)
            self.signInButton.titleLabel?.font = Fonts.normal
            self.signInButton.backgroundColor = Colors.shared.backgroundSub
            self.signInButton.layer.cornerRadius = 4
            
            let height = NSLayoutConstraint(item: self.signInButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
            let leading = NSLayoutConstraint(item: self.signInButton, attribute: .leading, relatedBy: .equal, toItem: self.centerContentView, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.signInButton, attribute: .trailing, relatedBy: .equal, toItem: self.centerContentView, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.signInButton, attribute: .top, relatedBy: .equal, toItem: self.idPasswordFiledStackView, attribute: .bottom, multiplier: 1, constant: 15)
            NSLayoutConstraint.activate([height, leading, trailing, top])
        }
        signInButtonSet()
        
        let bottomStackViewSet = {
            self.centerContentView.addSubview(self.bottomStackView)
            self.bottomStackView.translatesAutoresizingMaskIntoConstraints = false
            self.bottomStackView.addArrangedSubview(self.orSignInLabel)
            self.bottomStackView.addArrangedSubview(self.flatformSignInContentView)
            self.bottomStackView.axis = .vertical
            self.bottomStackView.distribution = .fill
            
            let leading = NSLayoutConstraint(item: self.bottomStackView, attribute: .leading, relatedBy: .equal, toItem: self.centerContentView, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.bottomStackView, attribute: .trailing, relatedBy: .equal, toItem: self.centerContentView, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.bottomStackView, attribute: .top, relatedBy: .equal, toItem: self.signInButton, attribute: .bottom, multiplier: 1, constant: 15)
            let bottom = NSLayoutConstraint(item: self.bottomStackView, attribute: .bottom, relatedBy: .equal, toItem: self.centerContentView, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        bottomStackViewSet()
        
        let orSignInLabelSet = {
            self.orSignInLabel.translatesAutoresizingMaskIntoConstraints = false
            self.orSignInLabel.text = NSLocalizedString("OrSignInWithLabelText", comment: "")
            self.orSignInLabel.textColor = Colors.shared.invisible
            self.orSignInLabel.textAlignment = .center
            self.orSignInLabel.font = UIFont.systemFont(ofSize: 12, weight: .medium)
            
            let height = NSLayoutConstraint(item: self.orSignInLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
            NSLayoutConstraint.activate([height])
        }
        orSignInLabelSet()
        
        let flatformContentViewSet = {
            self.flatformSignInContentView.translatesAutoresizingMaskIntoConstraints = false
            
            let height = NSLayoutConstraint(item: self.flatformSignInContentView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
            NSLayoutConstraint.activate([height])
        }
        flatformContentViewSet()
        
        let flatformContentStackViewSet = {
            self.flatformSignInContentView.addSubview(self.flatFormSignInStackView)
            self.flatFormSignInStackView.translatesAutoresizingMaskIntoConstraints = false
            self.flatFormSignInStackView.addArrangedSubview(self.appleSignInButton)
            self.flatFormSignInStackView.addArrangedSubview(self.googleSignInButton)
            self.flatFormSignInStackView.addArrangedSubview(self.facebookSignInButton)
            self.flatFormSignInStackView.axis = .horizontal
            self.flatFormSignInStackView.distribution = .fillEqually
            self.flatFormSignInStackView.spacing = 30
            
            let centerX = NSLayoutConstraint(item: self.flatFormSignInStackView, attribute: .centerX, relatedBy: .equal, toItem: self.flatformSignInContentView, attribute: .centerX, multiplier: 1, constant: 0)
            let centerY = NSLayoutConstraint(item: self.flatFormSignInStackView, attribute: .centerY, relatedBy: .equal, toItem: self.flatformSignInContentView, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([centerX, centerY])
        }
        flatformContentStackViewSet()
        
        let appleSignInButtonSet = {
            self.appleSignInButton.translatesAutoresizingMaskIntoConstraints = false
            self.appleSignInButton.addTarget(self, action: #selector(self.pressAppleSignInButton(sender:)), for: .touchUpInside)
            self.appleSignInButton.setImage(UIImage(named: "apple.png"), for: .normal)
            self.appleSignInButton.tintColor = Colors.shared.text
            
            let width = NSLayoutConstraint(item: self.appleSignInButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
            let height = NSLayoutConstraint(item: self.appleSignInButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
            NSLayoutConstraint.activate([width, height])
        }
        appleSignInButtonSet()
        
        let googleSignInButtonSet = {
            self.googleSignInButton.translatesAutoresizingMaskIntoConstraints = false
            self.googleSignInButton.addTarget(self, action: #selector(self.pressGoogleSignInButton(sender:)), for: .touchUpInside)
            self.googleSignInButton.setImage(UIImage(named: "google.png"), for: .normal)
            self.googleSignInButton.tintColor = UIColor(hexString: "#DB4437")
            
            let width = NSLayoutConstraint(item: self.googleSignInButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
            let height = NSLayoutConstraint(item: self.googleSignInButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
            NSLayoutConstraint.activate([width, height])
        }
        googleSignInButtonSet()
        
        let facebookSignInButtonSet = {
            self.facebookSignInButton.translatesAutoresizingMaskIntoConstraints = false
            self.facebookSignInButton.addTarget(self, action: #selector(self.pressFacebookSignInButton(sender:)), for: .touchUpInside)
            self.facebookSignInButton.setImage(UIImage(named: "facebook.png"), for: .normal)
            self.facebookSignInButton.tintColor = UIColor(hexString: "#4267B2")
            
            let width = NSLayoutConstraint(item: self.facebookSignInButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
            let height = NSLayoutConstraint(item: self.facebookSignInButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
            NSLayoutConstraint.activate([width, height])
        }
        facebookSignInButtonSet()
        
        let hideKeyboardButtonSet = {
            self.view.addSubview(self.hideKeyboardButton)
            self.hideKeyboardButton.translatesAutoresizingMaskIntoConstraints = false
            self.hideKeyboardButton.addTarget(self, action: #selector(self.pressHideKeyboard(sender:)), for: .touchUpInside)
            
            let leading = NSLayoutConstraint(item: self.hideKeyboardButton!, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.hideKeyboardButton!, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            self.keyboardHideBottomConstraint = NSLayoutConstraint(item: self.hideKeyboardButton!, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, self.keyboardHideBottomConstraint])
        }
        hideKeyboardButtonSet()
        
        let bottomContentViewSet = {
            self.view.addSubview(self.bottomContentView)
            self.bottomContentView.translatesAutoresizingMaskIntoConstraints = false
            
            let height = NSLayoutConstraint(item: self.bottomContentView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.bottomAreaSmall)
            let leading = NSLayoutConstraint(item: self.bottomContentView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.bottomContentView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.bottomContentView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([height, leading, trailing, bottom])
        }
        bottomContentViewSet()
        
        let bottomSignUpStackViewSet = {
            self.bottomContentView.addSubview(self.bottomSignUpStackView)
            self.bottomSignUpStackView.translatesAutoresizingMaskIntoConstraints = false
            self.bottomSignUpStackView.addArrangedSubview(self.bottomSignUpLeftLabel)
            self.bottomSignUpStackView.addArrangedSubview(self.bottomSignUpRightButton)
            self.bottomSignUpStackView.axis = .horizontal
            self.bottomSignUpStackView.distribution = .fill
            
            let centerX = NSLayoutConstraint(item: self.bottomSignUpStackView, attribute: .centerX, relatedBy: .equal, toItem: self.bottomContentView, attribute: .centerX, multiplier: 1, constant: 0)
            let centerY = NSLayoutConstraint(item: self.bottomSignUpStackView, attribute: .centerY, relatedBy: .equal, toItem: self.bottomContentView, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([centerX, centerY])
        }
        bottomSignUpStackViewSet()
        
        let bottomSignUpLeftLabelSet = {
            self.bottomSignUpLeftLabel.text = NSLocalizedString("DontHaveAccountText", comment: "")
            self.bottomSignUpLeftLabel.textColor = Colors.shared.textSub
            self.bottomSignUpLeftLabel.font = UIFont.systemFont(ofSize: 12, weight: .medium)
        }
        bottomSignUpLeftLabelSet()
        
        let bottomSignUpRightButtonSet = {
            self.bottomSignUpRightButton.addTarget(self, action: #selector(self.pressSignUpButton(sender:)), for: .touchUpInside)
            self.bottomSignUpRightButton.setTitle(NSLocalizedString("SignUpText", comment: ""), for: .normal)
            self.bottomSignUpRightButton.setTitleColor(Colors.shared.point, for: .normal)
            self.bottomSignUpRightButton.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .medium)
        }
        bottomSignUpRightButtonSet()
    }
}

