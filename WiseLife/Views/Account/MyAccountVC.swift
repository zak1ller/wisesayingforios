//
//  MyAccountVC.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/19.
//

import Foundation
import UIKit
 
protocol MyAccountVcDelegate: class {
    func myAccountVcDidClose()
    func myAccountVcSignedOut()
}

class MyAccountVC: UIViewController, UITableViewDelegate, UITableViewDataSource, TopViewBackButtonDelegate, EnterPasswordVcDelegate {
    private var loadingView: LoadingView!
    private let topView = TopView()
    private let tableView = UITableView()
    
    weak var delegate: MyAccountVcDelegate?
    private var list: [MenuOneLineListCell.MenuOneLineItem] = []
    
    deinit {
        print("My Account Vc Deinited.")
        delegate?.myAccountVcDidClose()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        list.append(MenuOneLineListCell.MenuOneLineItem(id: "my_info", title: NSLocalizedString("MyInformation", comment: "")))
        list.append(MenuOneLineListCell.MenuOneLineItem(id: "sign_out", title: NSLocalizedString("SignOut", comment: "")))
        list.append(MenuOneLineListCell.MenuOneLineItem(id: "delete_account", title: NSLocalizedString("DeleteAccount", comment: "")))
        
        loadingView = LoadingView(view: view)
        loadingView.setColor(color: Colors.shared.invisible)
        
        setLayout()
    }
    
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func enterPasswordVcDeletedAccount() {
        dismiss(animated: true, completion: {
            self.delegate?.myAccountVcSignedOut()
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuOneLineListCell", for: indexPath) as! MenuOneLineListCell
        cell.tag = indexPath.row
        cell.setItem(item: list[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = list[indexPath.row]
        if item.id == "my_info" {
            let vc = MyInfoVC()
            DispatchQueue.main.async(execute: {
                self.present(vc, animated: true, completion: nil)
            })
        } else if item.id == "sign_out" {
            let alert = UIAlertController(title: NSLocalizedString("AreYouSureYouWantToSignout", comment: ""), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: NSLocalizedString("SignOut", comment: ""), style: .default, handler: {
                (_) in
                if let error = FirebaseManager.shared.signOut() {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    self.dismiss(animated: true, completion: {
                        self.delegate?.myAccountVcSignedOut()
                    })
                }
            }))
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true, completion: nil)
            })
        } else if item.id == "delete_account" {
            let alert = UIAlertController(title: NSLocalizedString("AreYouSureYouWantToDeleteAccount", comment: ""), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: NSLocalizedString("DeleteAccount", comment: ""), style: .destructive, handler: {
                (_) in
                if FirebaseManager.shared.getUserData(dataType: .provider) == "password" {
                    let vc = EnterPasswordVC()
                    vc.delegate = self
                    self.present(vc, animated: true, completion: nil)
                } else {
                    self.loadingView.start()
                    
                    DispatchQueue.global().async(execute: {
                        User.deleteUser(result: {
                            error in
                            if let error = error {
                                let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                                DispatchQueue.main.async(execute: {
                                    self.present(alert, animated: true, completion: nil)
                                })
                            } else {
                                DispatchQueue.main.async(execute: {
                                    self.dismiss(animated: true, completion: {
                                        self.delegate?.myAccountVcSignedOut()
                                    })
                                })
                            }
                        })
                    })
                }
            }))
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true, completion: nil)
            })
        }
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        
        topView.showOnSubView(toView: view)
        topView.settitle(title: NSLocalizedString("Account", comment: ""))
        topView.showBackButton()
        topView.backButtonDelegate = self
        
        let tableViewSet = {
            self.view.addSubview(self.tableView)
            self.tableView.translatesAutoresizingMaskIntoConstraints = false
            self.tableView.register(MenuOneLineListCell.self, forCellReuseIdentifier: "MenuOneLineListCell")
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.rowHeight = UITableView.automaticDimension
            self.tableView.separatorStyle = .none
            self.tableView.backgroundColor = UIColor.clear
            
            let leading = NSLayoutConstraint(item: self.tableView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.tableView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.tableView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            let bottom = NSLayoutConstraint(item: self.tableView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        tableViewSet()
    }
}
