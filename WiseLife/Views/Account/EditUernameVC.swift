//
//  EditUernameVC.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/20.
//

import Foundation
import UIKit

protocol EditUsernameVcDelegate: class {
    func editUsernameVcDidChangeUsername(username: String)
}

class EditUsernameVC: UIViewController, UITextFieldDelegate, TopViewBackButtonDelegate, TopViewRightTextButtonDelegate, ClearFieldViewDidChangeDelegate {
    private var loadingView: LoadingView!
    private let topView = TopView()
    private var usernameFieldView: ClearFieldView!
    
    weak var delegate: EditUsernameVcDelegate?
    
    private var beforeUsername = ""
    
    deinit {
        print("Edit Username VC Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadingView = LoadingView(view: view)
        loadingView.setColor(color: Colors.shared.invisible)
        setLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        usernameFieldView.onBecome()
    }
    
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func pressRightTextButton() {
        view.endEditing(true)
        
        let text = usernameFieldView.getText().trimmingCharacters(in: .whitespacesAndNewlines)
        if text.count < SizesForLegnth.usernameMinimumLength {
            let alert = UIAlertController(title: NSLocalizedString("UsernameTooShort", comment: ""), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: {
                (_) in
                self.usernameFieldView.onBecome()
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        if text.count > SizesForLegnth.usernameMaximumLength {
            let alert = UIAlertController(title: NSLocalizedString("UsernameTooLong", comment: ""), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: {
                (_) in
                self.usernameFieldView.onBecome()
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        loadingView.start()
        
        DispatchQueue.global().async(execute: {
            User.editUsername(uname: text, result: {
                error in
                self.loadingView.stop()
                
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: {
                        (_) in
                        DispatchQueue.main.async(execute: {
                            self.usernameFieldView.onBecome()
                        })
                    }))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                } else {
                    KeyManager().isUpdateUsername = true
                    
                    DispatchQueue.main.async(execute: {
                        self.dismiss(animated: true, completion: {
                            self.delegate?.editUsernameVcDidChangeUsername(username: text)
                        })
                    })
                }
            })
        })
    }
    
    func setBeforeUsername(username: String) {
        beforeUsername = username
    }
    
    func clearFieldViewDidChange(textField: UITextField) {
        if textField.text?.count ?? 0 > SizesForLegnth.usernameMaximumLength {
            textField.text?.removeLast()
        }
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        
        topView.showOnSubView(toView: view)
        topView.settitle(title: NSLocalizedString("Username", comment: ""))
        topView.showBackButton()
        topView.backButtonDelegate = self
        topView.showRightTextButton(title: NSLocalizedString("Done", comment: ""))
        topView.rightTextButtonDelegate = self
        
        let usernameFieldViewSet = {
            self.usernameFieldView = ClearFieldView(vc: self)
            self.view.addSubview(self.usernameFieldView)
            self.usernameFieldView.translatesAutoresizingMaskIntoConstraints = false
            self.usernameFieldView.didChangeDelegate = self
            self.usernameFieldView.setText(text: self.beforeUsername)
            self.usernameFieldView.setTextFieldDelegate(delegate: self)
            self.usernameFieldView.setPlaceHoder(text: self.beforeUsername)
            self.usernameFieldView.setId(id: 0)
            
            let leading = NSLayoutConstraint(item: self.usernameFieldView!, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.usernameFieldView!, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            let top = NSLayoutConstraint(item: self.usernameFieldView!, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            NSLayoutConstraint.activate([leading, trailing, top])
        }
        usernameFieldViewSet()
    }
}
