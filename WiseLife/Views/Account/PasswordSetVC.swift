//
//  PasswordSetVC.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/12/26.
//

import Foundation
import UIKit

protocol PasswordSetVcDelegate: class {
    func passwordSetVcCompleteSignUp()
}

class PasswordSetVC: UIViewController {
    var loadingView: LoadingView!
    let topView = TopView()
    let contentView = UIView()
    let doneButton = UIButton(type: .system)
    let stackView = UIStackView()
    let password1Label = UILabel()
    let password1Field = UITextField()
    let password2Label = UILabel()
    let password2Field = UITextField()
    
    var isSet = false
    var contentViewBottomContraint: NSLayoutConstraint!
    weak var delegate: PasswordSetVcDelegate?
    
    deinit {
        if !isSet {
            FirebaseManager.shared.deleteUserForPassword(password: "wswsws", result: {
                error, isDelete  in
                print("remove account")
                print(FirebaseManager.shared.signOut() ?? "")
            })
        }
        print("Password Set Vc Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIWindow.keyboardWillShowNotification,
            object: nil)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: UIWindow.keyboardWillHideNotification,
            object: nil)
        
        loadingView = LoadingView(view: view)
        loadingView.setColor(color: Colors.shared.invisible)
        
        setLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        password1Field.becomeFirstResponder()
    }
    
    @objc private func keyboardWillShow(_ notificaiton: Notification) {
        if let keyboardFrame: NSValue = notificaiton.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardHeight = keyboardFrame.cgRectValue.height
            contentViewBottomContraint.constant = -keyboardHeight
            doneButton.isHidden = false
        }
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        contentViewBottomContraint.constant = 0
        doneButton.isHidden = true
    }
    
    @objc private func pressDoneButton(sender: UIButton) {
        done()
    }
    
    private func done() {
        let p = password1Field.text ?? ""
        let cp = password2Field.text ?? ""
        
        if p.count == 0 {
            password1Field.becomeFirstResponder()
        } else if cp.count == 0 {
            password2Field.becomeFirstResponder()
        } else if p != cp {
            let alert = UIAlertController(title: NSLocalizedString("SignupDoNotMatchPassword", comment: ""), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        } else if p.count > 20 {
            let alert = UIAlertController(title: NSLocalizedString("PleaseSetPasswordLessThan20", comment: ""), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        } else if p.count < 6 {
            let alert = UIAlertController(title: NSLocalizedString("WeakPasswordErrorMessage", comment: ""), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        } else if StringManager.findString(str: p, findStr: " ") {
            let alert = UIAlertController(title: NSLocalizedString("CannotUseSpace", comment: ""), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        } else {
            self.loadingView.start()
            
            FirebaseManager.shared.setPassword(password: p, result: {
                error in
                self.loadingView.stop()
                
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        self.completeSignUp()
                    })
                }
            })
        }
    }
    
    private func completeSignUp() {
        let alert = UIAlertController(title: NSLocalizedString("SignUpCompleteMessage", comment: ""), message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: {
            (_) in
            self.isSet = true
            self.dismiss(animated: true, completion: {
                self.delegate?.passwordSetVcCompleteSignUp()
            })
        }))
        present(alert, animated: true, completion: nil)
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        
        topView.show(toView: view)
        topView.settitle(title: NSLocalizedString("Password", comment: ""))
        topView.showBackButton()
        topView.backButtonDelegate = self
        
        let contentViewSet = {
            self.view.addSubview(self.contentView)
            self.contentView.translatesAutoresizingMaskIntoConstraints = false
            self.view.sendSubviewToBack(self.contentView)
            
            let leading = NSLayoutConstraint(item: self.contentView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.contentView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.contentView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0)
            self.contentViewBottomContraint = NSLayoutConstraint(item: self.contentView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top, self.contentViewBottomContraint])
        }
        contentViewSet()
        
        let stackViewSet = {
            self.contentView.addSubview(self.stackView)
            self.stackView.translatesAutoresizingMaskIntoConstraints = false
            self.stackView.addArrangedSubview(self.password1Label)
            self.stackView.addArrangedSubview(self.password1Field)
            self.stackView.addArrangedSubview(self.password2Label)
            self.stackView.addArrangedSubview(self.password2Field)
            self.stackView.axis = .vertical
            self.stackView.distribution = .fill
            self.stackView.spacing = Sizes.margin1
            
            let leading = NSLayoutConstraint(item: self.stackView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.stackView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            let centerY = NSLayoutConstraint(item: self.stackView, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, centerY])
        }
        stackViewSet()
        
        let passwordLabelSet = {
            self.password1Label.text = NSLocalizedString("Password", comment: "")
            self.password1Label.textColor = Colors.shared.text
            self.password1Label.font = UIFont.systemFont(ofSize: 12, weight: .medium)
            
            self.password2Label.text = NSLocalizedString("ConfirmPassword", comment: "")
            self.password2Label.textColor = Colors.shared.text
            self.password2Label.font = UIFont.systemFont(ofSize: 12, weight: .medium)
        }
        passwordLabelSet()
        
        let passwordFieldSet = {
            self.password1Field.tag = 1
            self.password1Field.translatesAutoresizingMaskIntoConstraints = false
            self.password1Field.delegate = self
            self.password1Field.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
            self.password1Field.isSecureTextEntry = true
            self.password1Field.returnKeyType = .next
            self.password1Field.layer.cornerRadius = 4
            self.password1Field.layer.borderWidth = 1
            self.password1Field.layer.borderColor = Colors.shared.border.cgColor
            self.password1Field.setLeftPadding(Sizes.margin1)
            self.password1Field.setRightPadding(Sizes.margin1)
            NSLayoutConstraint.activate([NSLayoutConstraint(item: self.password1Field, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 56)])
            
            self.password2Field.tag = 2
            self.password2Field.translatesAutoresizingMaskIntoConstraints = false
            self.password2Field.delegate = self
            self.password2Field.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
            self.password2Field.isSecureTextEntry = true
            self.password2Field.returnKeyType = .done
            self.password2Field.layer.cornerRadius = 4
            self.password2Field.layer.borderWidth = 1
            self.password2Field.layer.borderColor = Colors.shared.border.cgColor
            self.password2Field.setLeftPadding(Sizes.margin1)
            self.password2Field.setRightPadding(Sizes.margin1)
            NSLayoutConstraint.activate([NSLayoutConstraint(item: self.password2Field, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 56)])
        }
        passwordFieldSet()
        
        let doneButtonset = {
            self.contentView.addSubview(self.doneButton)
            self.doneButton.translatesAutoresizingMaskIntoConstraints = false
            self.doneButton.addTarget(self, action: #selector(self.pressDoneButton(sender:)), for: .touchUpInside)
            self.doneButton.setTitle(NSLocalizedString("Done", comment: ""), for: .normal)
            self.doneButton.setTitleColor(UIColor.white, for: .normal)
            self.doneButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
            self.doneButton.backgroundColor = Colors.shared.invisible
            self.doneButton.isEnabled = false
            
            let height = NSLayoutConstraint(item: self.doneButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 56)
            let leading = NSLayoutConstraint(item: self.doneButton, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.doneButton, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.doneButton, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([height, leading, trailing, bottom])
        }
        doneButtonset()
    }
}

extension PasswordSetVC: TopViewBackButtonDelegate {
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
}

extension PasswordSetVC: UITextFieldDelegate {
    @objc func textFieldDidChange(_ textField: UITextField) {
        let p = password1Field.text ?? ""
        let cp = password2Field.text ?? ""
        
        if p.count > 0 && cp.count > 0{
            doneButton.isEnabled = true
            doneButton.backgroundColor = Colors.shared.point
        } else {
            doneButton.isEnabled = false
            doneButton.backgroundColor = Colors.shared.invisible
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            password2Field.becomeFirstResponder()
        } else if textField.tag == 2 {
            done()
        }
        return false
    }
}
