//
//  EmailAuthVC.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/12/26.
//

import Foundation
import UIKit

protocol EmailAuthVcDelegate: class {
    func emailAuthVcAuthSuccess()
}

class EmailAuthVC: UIViewController {
    var loadingView: LoadingView!
    let contentView = UIView()
    let contentStackView = UIStackView()
    let authTitleLabel = UILabel()
    let authButtonStackView = UIStackView()
    let authCompleteButton = UIButton(type: .system)
    let authResendButton = UIButton(type: .system)
    let cancelButtonBackView = UIView()
    let cancelButton = UIButton(type: .system)
    
    weak var delegate: EmailAuthVcDelegate?
    
    deinit {
        print("Email Auth Vc Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadingView = LoadingView(view: view)
        loadingView.setColor(color: Colors.shared.invisible)
        
        setLayout()
    }
    
    @objc private func pressAuthCompleteButton(sender: UIButton) {
        self.loadingView.start()
        
        DispatchQueue.global().async(execute: {
            FirebaseManager.shared.reload(result: {
                error in
                self.loadingView.stop()
                
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                } else {
                    if FirebaseManager.shared.isVerificEmail() {
                        DispatchQueue.main.async(execute: {
                            self.dismiss(animated: true, completion: {
                                self.delegate?.emailAuthVcAuthSuccess()
                            })
                        })
                        return
                    }
                    
                    let alert = UIAlertController(title: NSLocalizedString("NotVerifiedEmailAuth", comment: ""), message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                }
            })
        })
    }
    
    @objc private func pressAuthResendButton(sender: UIButton) {
        
    }
    
    @objc private func pressCancelButton(sender: UIButton) {
        loadingView.start()
        
        FirebaseManager.shared.deleteUserForPassword(password: "wswsws", result: {
            error, isDelete  in
            self.loadingView.stop()
            
            print(FirebaseManager.shared.signOut() ?? "")
            
            DispatchQueue.main.async(execute: {
                self.dismiss(animated: true, completion: nil)
            })
        })
    }
    
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundAlpha
        
        let contentViewSet = {
            self.view.addSubview(self.contentView)
            self.contentView.translatesAutoresizingMaskIntoConstraints = false
            self.contentView.backgroundColor = Colors.shared.backgroundSub
            self.contentView.layer.cornerRadius = 16
            
            let leading = NSLayoutConstraint(item: self.contentView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.contentView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.contentView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 16)
            NSLayoutConstraint.activate([leading, trailing, bottom])
        }
        contentViewSet()
        
        let contentStackViewSet = {
            self.contentView.addSubview(self.contentStackView)
            self.contentStackView.translatesAutoresizingMaskIntoConstraints = false
            self.contentStackView.addArrangedSubview(self.authTitleLabel)
            self.contentStackView.addArrangedSubview(self.authButtonStackView)
            self.contentStackView.addArrangedSubview(self.cancelButton)
            self.contentStackView.axis = .vertical
            self.contentStackView.distribution = .fill
            self.contentStackView.spacing = 30
            
            let leading = NSLayoutConstraint(item: self.contentStackView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.contentStackView, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            let top = NSLayoutConstraint(item: self.contentStackView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: 30)
            let bottom = NSLayoutConstraint(item: self.contentStackView, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: -(Sizes.margin1+Sizes.bottomSpace))
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        contentStackViewSet()
        
        let authTitleLabelSet = {
            self.authTitleLabel.text = NSLocalizedString("SentVerifyEmailMessage", comment: "")
            self.authTitleLabel.textColor = Colors.shared.text
            self.authTitleLabel.textAlignment = .center
            self.authTitleLabel.font = Fonts.normal
            self.authTitleLabel.numberOfLines = 0
        }
        authTitleLabelSet()
        
        let authStackViewSet = {
            self.authButtonStackView.translatesAutoresizingMaskIntoConstraints = false
            self.authButtonStackView.addArrangedSubview(self.authCompleteButton)
            self.authButtonStackView.addArrangedSubview(self.authResendButton)
            self.authButtonStackView.axis = .horizontal
            self.authButtonStackView.distribution = .fillEqually
            self.authButtonStackView.spacing = Sizes.margin1*0.5
            
            let height = NSLayoutConstraint(item: self.authButtonStackView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
            NSLayoutConstraint.activate([height])
        }
        authStackViewSet()
        
        let authCompleteButtonSet = {
            self.authCompleteButton.addTarget(self, action: #selector(self.pressAuthCompleteButton(sender:)), for: .touchUpInside)
            self.authCompleteButton.setTitle(NSLocalizedString("Confirm", comment: ""), for: .normal)
            self.authCompleteButton.setTitleColor(UIColor.white, for: .normal)
            self.authCompleteButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
            self.authCompleteButton.layer.cornerRadius = 4
            self.authCompleteButton.backgroundColor = Colors.shared.point
        }
        authCompleteButtonSet()
        
        let authResendButtonSet = {
            self.authResendButton.addTarget(self, action: #selector(self.pressAuthResendButton(sender:)), for: .touchUpInside)
            self.authResendButton.setTitle(NSLocalizedString("ResendAuthEmail", comment: ""), for: .normal)
            self.authResendButton.setTitleColor(Colors.shared.text, for: .normal)
            self.authResendButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
            self.authResendButton.layer.cornerRadius = 4
            self.authResendButton.layer.borderWidth = 1
            self.authResendButton.layer.borderColor = Colors.shared.border.cgColor
            self.authResendButton.backgroundColor = Colors.shared.backgroundSub
        }
        authResendButtonSet()
        
        let cancelButtonSet = {
            self.cancelButton.addTarget(self, action: #selector(self.pressCancelButton(sender:)), for: .touchUpInside)
            self.cancelButton.setTitle(NSLocalizedString("Cancel", comment: ""), for: .normal)
            self.cancelButton.setTitleColor(Colors.shared.invisible, for: .normal)
            self.cancelButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        }
        cancelButtonSet()
    }
}
