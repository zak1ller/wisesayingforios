//
//  EnterPasswordVC.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/21.
//

import Foundation
import UIKit

protocol EnterPasswordVcDelegate: class {
    func enterPasswordVcDeletedAccount()
}

class EnterPasswordVC: UIViewController, UITextFieldDelegate, TopViewBackButtonDelegate {
    private var loadingView: LoadingView!
    private let topView = TopView()
    private var passwordFieldView: ClearFieldView!
    
    weak var delegate: EnterPasswordVcDelegate?
    
    deinit {
        print("Enter Password VC Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordFieldView = ClearFieldView(vc: self)
        loadingView = LoadingView(view: view)
        loadingView.setColor(color: Colors.shared.invisible)
        setLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        passwordFieldView.onBecome()
    }
    
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let text = textField.text ?? ""
        if text.count != 0 {
            loadingView.start()
            
            DispatchQueue.global().async(execute: {
                User.deleteUser(result: {
                    error in
                    if let error = error {
                        self.loadingView.stop()
                        
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                        return
                    }
                    
                    FirebaseManager.shared.deleteUserForPassword(password: text, result: {
                        error, isDelete  in
                        self.loadingView.stop()
                        
                        if let error = error {
                            let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                            DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true, completion: nil)
                            })
                        } else {
                            DispatchQueue.main.async(execute: {
                                self.dismiss(animated: true, completion: {
                                    self.delegate?.enterPasswordVcDeletedAccount()
                                })
                            })
                        }
                    })
                })
            })
        }
        return false
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        
        topView.showOnSubView(toView: view)
        topView.settitle(title: NSLocalizedString("DeleteAccount", comment: ""))
        topView.showBackButton()
        topView.backButtonDelegate = self
        
        let passwordFieldViewSet = {
            self.view.addSubview(self.passwordFieldView)
            self.passwordFieldView.translatesAutoresizingMaskIntoConstraints = false
            self.passwordFieldView.setId(id: 0)
            self.passwordFieldView.setPlaceHoder(text: NSLocalizedString("EnterYourPassword", comment: ""))
            self.passwordFieldView.onPasswordMode()
            self.passwordFieldView.setTextFieldDelegate(delegate: self)
            
            let top = NSLayoutConstraint(item: self.passwordFieldView!, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            let leading = NSLayoutConstraint(item: self.passwordFieldView!, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin1)
            let trailing = NSLayoutConstraint(item: self.passwordFieldView!, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([top, leading, trailing])
        }
        passwordFieldViewSet()
    }
}
