//
//  EditProfileVC.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/12/17.
//

import Foundation
import UIKit
import Alamofire
import AVFoundation
import Photos

class EditProfileVC: UIViewController {
    var picker = UIImagePickerController()
    var loadingView: LoadingView!
    let topView = TopView()
    let userImageView = UIImageView()
    let userImageChangeButton = UIButton(type: .system)
    let menuStackView = UIStackView()
    let nameMenu = TitleValueMenuView(item: TitleValueMenuView.Item(title: NSLocalizedString("Username", comment: ""), value: nil))
    let bioMenu = TitleValueMenuView(item: TitleValueMenuView.Item(title: NSLocalizedString("Bio", comment: ""), value: nil))
    let emailMenu = TitleValueMenuView(item: TitleValueMenuView.Item(title: NSLocalizedString("SignInEmail", comment: ""), value: nil))
    let providerMenu = TitleValueMenuView(item: TitleValueMenuView.Item(title: NSLocalizedString("SigninProvide", comment: ""), value: nil))

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadingView = LoadingView(view: view)
        loadingView.setColor(color: Colors.shared.invisible)
        
        setLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateImage()
        updateUsername()
        updateBio()
        updateEmail()
        updateProvider()
    }
    
    @objc private func pressChangeName() {
        let vc = EditUserNameVC()
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    
    @objc private func pressChangeBio() {
        let vc = EditBioVC()
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
   
    @objc private func pressUserImageChangeButton(sender: UIButton) {
        let alert = UIAlertController(title: NSLocalizedString("EditUserImage", comment: ""), message: nil, preferredStyle: ViewManager.actionSheetForiPad())
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: NSLocalizedString("RemoveCurrentPhoto", comment: ""), style: .destructive, handler: {
            (_) in
            self.loadingView.start()
            
            DispatchQueue.global().async(execute: {
                UserProfileImage.deleteCacheData()
            })
            
            DispatchQueue.global().async(execute: {
                User.deleteUserProfileImage(result: {
                    error in
                    self.loadingView.stop()
                    
                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        self.updateImage()
                    }
                })
            })
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("TakePhoto", comment: ""), style: .default, handler: {
            (_) in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("ChooseFrromLibrary", comment: ""), style: .default, handler: {
            (_) in
            self.openLibrary()
        }))
        present(alert, animated: true, completion: nil)
    }

    private func updateImage() {
        DispatchQueue.global().async(execute: {
            UserProfileImage.downloadProfileImage(uid: FirebaseManager.shared.getUserData(dataType: .uid) ?? "", result: {
                error, image in
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                    return
                }
                
                if let image = image {
                    DispatchQueue.main.async(execute: {
                        self.userImageView.image = image
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        self.userImageView.image = UIImage(named: "user.png")
                        self.userImageView.setImageColor(color: Colors.shared.invisible)
                    })
                }
            })
        })
    }
    
    private func updateUsername() {
        DispatchQueue.global().async(execute: {
            User.getUsername(uid: FirebaseManager.shared.getUserData(dataType: .uid) ?? "", result: {
                error, name in
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                    return
                }
                
                DispatchQueue.main.async(execute: {
                    self.nameMenu.setValue(value: name)
                })
            })
        })
    }
    
    private func updateBio() {
        DispatchQueue.global().async(execute: {
            User.getBio(uid: FirebaseManager.shared.getUserData(dataType: .uid) ?? "", result: {
                error, bio in
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                    return
                }
                
                DispatchQueue.main.async(execute: {
                    self.bioMenu.setValue(value: bio)
                })
            })
        })
    }
    
    private func updateProvider() {
        var provider = FirebaseManager.shared.getUserData(dataType: .provider)
        if provider == "password" {
            provider = "Email"
        }
        providerMenu.setValue(value: provider)
    }
    
    private func updateEmail() {
        emailMenu.setValue(value: FirebaseManager.shared.getUserData(dataType: .email))
    }
    
    private func openCamera() {
        AVCaptureDevice.requestAccess(for: .video, completionHandler: {
            res in
            if res {
                DispatchQueue.main.async(execute: {
                    self.picker = UIImagePickerController()
                    self.picker.delegate = self
                    self.picker.sourceType = .camera
                    self.picker.cameraCaptureMode = .photo
                    self.picker.cameraDevice = .rear
                    self.picker.allowsEditing = true
                    self.picker.delegate = self
                    self.present(self.picker, animated: true)
                })
            } else {
                let alert = UIAlertController(title: NSLocalizedString("RequestCameraPermissionMessage", comment: ""), message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default, handler: {
                    (_) in
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                        return
                    }
                    
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)")
                        })
                    }
                }))
                DispatchQueue.main.async(execute: {
                    self.present(alert, animated: true, completion: nil)
                })
            }
        })
    }
    
    private func openLibrary() {
        PHPhotoLibrary.requestAuthorization({
            status in
            switch status {
            case .authorized:
                DispatchQueue.main.async(execute: {
                    self.picker = UIImagePickerController()
                    self.picker.delegate = self
                    self.picker.sourceType = .photoLibrary
                    self.picker.allowsEditing = true
                    self.present(self.picker, animated: true, completion: nil)
                })
            case .denied:
                let alert = UIAlertController(title: NSLocalizedString("RequestLibraryPermissionMessage", comment: ""), message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default, handler: {
                    (_) in
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                        return
                    }
                    
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)")
                        })
                    }
                }))
                DispatchQueue.main.async(execute: {
                    self.present(alert, animated: true, completion: nil)
                })
            default:
                break
            }
        })
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        topView.show(toView: view)
        topView.settitle(title: NSLocalizedString("EditProfile", comment: ""))
        topView.showBackButton()
        topView.backButtonDelegate = self
        
        let userImageViewSet = {
            self.view.addSubview(self.userImageView)
            self.userImageView.translatesAutoresizingMaskIntoConstraints = false
            self.userImageView.image = UIImage(named: "user.png")
            self.userImageView.setImageColor(color: Colors.shared.invisible)
            self.userImageView.layer.cornerRadius = 50
            self.userImageView.layer.borderWidth = 1
            self.userImageView.layer.borderColor = Colors.shared.border.cgColor
            self.userImageView.clipsToBounds = true
            
            let width = NSLayoutConstraint(item: self.userImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 100)
            let height = NSLayoutConstraint(item: self.userImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 100)
            let top = NSLayoutConstraint(item: self.userImageView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.margin1)
            let centerX = NSLayoutConstraint(item: self.userImageView, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([width, height, top, centerX])
        }
        userImageViewSet()
        
        let userImageChangeButtonSet = {
            self.view.addSubview(self.userImageChangeButton)
            self.userImageChangeButton.translatesAutoresizingMaskIntoConstraints = false
            self.userImageChangeButton.addTarget(self, action: #selector(self.pressUserImageChangeButton(sender:)), for: .touchUpInside)
            self.userImageChangeButton.setTitle(NSLocalizedString("EditUserImage", comment: ""), for: .normal)
            self.userImageChangeButton.setTitleColor(Colors.shared.point, for: .normal)
            self.userImageChangeButton.titleLabel?.font = Fonts.button
            
            let centerX = NSLayoutConstraint(item: self.userImageChangeButton, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.userImageChangeButton, attribute: .top, relatedBy: .equal, toItem: self.userImageView, attribute: .bottom, multiplier: 1, constant: Sizes.margin1)
            NSLayoutConstraint.activate([centerX, top])
        }
        userImageChangeButtonSet()
        
        let menuStackViewSet = {
            self.view.addSubview(self.menuStackView)
            self.menuStackView.translatesAutoresizingMaskIntoConstraints = false
            self.menuStackView.addArrangedSubview(self.nameMenu)
            self.menuStackView.addArrangedSubview(self.bioMenu)
            self.menuStackView.addArrangedSubview(self.emailMenu)
            self.menuStackView.addArrangedSubview(self.providerMenu)
            self.menuStackView.axis = .vertical
            self.menuStackView.distribution = .fill
            
            self.nameMenu.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.pressChangeName)))
            self.bioMenu.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.pressChangeBio)))
            
            ViewManager.appendTopLine(toView: self.menuStackView, backgroundView: self.view, size: .normal, color: Colors.shared.border)
            ViewManager.appendUnderLine(toView: self.menuStackView, backgroundView: self.view, size: .normal, color: Colors.shared.border)
            
            let leading = NSLayoutConstraint(item: self.menuStackView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.menuStackView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.menuStackView, attribute: .top, relatedBy: .equal, toItem: self.userImageChangeButton, attribute: .bottom, multiplier: 1, constant: Sizes.margin1)
            NSLayoutConstraint.activate([leading, trailing, top])
        }
        menuStackViewSet()
    }
}

extension EditProfileVC: TopViewBackButtonDelegate {
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
}

extension EditProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: {
            guard let selectedImage = info[.originalImage] as? UIImage else {
                fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
            }
            
            self.loadingView.start()
            
            DispatchQueue.global().async(execute: {
                User.changeUserProfile(image: selectedImage, result: {
                    error in
                    self.loadingView.stop()
                    
                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        self.updateImage()
                    }
                })
            })
        })
    }
}

