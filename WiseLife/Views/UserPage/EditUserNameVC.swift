//
//  EditUserNameVC.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/12/22.
//

import Foundation
import UIKit

protocol EditUserNameVcDelegate: class {
    func editUserNameVcDidChangeUsername()
}

class EditUserNameVC: UIViewController {
    var loadingView: LoadingView!
    var textField: ClearFieldView!
    let topView = TopView()
    
    weak var delegate: EditUserNameVcDelegate?
    var beforeUsername: String?
    
    deinit {
        print("Edit Username VC Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getUsername()
    }
    
    private func changeUsername() {
        let text = textField.getText().trimmingCharacters(in: .whitespacesAndNewlines)
        
        if beforeUsername == text {
            dismiss(animated: true, completion: {
                self.delegate?.editUserNameVcDidChangeUsername()
            })
            return
        }
        
        if text.count < SizesForLegnth.usernameMinimumLength {
            let alert = UIAlertController(title: NSLocalizedString("UsernameTooShort", comment: ""), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        } else if text.count > SizesForLegnth.usernameMaximumLength {
            let alert = UIAlertController(title: NSLocalizedString("UsernameTooLong", comment: ""), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        } else {
            loadingView.start()
            
            DispatchQueue.global().async(execute: {
                User.editUsername(uname: text, result: {
                    error in
                    self.loadingView.stop()
                    
                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        DispatchQueue.main.async(execute: {
                            self.dismiss(animated: true, completion: {
                                self.delegate?.editUserNameVcDidChangeUsername()
                            })
                        })
                    }
                })
            })
        }
    }
    
    private func getUsername() {
        DispatchQueue.global().async(execute: {
            User.getUsername(uid: FirebaseManager.shared.getUserData(dataType: .uid) ?? "", result: {
                error, name in
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                }
                
                if let name = name {
                    DispatchQueue.main.async(execute: {
                        self.textField.setText(text: name)
                        self.textField.setPlaceHoder(text: name)
                    })
                }
                
                DispatchQueue.main.async(execute: {
                    self.textField.onBecome()
                })
                
                self.beforeUsername = name
            })
        })
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        
        topView.show(toView: view)
        topView.showBackButton()
        topView.backButtonDelegate = self
        topView.showRightTextButton(title: NSLocalizedString("Done", comment: ""))
        topView.rightTextButtonDelegate = self
        topView.settitle(title: NSLocalizedString("Username", comment: ""))
        
        loadingView = LoadingView(view: view)
        loadingView.setColor(color: Colors.shared.invisible)
        
        let textFieldSet = {
            self.textField = ClearFieldView(vc: self)
            self.view.addSubview(self.textField)
            self.textField.translatesAutoresizingMaskIntoConstraints = false
            self.textField.didChangeDelegate = self
            
            let top = NSLayoutConstraint(item: self.textField!, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            let leading = NSLayoutConstraint(item: self.textField!, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let trailing = NSLayoutConstraint(item: self.textField!, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            NSLayoutConstraint.activate([top, leading, trailing])
        }
        textFieldSet()
    }
}

extension EditUserNameVC: TopViewBackButtonDelegate, TopViewRightTextButtonDelegate {
    func pressRightTextButton() {
        changeUsername()
    }
    
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
}

extension EditUserNameVC: ClearFieldViewDidChangeDelegate {
    func clearFieldViewDidChange(textField: UITextField) {
        if var text = textField.text {
            // 크기를 초과한 경우 최대 크기가 될때까지 마지막 문자열을 지워준다.
            if text.count > SizesForLegnth.usernameMaximumLength {
                while text.count > SizesForLegnth.usernameMaximumLength {
                    text.removeLast()
                }
                textField.text = text
            }
            
        }
    }
}
