//
//  FollowerListVC.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/12/24.
//

import Foundation
import UIKit

class FollowListVC: UIViewController {
    let topView = TopView()
    let searchBar = UISearchBar()
    let tableView = UITableView()
    
    var followers: [User.Follow] = []
    var followings: [User.Follow] = []
    var searchReuslts: [User.Follow] = []
    var uid = ""
    var lastSearchText: String?
    var isSearch = false
    var isSearchTyping = false
    var searchLastEntered: Int64 = 0
    var mode: Mode = .follower
    
    enum Mode {
        case follower, following
    }
    
    deinit {
        print("Follower List VC Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateData(nil, completion: nil)
    }
    
    private func updateData(_ searchText: String?, completion: (() -> ())?) {
        searchReuslts.removeAll()
        
        DispatchQueue.global().async(execute: {
            User.getUserDatas(uid: self.uid, searchText: searchText, result: {
                error, userData in
                if let completion = completion {
                    completion()
                }
                
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                    return
                }
                
                if let userData = userData {
                    if searchText != nil {
                        switch self.mode {
                        case .follower:
                            self.searchReuslts = userData.followers
                        case .following:
                            self.searchReuslts = userData.followings
                        }
                    } else {
                        self.followers = userData.followers
                        self.followings = userData.followings
                    }
                }
                
                DispatchQueue.main.async(execute: {
                    self.tableView.reloadData()
                    
                    switch self.mode {
                    case .follower:
                        self.topView.settitle(title: "\(self.followers.count) \(NSLocalizedString("Followers", comment: ""))")
                    case .following:
                        self.topView.settitle(title: "\(self.followings.count) \(NSLocalizedString("Following", comment: ""))")
                    }
                })
            })
        })
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
    
        topView.show(toView: view)
        topView.showBackButton()
        topView.backButtonDelegate = self
        
        let tableViewSet = {
            self.view.addSubview(self.tableView)
            self.tableView.translatesAutoresizingMaskIntoConstraints = false
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.register(FollowListCell.self, forCellReuseIdentifier: "FollowListCell")
            self.tableView.rowHeight = UITableView.automaticDimension
            self.tableView.separatorStyle = .none
            self.tableView.backgroundColor = UIColor.clear
            
            let leading = NSLayoutConstraint(item: self.tableView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.tableView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.tableView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.tableView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        tableViewSet()
    }
}

extension FollowListVC: TopViewBackButtonDelegate {
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
}

extension FollowListVC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchLastEntered = TimeManager.getCurrentMilliseconds()
        
        if searchText.count == 0 {
            searchBar.showsCancelButton = false
            
            isSearch = false
            lastSearchText = nil
            tableView.reloadData()
        } else {
            searchBar.showsCancelButton = true
            
            isSearch = true
            lastSearchText = searchText
            search(time: TimeManager.getCurrentMilliseconds(), searchText: searchText)
        }
    }
    
    func search(time: Int64, searchText: String) {
        DispatchQueue.global().asyncAfter(deadline: DispatchTime.now()+0.5, execute: {
            if self.isSearch {
                if time == self.searchLastEntered {
                    self.updateData(searchText, completion: nil)
                }
            }
        })
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        searchBar.endEditing(true)
        
        isSearch = false
        lastSearchText = nil
        updateData(nil, completion: nil)
    }
}

extension FollowListVC: UITableViewDelegate, UITableViewDataSource, FollowListCellRemoveButtonDelegate, FollowListCellUserButtonDelegate, FollowListCellFollowButtonDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        searchBar.endEditing(true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearch {
            return searchReuslts.count
        } else {
            switch mode {
            case .follower:
                return followers.count
            case .following:
                return followings.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data: User.Follow
        if isSearch {
            data = searchReuslts[indexPath.row]
        } else {
            switch mode {
            case .follower:
                data = followers[indexPath.row]
            case .following:
                data = followings[indexPath.row]
            }
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FollowListCell", for: indexPath) as! FollowListCell
        cell.tag = indexPath.row
        
        switch mode {
        case .follower:
            cell.setData(infoUid: uid, data: data, mode: mode)
        case .following:
            cell.setData(infoUid: uid, data: data, mode: mode)
        }
        
        cell.userButtonDelegate = self
        cell.followButtonDelegate = self
        cell.removeButtonDelegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.addSubview(searchBar)
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.delegate = self
        searchBar.searchBarStyle = .minimal
        
        let leading = NSLayoutConstraint(item: searchBar, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 7)
        let trailing = NSLayoutConstraint(item: searchBar, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: -7)
        let top = NSLayoutConstraint(item: searchBar, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: Sizes.margin1)
        let bottom = NSLayoutConstraint(item: searchBar, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1)
        NSLayoutConstraint.activate([leading, trailing, top, bottom])
        return view
    }
    
    func followListCellPressUserButton(i: Int) {
        // 본인이 아닌 경우에만 유저 페이지로 이동한다.
        let targetUid: String
        switch mode {
        case .follower:
            targetUid = followers[i].uid
        case .following:
            targetUid = followings[i].tuid
        }
        
        if targetUid != FirebaseManager.shared.getUserData(dataType: .uid) {
            let vc = UserPageVC()
            vc.modalPresentationStyle = .fullScreen
            vc.uid = targetUid
            present(vc, animated: true, completion: nil)
        } else {
            SoundManager.vibrate(intensity: .medium)
        }
    }
    
    func followListCellPressFollowButton(i: Int) {
        let uid: String
        switch mode {
        case .follower:
            uid = followers[i].uid
        case .following:
            uid = followings[i].tuid
        }
        
        DispatchQueue.global().async(execute: {
            User.follow(tuid: uid, action: .follow, result: {
                error in
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                } else {
                    if let lastSearchText = self.lastSearchText {
                        self.updateData(lastSearchText, completion: nil)
                    } else {
                        self.updateData(nil, completion: nil)
                    }
                }
            })
        })
    }
    
    func followListCellPresssedRemoveButton(mode: FollowListCell.RemoveButtonMode, i: Int) {
        let uid: String
        switch self.mode {
        case .follower:
            uid = followers[i].uid
        case .following:
            uid = followings[i].tuid
        }
        
        switch mode {
        case .follow:
            DispatchQueue.global().async(execute: {
                User.follow(tuid: uid, action: .follow, result: {
                    error in
                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                        return
                    } else {
                        if let lastSearchText = self.lastSearchText {
                            self.updateData(lastSearchText, completion: nil)
                        } else {
                            self.updateData(nil, completion: nil)
                        }
                    }
                })
            })
        case .unfollow:
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: ViewManager.actionSheetForiPad())
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Unfollow", comment: ""), style: .default, handler: {
                (_) in
                DispatchQueue.global().async(execute: {
                    User.follow(tuid: uid, action: .unfollow, result: {
                        error in
                        if let error = error {
                            let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                            DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true, completion: nil)
                            })
                        } else {
                            if let lastSearchText = self.lastSearchText {
                                self.updateData(lastSearchText, completion: nil)
                            } else {
                                self.updateData(nil, completion: nil)
                            }
                        }
                    })
                })
            }))
            present(alert, animated: true, completion: nil)
        case .remove:
            let alert = UIAlertController(title: NSLocalizedString("RemoveFollower", comment: ""), message: nil, preferredStyle: ViewManager.actionSheetForiPad())
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
            
            if self.mode == .following {
                alert.addAction(UIAlertAction(title: NSLocalizedString("Unfollow", comment: ""), style: .default, handler: {
                    (_) in
                    DispatchQueue.global().async(execute: {
                        User.follow(tuid: uid, action: .unfollow, result: {
                            error in
                            if let error = error {
                                let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                                DispatchQueue.main.async(execute: {
                                    self.present(alert, animated: true, completion: nil)
                                })
                            } else {
                                if let lastSearchText = self.lastSearchText {
                                    self.updateData(lastSearchText, completion: nil)
                                } else {
                                    self.updateData(nil, completion: nil)
                                }
                            }
                        })
                    })
                }))
            }
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("Block", comment: ""), style: .destructive, handler: {
                (_) in
                DispatchQueue.global().async(execute: {
                    User.block(tuid: uid, action: .block, result: {
                        error in
                        if let error = error {
                            let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                            DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true, completion: nil)
                            })
                        } else {
                            if let lastSearchText = self.lastSearchText {
                                self.updateData(lastSearchText, completion: nil)
                            } else {
                                self.updateData(nil, completion: nil)
                            }
                        }
                    })
                })
            }))
            present(alert, animated: true, completion: nil)
        case .unblock:
            let alert = UIAlertController(title: NSLocalizedString("Unblock", comment: ""), message: nil, preferredStyle: ViewManager.actionSheetForiPad())
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .destructive, handler: {
                (_) in
                DispatchQueue.global().async(execute: {
                    User.block(tuid: uid, action: .unblock, result: {
                        error in
                        if let error = error {
                            let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                            DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true, completion: nil)
                            })
                        } else {
                            if let lastSearchText = self.lastSearchText {
                                self.updateData(lastSearchText, completion: nil)
                            } else {
                                self.updateData(nil, completion: nil)
                            }
                        }
                    })
                })
            }))
            present(alert, animated: true, completion: nil)
        }
    }
}
