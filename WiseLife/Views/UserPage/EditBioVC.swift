//
//  EditBioVC.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/12/22.
//

import Foundation
import UIKit

protocol EditBioVcDelegate: class {
    func editBioVcDidChange()
}

class EditBioVC: UIViewController {
    var loadingView: LoadingView!
    let textView = UITextView ()
    let topView = TopView()
    
    weak var delegate: EditBioVcDelegate?
    var textViewBottomConstraint: NSLayoutConstraint!
    
    var beforeBio: String?
    
    deinit {
        print("Edit Bio VC Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIWindow.keyboardWillShowNotification,
            object: nil)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: UIWindow.keyboardWillHideNotification,
            object: nil)
        
        setLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        textView.becomeFirstResponder()
        getBio()
    }
    
    @objc private func keyboardWillShow(_ notificaiton: Notification) {
        if let keyboardFrame: NSValue = notificaiton.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardHeight = keyboardFrame.cgRectValue.height
            textViewBottomConstraint.constant = -keyboardHeight
        }
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        textViewBottomConstraint.constant = 0
    }
    
    private func getBio() {
        DispatchQueue.global().async(execute: {
            User.getBio(uid: FirebaseManager.shared.getUserData(dataType: .uid) ?? "", result: {
                error, bio in
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                }
                
                if let bio = bio {
                    DispatchQueue.main.async(execute: {
                        self.textView.text = bio
                    })
                }
                
                self.beforeBio = bio
            })
        })
    }
    
    private func editBio() {
        var text = textView.text.trimmingCharacters(in: .whitespacesAndNewlines)
        text = StringManager.removeDoubleLine(text: text)
        
        if text == beforeBio {
            dismiss(animated: true, completion: {
                self.delegate?.editBioVcDidChange()
            })
            return
        }
        
        if text.count > SizesForLegnth.bioMaximum {
            let alert = UIAlertController(title: NSLocalizedString("BioTooLong", comment: ""), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        
        loadingView.start()
        
        DispatchQueue.global().async(execute: {
            User.editBio(bio: text, result: {
                error in
                self.loadingView.stop()
                
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        self.dismiss(animated: true, completion: {
                            self.delegate?.editBioVcDidChange()
                        })
                    })
                }
            })
        })
        
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        
        topView.show(toView: view)
        topView.showBackButton()
        topView.backButtonDelegate = self
        topView.showRightTextButton(title: NSLocalizedString("Done", comment: ""))
        topView.rightTextButtonDelegate = self
        topView.settitle(title: NSLocalizedString("Bio", comment: ""))
        
        loadingView = LoadingView(view: view)
        loadingView.setColor(color: Colors.shared.invisible)
        
        let textViewSet = {
            self.view.addSubview(self.textView)
            self.textView.translatesAutoresizingMaskIntoConstraints = false
            self.textView.delegate = self
            self.textView.font = Fonts.normal
            self.textView.textColor = Colors.shared.text
            self.textView.backgroundColor = UIColor.clear
            self.textView.textContainerInset = UIEdgeInsets.zero
            self.textView.textContainer.lineFragmentPadding = 0
            ViewManager.appendUnderLine(toView: self.textView, backgroundView: self.view, size: .bold, color: Colors.shared.border)
            
            let leading = NSLayoutConstraint(item: self.textView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let trailing = NSLayoutConstraint(item: self.textView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            let top = NSLayoutConstraint(item: self.textView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            self.textViewBottomConstraint = NSLayoutConstraint(item: self.textView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top, self.textViewBottomConstraint])
        }
        textViewSet()
    }
}

extension EditBioVC: TopViewBackButtonDelegate, TopViewRightTextButtonDelegate {
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func pressRightTextButton() {
        editBio()
    }
    
}

extension EditBioVC: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if var text = textView.text {
            // 크기를 초과한 경우 최대 크기가 될때까지 마지막 문자열을 지워준다.
            if text.count > SizesForLegnth.bioMaximum {
                while text.count > SizesForLegnth.bioMaximum {
                    text.removeLast()
                }
                textView.text = text
            }
            
        }
    }
}
