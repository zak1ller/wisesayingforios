//
//  UserPageVC.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/12/13.
//

import Foundation
import UIKit

class UserPageVC: UIViewController {
    let topView = TopView()
    let tableView = UITableView()
    
    var uid = ""
    var uname: String?
    var isReloadProfile = false
    var posts: [Post] = []
    
    deinit {
        print("User Page VC Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        isReloadProfile = false
        updateUid()
    }
    
    private func updateUid() {
        DispatchQueue.global().async(execute: {
            User.getUserDatas(uid: self.uid, searchText: nil, result: {
                error, data in
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                } else if let data = data {
                    self.uname = data.username
                    self.posts = data.postList
                    
                    DispatchQueue.main.async(execute: {
                        self.tableView.reloadData()
                        self.topView.settitle(title: data.username)
                    })
                }
            })
        })
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        
        topView.show(toView: view)
        topView.showBackButton()
        topView.backButtonDelegate = self
        topView.showRightImageButton(imagee: UIImage(named: "menu.png"))
        topView.rightImageButtonDelegate = self
        
        let tableViewSet = {
            self.view.addSubview(self.tableView)
            self.tableView.translatesAutoresizingMaskIntoConstraints = false
            self.tableView.register(MyCollectionListCell.self, forCellReuseIdentifier: "MyCollectionListCell")
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.rowHeight = UITableView.automaticDimension
            self.tableView.separatorStyle = .none
            self.tableView.backgroundColor = UIColor.clear
            
            let leading = NSLayoutConstraint(item: self.tableView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.tableView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.tableView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.tableView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        tableViewSet()
    }
}

extension UserPageVC: TopViewBackButtonDelegate, TopViewRightImageButtonDelegate {
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func pressRightImageButton() {
        let alert = UIAlertController(title: uname, message: nil, preferredStyle: ViewManager.actionSheetForiPad())
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        
        if FirebaseManager.shared.getUserData(dataType: .uid) ?? "" != uid {
            alert.addAction(UIAlertAction(title: NSLocalizedString("Report", comment: ""), style: .destructive, handler: {
                (_) in
                let vc = MenuVC()
                vc.delegate = self
                vc.modalPresentationStyle = .overFullScreen
                vc.setList(list: [
                    MenuVC.MenuItem(index: 0, id: "InappropriateUsername", title: NSLocalizedString("InappropriateUsername", comment: ""), image: nil),
                    MenuVC.MenuItem(index: 0, id: "SensationalUser", title: NSLocalizedString("SensationalUser", comment: ""), image: nil),
                    MenuVC.MenuItem(index: 0, id: "NoSourcePostUser", title: NSLocalizedString("NoSourcePostUser", comment: ""), image: nil),
                    MenuVC.MenuItem(index: 0, id: "DirectFeedback", title: NSLocalizedString("DirectFeedback", comment: ""), image: nil)
                ])
                self.present(vc, animated: true, completion: nil)
            }))
        }
        present(alert, animated: true, completion: nil)
    }
}

extension UserPageVC: MenuVcDelegate {
    func menuVcDidPress(id: String, i: Int) {
        if id == "InappropriateUsername" {
            DispatchQueue.global().async(execute: {
                Report.send(pid: nil, cid: nil, tuid: self.uid, group: .user, content: .InappropriateUsername, result: {
                    error in
                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        let alert = UIAlertController(title: NSLocalizedString("SubmittedSuccessfully", comment: ""), message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    }
                })
            })
        } else if id == "SensationalUser" {
            DispatchQueue.global().async(execute: {
                Report.send(pid: nil, cid: nil, tuid: self.uid, group: .user, content: .SensationalUser, result: {
                    error in
                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        let alert = UIAlertController(title: NSLocalizedString("SubmittedSuccessfully", comment: ""), message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    }
                })
            })
        } else if id == "NoSourcePostUser" {
            DispatchQueue.global().async(execute: {
                Report.send(pid: nil, cid: nil, tuid: self.uid, group: .user, content: .NoSourcePostUser, result: {
                    error in
                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        let alert = UIAlertController(title: NSLocalizedString("SubmittedSuccessfully", comment: ""), message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    }
                })
            })
        } else if id == "DirectFeedback" {
            let vc = DirectReportVC()
            vc.modalPresentationStyle = .fullScreen
            vc.tuid = uid
            vc.reportGroup = .user
            vc.reportContent = .DirectFeedback
            present(vc, animated: true, completion: nil)
        }
    }
}

extension UserPageVC: UITableViewDelegate, UITableViewDataSource, MyCollectionListCellDelegate {
    func myCollectionListCellDidPressFollowers(followers: [User.Follow]) {
        let vc = FollowListVC()
        vc.modalPresentationStyle = .fullScreen
        vc.uid = uid
        vc.mode = .follower
        present(vc, animated: true, completion: nil)
    }
    
    func myCollectionListCellDidPressFollowings(folowings: [User.Follow]) {
        let vc = FollowListVC()
        vc.modalPresentationStyle = .fullScreen
        vc.uid = uid
        vc.mode = .following
        present(vc, animated: true, completion: nil)
    }
    
    func myCollectionListCellDidPressMenuButton(i: Int) {
        
    }
    
    func myCollectionListCellDidPressLeftButton(action: MyCollectionListCell.ButtonAction) {
        if action == .editProfile {
            
        } else if action == .follow {
            DispatchQueue.global().async(execute: {
                User.follow(tuid: self.uid, action: .follow, result: {
                    error in
                    if let error = error {
                        let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        DispatchQueue.main.async(execute: {
                            self.tableView.reloadData()
                        })
                    }
                })
            })
        } else if action == .unfollow {
            let alert = UIAlertController(title: NSLocalizedString("Unfollow", comment: ""), message: nil, preferredStyle: ViewManager.actionSheetForiPad())
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .default, handler: {
                (_) in
                DispatchQueue.global().async(execute: {
                    User.follow(tuid: self.uid, action: .unfollow, result: {
                        error in
                        if let error = error {
                            let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                            DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true, completion: nil)
                            })
                        } else {
                            DispatchQueue.main.async(execute: {
                                self.tableView.reloadData()
                            })
                        }
                    })
                })
            }))
            present(alert, animated: true, completion: nil)
        } else if action == .iBlocked {
            let alert = UIAlertController(title: NSLocalizedString("Unblock", comment: ""), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: {
                (_) in
                DispatchQueue.global().async(execute: {
                    User.block(tuid: self.uid, action: .unblock, result: {
                        error in
                        if let error = error {
                            let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                            DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true, completion: nil)
                            })
                        } else {
                            DispatchQueue.main.async(execute: {
                                self.tableView.reloadData()
                            })
                        }
                    })
                })
            }))
            present(alert, animated: true, completion: nil)
        }
    }
    
    func myCollectionListCellDidPressRightButotn(action: MyCollectionListCell.ButtonAction) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count+1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCollectionListCell", for: indexPath) as! MyCollectionListCell
        if indexPath.row == 0 {
            cell.updateCellMode(mode: .user)
            cell.setUserData(uid: uid, result: {
                error in
                // cell size 초기화를 위해 유저 데이터 콜백시 테이블 리로드
                DispatchQueue.main.async(execute: {
                    if !self.isReloadProfile {
                        print("reload")
                        self.isReloadProfile = true
                        self.tableView.reloadData()
                    }
                })
            })
        } else {
            cell.updateCellMode(mode: .list)
        }
        
        cell.tag = indexPath.row
        cell.delegate = self
        
        if indexPath.row-1 >= 0 {
            // 1은 user content view에 쓰인 값이니 -1을 해줘야 리스트의 index를 정상적으로 맞출 수 있다.
            // row가 0인 경우 -1을 하면 오류가 발생하게 되므로 row-1이 0 이상인 경우에만 아래 구문이 실행되도록 한다.
            cell.setData(data: posts[indexPath.row-1])
        }
        
        return cell
    }
}
