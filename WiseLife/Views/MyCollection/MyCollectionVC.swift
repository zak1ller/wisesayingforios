//
//  MyCollectionVC.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/10.
//

import Foundation
import UIKit

class MyCollectionVC: UIViewController, UITableViewDelegate, UITableViewDataSource, TopViewRightImageButtonDelegate, TopViewLeftImageButtonDelegate, MyCollectionListCellDelegate, MenuVcDelegate, MyAccountVcDelegate, SignInVcDelegate, UpdateSignInStateDelegate {
    private var loadingView: LoadingView!
    private let contentStackView = UIStackView()
    private var topView: TopView!
    private let tableView = UITableView()
    private let refreshControl = UIRefreshControl()
    
    private var list: [Post] = []
    private var isReloadProfile = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topView = TopView()
        loadingView = LoadingView(view: view)
        loadingView.setColor(color: Colors.shared.invisible)
        
        setLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        isReloadProfile = false
        if FirebaseManager.shared.isSignedIn() == .success {
            reload()
        } else {
            updateSignInCheckSignIn()
        }
    }
    
    func myAccountVcDidClose() {
        if KeyManager().isUpdateUsername {
            KeyManager().isUpdateUsername = false
            tableView.reloadData()
        }
    }
    
    func myAccountVcSignedOut() {
        let vc = SignInVC()
        vc.delegate = self
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func signInVcSignedIn() {
        reload()
    }
    
    func pressLeftImageButton() {
        let vc = MenuVC()
        vc.modalPresentationStyle = .overFullScreen
        vc.delegate = self
        vc.setList(list: [
            MenuVC.MenuItem(id: "account", title: NSLocalizedString("Account", comment: ""), image: UIImage(named: "menu_account.png")),
            MenuVC.MenuItem(id: "privacy", title: NSLocalizedString("PrivacyPolicy", comment: ""), image: UIImage(named: "menu_security.png"))
        ])
        present(vc, animated: false, completion: nil)
    }
    
    func pressRightImageButton() {
        let vc = PlaygroundPostingViewController()
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    
    func menuVcDidPress(id: String, i: Int) {
        if id == "account" {
            let vc = MyAccountVC()
            vc.delegate = self
            present(vc, animated: true, completion: nil)
        } else if id == "privacy" {
            
        }
    }
    
    func updateSignInCheckSignIn() {
        self.loadingView.start()
        
        DispatchQueue.global().async(execute: {
            FirebaseManager.shared.checkSignInState(result: {
                error, res in
                self.loadingView.stop()
                
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                    return
                }
            
                if res == .needSignIn {
                    DispatchQueue.main.async(execute: {
                        let vc = SignInVC()
                        vc.delegate = self
                        vc.modalPresentationStyle = .fullScreen
                        self.present(vc, animated: true, completion: nil)
                    })
                }
            })
        })
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if refreshControl.isRefreshing {
            reload()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count+1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCollectionListCell", for: indexPath) as! MyCollectionListCell
        
        if indexPath.row == 0 {
            cell.updateCellMode(mode: .user)
            cell.setUserData(uid: FirebaseManager.shared.getUserData(dataType: .uid) ?? "", result: {
                error in
                // cell size 초기화를 위해 유저 데이터 콜백시 테이블 리로드
                DispatchQueue.main.async(execute: {
                    if !self.isReloadProfile {
                        self.tableView.reloadData()
                        self.isReloadProfile = true
                    }
                })
            })
        } else {
            cell.updateCellMode(mode: .list)
        }
        
        cell.tag = indexPath.row
        cell.delegate = self
        
        if indexPath.row-1 >= 0 {
            // 1은 user content view에 쓰인 값이니 -1을 해줘야 리스트의 index를 정상적으로 맞출 수 있다.
            // row가 0인 경우 -1을 하면 오류가 발생하게 되므로 row-1이 0 이상인 경우에만 아래 구문이 실행되도록 한다.
            cell.setData(data: list[indexPath.row-1])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row-1 >= 0 {
            let item = list[indexPath.row-1]
            let vc = MyCollectionShowDetailVC()
            vc.setData(data: item)
            DispatchQueue.main.async(execute: {
                self.present(vc, animated: true, completion: nil)
            })
        }
    }
    
    func myCollectionListCellDidPressFollowers(followers: [User.Follow]) {
        let vc = FollowListVC()
        vc.modalPresentationStyle = .fullScreen
        vc.uid = FirebaseManager.shared.getUserData(dataType: .uid) ?? ""
        vc.mode = .follower
        present(vc, animated: true, completion: nil)
    }
    
    func myCollectionListCellDidPressFollowings(folowings: [User.Follow]) {
        let vc = FollowListVC()
        vc.modalPresentationStyle = .fullScreen
        vc.uid = FirebaseManager.shared.getUserData(dataType: .uid) ?? ""
        vc.mode = .following
        present(vc, animated: true, completion: nil)
    }
    
    func myCollectionListCellDidPressLeftButton(action: MyCollectionListCell.ButtonAction) {
        if action == .editProfile {
            let vc = EditProfileVC()
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
        }
    }
    
    func myCollectionListCellDidPressRightButotn(action: MyCollectionListCell.ButtonAction) {
        
    }
    
    func myCollectionListCellDidPressMenuButton(i: Int) {
        let item = list[i-1]
        let alert = UIAlertController(title: item.content, message: nil, preferredStyle: ViewManager.actionSheetForiPad())
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: NSLocalizedString("ShareButtonText", comment: ""), style: .default, handler: {
            (_) in
            let text = "\(item.content)"
            let textShare = [ text ]
            let activityViewController = UIActivityViewController(activityItems: textShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .destructive, handler: {
            (_) in
            self.loadingView.start()
            
            DispatchQueue.global().async(execute: {
                item.delete(result: {
                    error in
                    self.loadingView.stop()
                    
                    if let error = error {
                        let alert = UIAlertController(title: NSLocalizedString(error, comment: ""), message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        self.list.remove(at: i-1)
                        
                        DispatchQueue.main.async(execute: {
                            self.tableView.reloadData()
                        })
                    }
                })
            })
        }))
        present(alert, animated: true, completion: nil)
    }
    
    private func reload() {
        refreshControl.beginRefreshing()
        
        DispatchQueue.global().asyncAfter(deadline: DispatchTime.now()+0.5, execute: {
            Post.getMyList(result: {
                error, list in
                DispatchQueue.main.async(execute: {
                    if self.refreshControl.isRefreshing {
                        self.refreshControl.endRefreshing()
                    }
                })
                
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                }
                
                if let list = list {
                    self.list = list
                    
                    DispatchQueue.main.async(execute: {
                        self.tableView.reloadData()
                    })
                }
            })
        })
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        
        topView.show(toView: view)
        topView.settitle(title: "MY")
        topView.showLeftImageButton(image: UIImage(named: "list.png"))
        topView.showRightImageButton(imagee: UIImage(named: "search.png"))
        topView.leftImageButtonDelegate = self
        topView.rightImageButtonDelegate = self
        
        let contentStackViewSet = {
            self.view.addSubview(self.contentStackView)
            self.contentStackView.translatesAutoresizingMaskIntoConstraints = false
            self.contentStackView.addArrangedSubview(self.tableView)
            self.contentStackView.axis = .vertical
            self.contentStackView.distribution = .fill
            
            let leading = NSLayoutConstraint(item: self.contentStackView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.contentStackView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.contentStackView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.contentStackView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        contentStackViewSet()
        
        let tableViewSet = {
            self.tableView.register(MyCollectionListCell.self, forCellReuseIdentifier: "MyCollectionListCell")
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.rowHeight = UITableView.automaticDimension
            self.tableView.separatorStyle = .none
            self.tableView.backgroundColor = UIColor.clear
            self.tableView.refreshControl = self.refreshControl
        }
        tableViewSet()
    }
}
