//
//  MyCollectionShowDetailVC.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/15.
//

import Foundation
import UIKit

class MyCollectionShowDetailVC: UIViewController, TopViewBackButtonDelegate {
    private let topView = TopView()
    private let contentLabel = UILabel()
    private let hashtagsLabel = UILabel()
    
    private var data: Post?
    
    deinit {
        print("My Collection Show Detail VC Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
    }
    
    func setData(data: Post) {
        self.data = data
    }
    
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        
        topView.showOnSubView(toView: view)
        topView.showBackButton()
        topView.backButtonDelegate = self
        
        let contentLabelSet = {
            self.view.addSubview(self.contentLabel)
            self.contentLabel.translatesAutoresizingMaskIntoConstraints = false
            self.contentLabel.text = self.data?.content
            self.contentLabel.textColor = Colors.shared.text
            self.contentLabel.font = Fonts.normal
            self.contentLabel.numberOfLines = 0
            
            let leading = NSLayoutConstraint(item: self.contentLabel, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let trailing = NSLayoutConstraint(item: self.contentLabel, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            let top = NSLayoutConstraint(item: self.contentLabel, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            NSLayoutConstraint.activate([leading, trailing, top])
        }
        contentLabelSet()
        
        let hashtagsLabelSet = {
            self.view.addSubview(self.hashtagsLabel)
            self.hashtagsLabel.translatesAutoresizingMaskIntoConstraints = false
            
            if let hashtags = self.data?.hashtags {
                self.hashtagsLabel.text = StringManager.makeHashtagString(hashtags: hashtags)
            }
            
            self.hashtagsLabel.textColor = Colors.shared.invisible
            self.hashtagsLabel.font = Fonts.normal
            self.hashtagsLabel.numberOfLines = 0
            
            let leading = NSLayoutConstraint(item: self.hashtagsLabel, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let trailing = NSLayoutConstraint(item: self.hashtagsLabel, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            let top = NSLayoutConstraint(item: self.hashtagsLabel, attribute: .top, relatedBy: .equal, toItem: self.contentLabel, attribute: .bottom, multiplier: 1, constant: Sizes.margin1*2)
            NSLayoutConstraint.activate([leading, trailing, top])
        }
        hashtagsLabelSet()
    }
}
