//
//  PlaygroundViewController.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/12/30.
//

import Foundation
import UIKit

class PlaygroundViewController: UIViewController {
    var loadingView: LoadingView!
    let topView = TopView()
    let tableView = UITableView()
    let refreshControl = UIRefreshControl()
    let writePostButton = UIButton(type: .system)
    
    var page = 0
    var list: [PlaygroundPost] = []
    var isFirstAppear = true
    var isLastPage = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadingView = LoadingView(view: view)
        setLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isFirstAppear {
            isFirstAppear = false
            updateList(true, nil)
        } 
    }
    
    @objc private func pressWritePostButton(sender: UIButton) {
        let vc = PlaygroundPostingViewController()
        vc.modalPresentationStyle = .fullScreen
        vc.delegate = self
        vc.isEditMode = false
        present(vc, animated: true, completion: nil)
    }
    
    private func updateList(_ isReload: Bool, _ completion: ((_ res: Bool) -> ())?) {
        print("update list")
        
        if isReload {
            page = 0
            list.removeAll()
            isLastPage = true
        }
        
        DispatchQueue.global().async(execute: {
            PlaygroundPost.getList(page: self.page, result: {
                error, data, isLastPage  in
                if let completion = completion {
                    completion(true)
                }
                
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                    return
                }
                
                self.isLastPage = isLastPage
                
                if let data = data {
                    self.list.append(contentsOf: data)
                }
                
                DispatchQueue.main.async(execute: {
                    self.tableView.reloadData()
                    
                    if isReload {
                        // 화면이 초기화 되었기에 리스트를 최상단으로 이동한다.
                        if self.list.count > 0 {
                            self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                        }
                    }
                })
            })
        })
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        
        topView.show(toView: view)
        topView.settitle(title: NSLocalizedString("Playground", comment: ""))
        
        let tableViewSet = {
            self.view.addSubview(self.tableView)
            self.tableView.translatesAutoresizingMaskIntoConstraints = false
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.register(PlaygroundListCell.self, forCellReuseIdentifier: "PlaygroundListCell")
            self.tableView.rowHeight = UITableView.automaticDimension
            self.tableView.separatorStyle = .none
            self.tableView.backgroundColor = UIColor.clear
            self.tableView.refreshControl = self.refreshControl
            self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: Sizes.button+Sizes.margin1))
            
            let leading = NSLayoutConstraint(item: self.tableView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.tableView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.tableView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.tableView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        tableViewSet()
        
        let writePostButtonSet = {
            self.view.addSubview(self.writePostButton)
            self.writePostButton.translatesAutoresizingMaskIntoConstraints = false
            self.writePostButton.addTarget(self, action: #selector(self.pressWritePostButton(sender:)), for: .touchUpInside)
            self.writePostButton.layer.cornerRadius = Sizes.button*0.5
            self.writePostButton.setImage(UIImage(named: "pencil.png"), for: .normal)
            self.writePostButton.tintColor = UIColor.white
            self.writePostButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            self.writePostButton.backgroundColor = Colors.shared.point
            
            let width = NSLayoutConstraint(item: self.writePostButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
            let hegiht = NSLayoutConstraint(item: self.writePostButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: Sizes.button)
            let trailing = NSLayoutConstraint(item: self.writePostButton, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin1)
            let bottom = NSLayoutConstraint(item: self.writePostButton, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: -Sizes.margin1)
            NSLayoutConstraint.activate([width, hegiht, trailing, bottom])
        }
        writePostButtonSet()
    }
}

extension PlaygroundViewController: PlaygroundPostingViewControllerDelegate {
    func playgroundPostingViewControllerDidPost() {
        updateList(true, nil)
    }
    
    func playgroundPostingViewControllerDidEdit(i: Int, after: String) {
        list[i].content = after
        tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: .none)
    }
}

extension PlaygroundViewController: UITableViewDelegate, UITableViewDataSource, PlaygroundListCellDelegate {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if refreshControl.isRefreshing {
            self.tableView.isUserInteractionEnabled = false
            DispatchQueue.global().asyncAfter(deadline: DispatchTime.now()+1, execute: {
                self.updateList(true, { _ in
                    DispatchQueue.main.async(execute: {
                        self.tableView.isUserInteractionEnabled = true
                        self.refreshControl.endRefreshing()
                    })
                })
            })
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaygroundListCell", for: indexPath) as! PlaygroundListCell
        cell.tag = indexPath.row
        cell.delegate = self
        cell.setData(data: list[indexPath.row])
        
        if indexPath.row == list.count-1 {
            if !isLastPage {
                page += 1
                updateList(false, nil)
            }
        }
        return cell
    }
    
    func playgroundListCellDoubleTab(i: Int) {
        updateLike(i: i)
    }
    
    func playgroundListCellPressLikeButton(i: Int) {
        updateLike(i: i)
    }
    
    func playgroundListCellPressDetailButton(i: Int) {
        let item = list[i]
        if item.isShowAllText {
            list[i].isShowAllText = false
        } else {
            list[i].isShowAllText = true
        }
        tableView.reloadData()
    }
    
    func playgroundListCellLongPress(i: Int) {
        let item = list[i]
        let alert = UIAlertController(title: item.uname, message: item.content, preferredStyle: ViewManager.actionSheetForiPad())
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        
        if item.uid == FirebaseManager.shared.getUserData(dataType: .uid) ?? "" {
            alert.addAction(UIAlertAction(title: NSLocalizedString("Edit", comment: ""), style: .default, handler: {
                (_) in
                let vc = PlaygroundPostingViewController()
                vc.modalPresentationStyle = .fullScreen
                vc.delegate = self
                vc.setToEditData(data: item, i: i)
                self.present(vc, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .destructive, handler: {
                (_) in
                self.loadingView.start()
                
                DispatchQueue.global().async(execute: {
                    PlaygroundPost.delete(item: item, result: {
                        error in
                        self.loadingView.stop()
                        
                        if let error = error {
                            let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                            DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true, completion: nil)
                            })
                        } else {
                            DispatchQueue.main.async(execute: {
                                self.list.remove(at: i)
                                self.tableView.reloadData()
                            })
                        }
                    })
                })
            }))
        }
        present(alert, animated: true, completion: nil)
    }
    
    func playgroundListCellPressUserPageButton(i: Int) {
        let item = list[i]
        let alert = UIAlertController(title: item.uname, message: nil, preferredStyle: ViewManager.actionSheetForiPad())
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: item.uname, style: .default, handler: {
            (_) in
            let vc = UserPageVC()
            vc.modalPresentationStyle = .fullScreen
            vc.uid = item.uid
            self.present(vc, animated: true, completion: nil)
        }))
        if item.uid != FirebaseManager.shared.getUserData(dataType: .uid) ?? "" {
            alert.addAction(UIAlertAction(title: NSLocalizedString("Report", comment: ""), style: .destructive, handler: {
                (_) in
                let vc = MenuVC()
                vc.delegate = self
                vc.modalPresentationStyle = .overFullScreen
                vc.setList(list: [
                    MenuVC.MenuItem(index: i, id: "InappropriateUsername", title: NSLocalizedString("InappropriateUsername", comment: ""), image: nil),
                    MenuVC.MenuItem(index: i, id: "SensationalUser", title: NSLocalizedString("SensationalUser", comment: ""), image: nil),
                    MenuVC.MenuItem(index: i, id: "NoSourcePostUser", title: NSLocalizedString("NoSourcePostUser", comment: ""), image: nil),
                    MenuVC.MenuItem(index: i, id: "DirectFeedback", title: NSLocalizedString("DirectFeedback", comment: ""), image: nil)
                ])
                self.present(vc, animated: false, completion: nil)
            }))
        }
        present(alert, animated: true, completion: nil)
    }
    
    private func updateLike(i: Int) {
        let item = list[i]
        var likeAction = true
        
        if item.isLike {
            likeAction = false
            list[i].likesCount = list[i].likesCount-1
        } else {
            list[i].likesCount = list[i].likesCount+1
        }
        
        list[i].isLike = likeAction
        
        tableView.reloadData()
        
        DispatchQueue.global().async(execute: {
            PlaygroundPost.likePost(item: item, isLike: likeAction, result: {
                error in
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                }
            })
        })
    }
}

extension PlaygroundViewController: MenuVcDelegate {
    func menuVcDidPress(id: String, i: Int) {
        let item = list[i]
        if id == "InappropriateUsername" {
            sendReport(tuid: item.uid, group: .user, content: .InappropriateUsername)
        } else if id == "SensationalUser" {
            sendReport(tuid: item.uid, group: .user, content: .SensationalUser)
        } else if id == "NoSourcePostUser" {
            sendReport(tuid: item.uid, group: .user, content: .NoSourcePostUser)
        } else if id == "DirectFeedback" {
            let vc = DirectReportVC()
            vc.modalPresentationStyle = .fullScreen
            vc.tuid = item.uid
            vc.reportGroup = .user
            vc.reportContent = .DirectFeedback
            present(vc, animated: true, completion: nil)
        }
    }
    
    private func sendReport(tuid: String, group: Report.ReportGroup, content: Report.ReportContent) {
        DispatchQueue.global().async(execute: {
            Report.send(pid: nil, cid: nil, tuid: tuid, group: group, content: content, result: {
                error in
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                } else {
                    let alert = UIAlertController(title: NSLocalizedString("SubmittedSuccessfully", comment: ""), message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                }
            })
        })
    }
}
