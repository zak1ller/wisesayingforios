//
//  SinglePlaygroundViewController.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2021/01/28.
//

import Foundation
import UIKit

class SinglePlaygroundViewController: UIViewController {
    var loadingView: LoadingView!
    let topView = TopView()
    let tableView = UITableView()
    
    var list: [PlaygroundPost] = []
    var pid: String?
    
    deinit {
        print("SinglePlaygroundViewController")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadingView = LoadingView(view: view)
        loadingView.setColor(color: Colors.shared.invisible)
        
        setLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        update()
    }
    
    private func update() {
        list.removeAll()
        
        DispatchQueue.global().async(execute: {
            PlaygroundPost.getSinglePost(pid: self.pid ?? "", result: {
                error, item in
                if let error = error {
                    let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    DispatchQueue.main.async {
                        self.present(alert, animated: true, completion: nil)
                    }
                } else if let item = item {
                    self.list.append(item)
                    DispatchQueue.main.async(execute: {
                        self.tableView.reloadData()
                    })
                } else {
                    let alert = UIAlertController(title: NSLocalizedString("MyCollectionNoExistData", comment: ""), message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: {
                        (_) in
                        DispatchQueue.main.async(execute: {
                            self.dismiss(animated: true, completion: nil)
                        })
                    }))
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                }
            })
        })
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.backgroundSub
        
        topView.show(toView: view)
        topView.showBackButton()
        topView.backButtonDelegate = self
        
        let tableViewSet = {
            self.view.addSubview(self.tableView)
            self.tableView.translatesAutoresizingMaskIntoConstraints = false
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.register(PlaygroundListCell.self, forCellReuseIdentifier: "PlaygroundListCell")
            self.tableView.rowHeight = UITableView.automaticDimension
            self.tableView.separatorStyle = .none
            self.tableView.backgroundColor = UIColor.clear
            self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: Sizes.button+Sizes.margin1))
            
            let leading = NSLayoutConstraint(item: self.tableView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.tableView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.tableView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: 0)
            let bottom = NSLayoutConstraint(item: self.tableView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top, bottom])
        }
        tableViewSet()
    }
}

extension SinglePlaygroundViewController: TopViewBackButtonDelegate {
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
}

extension SinglePlaygroundViewController: PlaygroundPostingViewControllerDelegate {
    func playgroundPostingViewControllerDidPost() {
        
    }
    
    func playgroundPostingViewControllerDidEdit(i: Int, after: String) {
        list[0].content = after
        tableView.reloadData()
    }
}

extension SinglePlaygroundViewController: UITableViewDelegate, UITableViewDataSource, PlaygroundListCellDelegate {
    func playgroundListCellDoubleTab(i: Int) {
        
    }
    
    func playgroundListCellPressUserPageButton(i: Int) {
        
    }
    
    func playgroundListCellPressDetailButton(i: Int) {
        
    }
    
    func playgroundListCellPressLikeButton(i: Int) {
        
    }
    
    func playgroundListCellLongPress(i: Int) {
        let item = list[i]
        let alert = UIAlertController(title: item.uname, message: item.content, preferredStyle: ViewManager.actionSheetForiPad())
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        
        if item.uid == FirebaseManager.shared.getUserData(dataType: .uid) ?? "" {
            alert.addAction(UIAlertAction(title: NSLocalizedString("Edit", comment: ""), style: .default, handler: {
                (_) in
                let vc = PlaygroundPostingViewController()
                vc.modalPresentationStyle = .fullScreen
                vc.delegate = self
                vc.setToEditData(data: item, i: i)
                self.present(vc, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .destructive, handler: {
                (_) in
                self.loadingView.start()
                
                DispatchQueue.global().async(execute: {
                    PlaygroundPost.delete(item: item, result: {
                        error in
                        self.loadingView.stop()
                        
                        if let error = error {
                            let alert = UIAlertController(title: error, message: nil, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                            DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true, completion: nil)
                            })
                        } else {
                            DispatchQueue.main.async(execute: {
                                self.list.remove(at: i)
                                self.tableView.reloadData()
                                
                                DispatchQueue.main.async(execute: {
                                    self.dismiss(animated: true, completion: nil)
                                })
                            })
                        }
                    })
                })
            }))
        }
        present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaygroundListCell", for: indexPath) as! PlaygroundListCell
        cell.tag = indexPath.row
        cell.delegate = self
        cell.setData(data: list[indexPath.row])
        return cell
    }
}
