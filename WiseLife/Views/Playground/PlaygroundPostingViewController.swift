//
//  NewMyConllection.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/11.
//

import Foundation
import UIKit

protocol PlaygroundPostingViewControllerDelegate: class {
    func playgroundPostingViewControllerDidPost()
    func playgroundPostingViewControllerDidEdit(i: Int, after: String)
}

class PlaygroundPostingViewController: UIViewController {
    let topView = TopView()
    var loadingView: LoadingView!
    var hideKeyboardButton: HideKeyboardButton!
    let backgroundTouchView = UIView()
    let contentTextView = UITextView()
    var contentTextViewPlaceHolder = UILabel()
    
    weak var delegate: PlaygroundPostingViewControllerDelegate?
    
    var keyboardHideBottomConstraint: NSLayoutConstraint!
    var contentTextViewBottomConstrat: NSLayoutConstraint!
    var toEditData: PlaygroundPost?
    var toEditIndex = 0
    var isEditMode = false
    
    deinit {
        print("Playground Postring vc Deinited.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadingView = LoadingView(view: view)
        loadingView.setColor(color: Colors.shared.invisible)
        
        hideKeyboardButton = HideKeyboardButton(vc: self)
        hideKeyboardButton.isHidden = true
        
        setLayout()
        
        if let data = toEditData {
            contentTextView.text = data.content
        } else {
            contentTextViewPlaceHolder.text = NSLocalizedString("InputContent", comment: "")
        }
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIWindow.keyboardWillShowNotification,
            object: nil)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: UIWindow.keyboardWillHideNotification,
            object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !isEditMode {
            contentTextView.becomeFirstResponder()
        }
    }
    
    @objc private func keyboardWillShow(_ notificaiton: Notification) {
        if let keyboardFrame: NSValue = notificaiton.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardHeight = keyboardFrame.cgRectValue.height
            DispatchQueue.main.async(execute: {
                self.hideKeyboardButton.isHidden = false
            })
            keyboardHideBottomConstraint.constant = -keyboardHeight
            contentTextViewBottomConstrat.constant = -keyboardHeight
        }
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        DispatchQueue.main.async(execute: {
            self.hideKeyboardButton.isHidden = true
        })
        keyboardHideBottomConstraint.constant = 0
        contentTextViewBottomConstrat.constant = 0
    }
    
    @objc private func pressBackgroundTouchView() {
        view.endEditing(true)
    }
    
    @objc private func pressHideKeyboardButton(sender: UIButton) {
        view.endEditing(true)
    }
    
    func setToEditData(data: PlaygroundPost?, i: Int) {
        if let data = data {
            toEditIndex = i
            toEditData = data
            isEditMode = true
        } else {
            isEditing = false
        }
    }
    
    private func completeForEdit() {
        let newContent = contentTextView.text ?? "".trimmingCharacters(in: .whitespacesAndNewlines)
        if newContent == toEditData?.content {
            // 내용이 동일할 경우 서버에 요청하지 않고 끝냄
            dismiss(animated: true, completion: nil)
            return
        }
    
        
        loadingView.start()
    
        loadingView.start()
        
        DispatchQueue.global().async(execute: {
            PlaygroundPost.post(item: self.toEditData, content: newContent, result: {
                error, blockString  in
                self.loadingView.stop()
                
                if let error = error {
                    let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                    
                    if let blockString = blockString {
                        alert.title = error
                        alert.message = "'\(blockString)'"
                    } else {
                        alert.title = error
                        alert.message = nil
                    }
                    
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true, completion: nil)
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        self.dismiss(animated: true, completion: {
                            if self.isEditMode {
                                self.delegate?.playgroundPostingViewControllerDidEdit(i: self.toEditIndex, after: newContent)
                            } else {
                                self.delegate?.playgroundPostingViewControllerDidPost()
                            }
                        })
                    })
                }
            })
        })
    }
    
    private func complete() {
        view.endEditing(true)
        
        let text = StringManager.removeMultipleLine(text: contentTextView.text.trimmingCharacters(in: .whitespacesAndNewlines))
        if text.count < SizesForLegnth.wsContentMinimumLength {
            let alert = UIAlertController(title: NSLocalizedString("ContentShortMessage", comment: ""), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: {
                (_) in
                self.contentTextView.becomeFirstResponder()
            }))
            present(alert, animated: true , completion: nil)
        } else if text.count > SizesForLegnth.wsContentMaximumLength {
            let alert = UIAlertController(title: NSLocalizedString("ContentLongMessage", comment: ""), message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: {
                (_) in
                self.contentTextView.becomeFirstResponder()
            }))
            present(alert, animated: true , completion: nil)
        } else {
            if isEditMode {
                completeForEdit()
                return
            }
            
            loadingView.start()
            
            DispatchQueue.global().async(execute: {
                PlaygroundPost.post(item: nil, content: text, result: {
                    error, blockString  in
                    self.loadingView.stop()
                    
                    if let error = error {
                        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: ""), style: .cancel, handler: nil))
                        
                        if let blockString = blockString {
                            alert.title = error
                            alert.message = "'\(blockString)'"
                        } else {
                            alert.title = error
                            alert.message = nil
                        }
                        
                        DispatchQueue.main.async(execute: {
                            self.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        DispatchQueue.main.async(execute: {
                            self.dismiss(animated: true, completion: {
                                self.delegate?.playgroundPostingViewControllerDidPost()
                            })
                        })
                    }
                })
            })
        }
    }
    
    private func setLayout() {
        view.backgroundColor = Colors.shared.background
        
        topView.show(toView: view)
        topView.settitle(title: NSLocalizedString("NewPost", comment: ""))
        topView.showBackButton()
        topView.backButtonDelegate = self
        topView.showRightTextButton(title: NSLocalizedString("Done", comment: ""))
        topView.rightTextButtonDelegate = self
        
        let backgroundTouchViewSet = {
            self.view.addSubview(self.backgroundTouchView)
            self.backgroundTouchView.translatesAutoresizingMaskIntoConstraints = false
            self.backgroundTouchView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.pressBackgroundTouchView)))
            
            let leading = NSLayoutConstraint(item: self.backgroundTouchView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.backgroundTouchView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.backgroundTouchView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            let bottom = NSLayoutConstraint(item: self.backgroundTouchView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing,top, bottom])
        }
        backgroundTouchViewSet()
        
        let contentTextFieldSet = {
            self.view.addSubview(self.contentTextView)
            self.contentTextView.translatesAutoresizingMaskIntoConstraints = false
            self.contentTextView.delegate = self
            self.contentTextView.textColor = Colors.shared.text
            self.contentTextView.font = Fonts.normal
            self.contentTextView.backgroundColor = Colors.shared.background
            self.contentTextView.textContainerInset = UIEdgeInsets.zero
            self.contentTextView.textContainer.lineFragmentPadding = 0
            
            let leading = NSLayoutConstraint(item: self.contentTextView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: Sizes.margin3)
            let trailing = NSLayoutConstraint(item: self.contentTextView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -Sizes.margin3)
            let top = NSLayoutConstraint(item: self.contentTextView, attribute: .top, relatedBy: .equal, toItem: self.topView, attribute: .bottom, multiplier: 1, constant: Sizes.marginTop)
            self.contentTextViewBottomConstrat = NSLayoutConstraint(item: self.contentTextView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, top, self.contentTextViewBottomConstrat])
        }
        contentTextFieldSet()

        let contentTextViewPlaceHolderSet = {
            self.view.addSubview(self.contentTextViewPlaceHolder)
            self.contentTextViewPlaceHolder.translatesAutoresizingMaskIntoConstraints = false
            self.contentTextViewPlaceHolder.textColor = Colors.shared.invisible
            self.contentTextViewPlaceHolder.font = Fonts.normal
            
            let leading = NSLayoutConstraint(item: self.contentTextViewPlaceHolder, attribute: .leading, relatedBy: .equal, toItem: self.contentTextView, attribute: .leading, multiplier: 1, constant: 0)
            let top = NSLayoutConstraint(item: self.contentTextViewPlaceHolder, attribute: .top, relatedBy: .equal, toItem: self.contentTextView, attribute: .top, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, top])
        }
        contentTextViewPlaceHolderSet()
        
        let hideKeyboardButtonSet = {
            self.view.addSubview(self.hideKeyboardButton)
            self.hideKeyboardButton.translatesAutoresizingMaskIntoConstraints = false
            self.hideKeyboardButton.addTarget(self, action: #selector(self.pressHideKeyboardButton(sender:)), for: .touchUpInside)
            
            let leading = NSLayoutConstraint(item: self.hideKeyboardButton!, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1, constant: 0)
            let trailing = NSLayoutConstraint(item: self.hideKeyboardButton!, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
            self.keyboardHideBottomConstraint = NSLayoutConstraint(item: self.hideKeyboardButton!, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0)
            NSLayoutConstraint.activate([leading, trailing, self.keyboardHideBottomConstraint])
        }
        hideKeyboardButtonSet()
    }
}

extension PlaygroundPostingViewController: TopViewBackButtonDelegate, TopViewRightTextButtonDelegate {
    func pressBackButton() {
        dismiss(animated: true, completion: nil)
    }
    
    func pressRightTextButton() {
        complete()
    }
}

extension PlaygroundPostingViewController: UITextViewDelegate, ClearFieldViewDidChangeDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.count == 0 {
            contentTextViewPlaceHolder.isHidden  = false
        } else {
            contentTextViewPlaceHolder.isHidden  = true
        }
        
        if contentTextView.text.count > SizesForLegnth.wsContentMaximumLength {
            contentTextView.text.removeLast()
        }
    }
    
    func clearFieldViewDidChange(textField: UITextField) {
        
    }
}
