//
//  User.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/10.
//

import Foundation
import UIKit
import Alamofire
import AlamofireImage

class User {
    struct Follow {
        var uid: String
        var tuid: String
        var date: String
        
        init(uid: String, tuid: String, date: String) {
            self.uid = uid
            self.tuid = tuid
            self.date = date
        }
    }
    
    struct UserData {
        let username: String
        let bio: String?
        let likes: Int
        let posts: Int
        let followers: [Follow]
        let followings: [Follow]
        let postList: [Post]
        let blockMe: Bool
        let iBlock: Bool
        
        init(username: String, bio: String?, likes: Int, posts: Int, followers: [Follow], followings: [Follow], postList: [Post], blockMe: Bool = false, iBlock: Bool = false) {
            self.username = username
            self.bio = bio
            self.likes = likes
            self.posts = posts
            self.followers = followers
            self.postList = postList
            self.blockMe = blockMe
            self.iBlock = iBlock
            self.followings = followings
        }
    }
    
    enum FollowAction: String {
        case follow = "follow"
        case unfollow = "unfollow"
    }
    
    enum BlockAction: String {
        case block = "block"
        case unblock = "unblock"
    }
    
    static func isVaildEmailAddress(email: String, result: @escaping (_ error: String?, _ res: Bool) -> ()) {
        var dic: [String: Any] = [:]
        dic.updateValue(email, forKey: "email_address")
    
        let urlString = "https://onevoca.org/ws/email_is_vaild.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", false)
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, false)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error), false)
                        return
                    }

                    if let res = resultData["res"] as? Bool {
                        result(nil, res)
                    }
                } else {
                    result("???", false)
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""), false)
            }
        })
        task.resume()
    }
    
    static func isFollow(uid: String, tuid: String, result: @escaping (_ error: String?, _ isFollow: Bool, _ isBlock: Bool) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""), false, false)
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(uid, forKey: "uid")
        dic.updateValue(tuid, forKey: "tuid")
        
        let urlString = "https://onevoca.org/ws/follow_is_following.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", false, false)
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, false, false)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error), false, false)
                        return
                    }

                    let follow = resultData["follow"] as? Bool ?? false
                    let block = resultData["block"] as? Bool ?? false
                    result(nil, follow, block)
                } else {
                    result("???", false, false)
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""), false, false)
            }
        })
        task.resume()
    }
    
    static func block(tuid: String, action: BlockAction, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        dic.updateValue(tuid, forKey: "tuid")
        dic.updateValue(action.rawValue, forKey: "action")
        
        let urlString = "https://onevoca.org/ws/user_block.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error))
                        return
                    }

                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    }
                } else {
                    result("???")
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""))
            }
        })
        task.resume()
    }
    
    static func follow(tuid: String, action: FollowAction, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        dic.updateValue(tuid, forKey: "tuid")
        dic.updateValue(action.rawValue, forKey: "action")
        
        let urlString = "https://onevoca.org/ws/user_follow.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error))
                        return
                    }
                    
                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    }
                } else {
                    result("???")
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""))
            }
        })
        task.resume()
    }
    
    static func getProfileImageLastUpdate(uid: String, result: @escaping (_ error: String?, _ date: String?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""), nil)
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(uid, forKey: "uid")
        
        let urlString = "https://onevoca.org/ws/user_profile_get_last_update_date.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", nil)
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, nil)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error), nil)
                        return
                    }

                    if let date = resultData["date"] as? String {
                        result(nil, date)
                    } else {
                        result(nil, nil)
                    }
                } else {
                    result("???", nil)
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""), nil)
            }
        })
        task.resume()
    }
    
    static func getProfileImage(uid: String, result: @escaping (_ error: String?, _ image: UIImage?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""), nil)
            return
        }
        
        if let url = URL(string: "https://onevoca.org/ws/user_profile_images/\(uid).jpg") {
            let urlRequest = URLRequest(url: url)
            URLCache.shared.removeCachedResponse(for: urlRequest)
            
            AF.request(urlRequest).responseImage(completionHandler: {
                response in
                switch response.result {
                case .success(let image):
                    result(nil, image.af.imageAspectScaled(toFit: CGSize(width: 150, height: 150)))
                case .failure(_):
                    result(nil, nil)
                }
            })
        } else {
            result(nil, nil)
        }
    }
    
    static func getUserDatas(uid: String, searchText: String?, result: @escaping (_ error: String?, _ data: UserData?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""), nil)
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        dic.updateValue(uid, forKey: "tuid")
        
        if let searchText = searchText {
            dic.updateValue(searchText, forKey: "search_text")
        }
        
        let urlString = "https://onevoca.org/ws/user_get_information.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", nil)
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, nil)
                return
            }
            
            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error), nil)
                        return
                    }
                    
                    let uname: String
                    let bio: String?
                    var postCount = 0
                    var blockMe = false
                    var iBlock = false
                    
                    if let user = resultData["user"] as? NSDictionary {
                        uname = user["uname"] as? String ?? ""
                        bio = user["bio"] as? String ?? nil
                        blockMe = user["block_me"] as? Bool ?? false
                        iBlock = user["i_block"] as? Bool ?? false
                        postCount = user["post_count"] as? Int ?? 0
                    } else {
                        uname = "Unknown"
                        bio = nil
                    }
            
                    var followerList: [Follow] = []
                    if let followerData = resultData["followers"] as? Array<NSDictionary> {
                        for value in followerData {
                            followerList.append(Follow(uid: value["uid"] as? String ?? "", tuid: value["tuid"] as? String ?? "", date: value["date"] as? String ?? ""))
                        }
                    }
                    
                    var followingList: [Follow] = []
                    if let followingData = resultData["followings"] as? Array<NSDictionary> {
                        for value in followingData {
                            followingList.append(Follow(uid: value["uid"] as? String ?? "", tuid: value["tuid"] as? String ?? "", date: value["date"] as? String ?? ""))
                        }
                    }
        
                    var dataList: [Post] = []
                    var likeCount = 0
                    if let posts = resultData["posts"] as? Array<NSDictionary> {
                        for value in posts {
                            let id = value["id"] as? String ?? ""
                            let uid = value["uid"] as? String ?? ""
                            let uname = value["uname"] as? String ?? "Unknown"
                            let content = value["content"] as? String ?? ""
                            let date = value["date_reg"] as? String ?? "unknown"
                            
                            let likes = value["likes"] as? Int ?? 0
                            likeCount += likes
                            var tags: [String] = []
                            
                            if let hashtags = value["hashtags"] as? Array<NSDictionary> {
                                for value in hashtags {
                                    tags.append(value["value"] as? String ?? "")
                                }
                            }
                            
                            let d = Post(content: content, hashtags: tags, id: id, uid: uid, likes: likes, username: uname, date: date)
                            dataList.append(d)
                        }
                    }
                    result(nil, UserData(username: uname, bio: bio, likes: likeCount, posts: postCount, followers: followerList, followings: followingList, postList: dataList, blockMe: blockMe, iBlock: iBlock))
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""), nil)
            }
        })
        task.resume()
    }
    
    static func deleteUserProfileImage(result: @escaping (_ error: String?) -> ()) {
        guard let uid = FirebaseManager.shared.getUserData(dataType: .uid) else {
            result(NSLocalizedString("UidErrorMessage", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(uid, forKey: "uid")
        
        let urlString = "https://onevoca.org/ws/user_profile_delete_image.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
    
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error))
                        return
                    }

                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    }
                } else {
                    result("???")
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""))
            }
        })
        task.resume()
    }
    
    static func changeUserProfile(image: UIImage, result: @escaping (_ error: String?) -> ()) {
        let resizedImage = image.af.imageAspectScaled(toFill: CGSize(width: 150, height: 150))
        
        guard let uid = FirebaseManager.shared.getUserData(dataType: .uid) else {
            result(NSLocalizedString("UidErrorMessage", comment: ""))
            return
        }
        
        guard let imageData = resizedImage.jpegData(compressionQuality: 1) else {
            result("Image error.")
            return
        }

        guard let fileName = FirebaseManager.shared.getUserData(dataType: .uid) else {
            result(NSLocalizedString("UidErrorMessage", comment: ""))
            return
        }
        
        let url = "https://onevoca.org/ws/user_profile_image.php?uid=\(uid)"
        
        AF.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData, withName: "image", fileName: fileName, mimeType: "image/jpg")
        }, to: url).responseJSON { response in
            switch response.result {
            case .success(let json):
                if let data = json as? NSDictionary {
                    if let query = data["query"] as? String {
                        print(query)
                    }
                    
                    if let error = data["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error))
                        return
                    }
                    if let _ = data["success"] as? Bool {
                        result(nil)
                    }
                } else {
                    result("json parsing error.")
                }
            case .failure(let error):
                result(error.localizedDescription)
            }
        }
    }
    
    static func editBio(bio: String, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        dic.updateValue(bio, forKey: "bio")
        
        let urlString = "https://onevoca.org/ws/user_edit_bio.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error))
                        return
                    }

                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    }
                } else {
                    result("???")
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""))
            }
        })
        task.resume()
    }
    
    static func editUsername(uname: String, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        dic.updateValue(uname.trimmingCharacters(in: .whitespacesAndNewlines), forKey: "uname")
        
        let urlString = "https://onevoca.org/ws/user_edit_username.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error))
                        return
                    }

                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    }
                } else {
                    result("???")
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""))
            }
        })
        task.resume()
    }
    
    static func getBio(uid: String, result: @escaping (_ error: String?, _ bio: String?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""), nil)
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(uid, forKey: "uid")
        
        let urlString = "https://onevoca.org/ws/user_get_bio.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", nil)
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, nil)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error), nil)
                        return
                    }

                    if let bio = resultData["bio"] as? String {
                        result(nil, bio)
                    } else {
                        result(nil, nil)
                    }
                } else {
                    result("???", nil)
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""), nil)
            }
        })
        task.resume()
    }
    
    static func getUsername(uid: String, result: @escaping (_ error: String?, _ name: String?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""), nil)
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(uid, forKey: "uid")
        
        let urlString = "https://onevoca.org/ws/user_get_username.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", nil)
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, nil)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error), nil)
                        return
                    }

                    if let name = resultData["name"] as? String {
                        result(nil, name)
                    } else {
                        result("Unknown error.", nil)
                    }
                } else {
                    result("???", nil)
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""), nil)
            }
        })
        task.resume()
    }
    
    static func deleteUser(result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        
        let urlString = "https://onevoca.org/ws/delete_user.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error))
                        return
                    }

                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    }
                } else {
                    result("???")
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""))
            }
        })
        task.resume()
    }
    
    static func signinUpdate(result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .email) ?? "", forKey: "email")
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .provider) ?? "", forKey: "provider")
        dic.updateValue(UIDevice.current.modelName, forKey: "device")
        
        let urlString = "https://onevoca.org/ws/signin_user.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)

                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error))
                        return
                    }

                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    }
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""))
            }
        })
        task.resume()
    }
}
