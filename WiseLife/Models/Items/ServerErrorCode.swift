//
//  ServerErrorCode.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/14.
//

import Foundation

class ServerErrorCode {
    static let wrongUid = 0
    static let noExistUser = 1
    static let wrongAccess = 2
    static let wrongQuery = 3
    
    static let postAlreadyUse = 100
    static let postNoExist = 101
    static let postBlockString = 102
    
    static let contentShort = 200
    static let contentLong = 201
    static let contentOrverTry = 208
    static let commentShort = 205
    static let commentLong = 206
    static let commentOverTry = 207
    static let reportOverTry = 209
    static let reportShort = 210
    static let reportLong = 211
    
    static let usernameShort = 300
    static let usernameLong = 301
    static let usernameContainSpeiclChar = 302
    static let usernameOccupied = 303
    static let usernameBlockString = 304
    static let bioLong = 305
    static let bioBlockString = 306
    
    static let userProfileImageNoImage = 1000;
    static let userProfileImageNoname = 1001;
    static let userProfileImageUploadFailure = 1002;
    
    static func convertErrorCode(errorCode: Int) -> String {
        if errorCode == wrongUid {
            return NSLocalizedString("UidErrorMessage", comment: "")
        } else if errorCode == wrongQuery || errorCode == wrongAccess {
            return NSLocalizedString("QueryErrorMessage", comment: "")
        } else if errorCode == noExistUser {
            return NSLocalizedString("NoExistUserErrorMessage", comment: "")
        } else if errorCode == postAlreadyUse {
            return NSLocalizedString("MyCollectionIsAlreadyErrorMessage", comment: "")
        } else if errorCode == postNoExist {
            return NSLocalizedString("MyCollectionNoExistData", comment: "")
        } else if errorCode == postBlockString {
            return NSLocalizedString("PostBlockStringError", comment: "")
        } else if errorCode == contentShort {
            return NSLocalizedString("ContentShortMessage", comment: "")
        } else if errorCode == contentLong {
            return NSLocalizedString("ContentLongMessage", comment: "")
        } else if errorCode == contentOrverTry {
            return NSLocalizedString("ContentOverTryMesssage", comment: "")
        } else if errorCode == commentShort {
            return NSLocalizedString("CommentTooShort", comment: "")
        } else if errorCode == commentLong {
            return NSLocalizedString("CommentTooLong", comment: "")
        } else if errorCode == commentOverTry {
            return NSLocalizedString("CommentOverTryWarning", comment: "")
        } else if errorCode == usernameShort {
            return NSLocalizedString("UsernameTooShort", comment: "")
        } else if errorCode == usernameLong {
            return NSLocalizedString("UsernameTooLong", comment: "")
        } else if errorCode == usernameContainSpeiclChar {
            return NSLocalizedString("UsernameContainSpecialChar", comment: "")
        } else if errorCode == usernameOccupied {
            return NSLocalizedString("UsernameOccupiedMessage", comment: "")
        } else if errorCode == usernameBlockString {
            return NSLocalizedString("UsernameBlockStringMessage", comment: "")
        } else if errorCode == bioLong {
            return NSLocalizedString("BioTooLong", comment: "")
        } else if errorCode == bioBlockString {
            return NSLocalizedString("BlockStringMessage", comment: "")
        } else if errorCode == reportOverTry {
            return NSLocalizedString("DefaultOverTryMessage", comment: "")
        } else if errorCode == reportShort {
            return NSLocalizedString("ContentShortMessage", comment: "")
        } else if errorCode == reportLong {
            return NSLocalizedString("ContentLongMessage", comment: "")
        } else if errorCode == userProfileImageNoname {
            return NSLocalizedString("userProfileImageNoname", comment: "")
        } else if errorCode == userProfileImageNoImage {
            return NSLocalizedString("userProfileImageNoImage", comment: "")
        } else if errorCode == userProfileImageUploadFailure {
            return NSLocalizedString("userProfileImageUploadFailure", comment: "")
        } else {
            return "\(errorCode)"
        }
    }
}
