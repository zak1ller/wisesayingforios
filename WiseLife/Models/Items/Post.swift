//
//  MyCollection.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/14.
//

import Foundation

class Post {
    var id: String
    var uid: String
    var username: String
    var content: String
    var hashtags: [String]?
    var likes: Int
    var date: String
    
    enum LikeState {
        case like, unlike
    }
    
    struct Comments {
        var id: String
        var pid: String
        var uid: String
        var cid: String?
        var content: String
        var date: String
        var username: String
        var isReply = false
    }
    
    init(content: String, hashtags: [String]?, id: String, uid: String, likes: Int, username: String, date: String) {
        self.content = content
        self.hashtags = hashtags
        self.id = id
        self.uid = uid
        self.likes = likes
        self.username = username
        self.date = date
    }
    
    static func removeComment(comment: Comments, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        dic.updateValue(comment.id, forKey: "id")
        
        if let cid = comment.cid {
            dic.updateValue(cid, forKey: "cid")
        }
       
        let urlString = "https://onevoca.org/ws/comment_remove.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)

                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error))
                        return
                    }

                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    }
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""))
            }
        })
        task.resume()
    }
    
    func getComments(result: @escaping (_ error: String?, _ comments: [Comments]) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""), [])
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        dic.updateValue(id, forKey: "pid")
        
        let urlString = "https://onevoca.org/ws/comment_get_from_post.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", [])
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, [])
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error), [])
                        return
                    }

                    if let _ = resultData["no_data"] as? Bool {
                        result(nil, [])
                        return
                    }
                }
                
                if let arr = jsonData as? Array<NSDictionary> {
                    var dataList: [Comments] = []
                    
                    for value in arr {
                        let id = value["id"] as? String ?? ""
                        let pid = value["pid"] as? String ?? ""
                        let uid = value["uid"] as? String ?? ""
                        let cid = value["cid"] as? String ?? nil
                        let content = value["content"] as? String ?? ""
                        let date = value["date"] as? String ?? ""
                        let username = value["username"] as? String ?? "Unknown"
                        dataList.append(Comments(id: id, pid: pid, uid: uid, cid: cid, content: content, date: date, username: username))
                    }
                    result(nil, dataList)
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""), [])
            }
        })
        task.resume()
    }
    
    func addComment(comment: String, tagTextCount: Int, cid: String?, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        dic.updateValue(id, forKey: "pid")
        dic.updateValue(comment, forKey: "comment")
        dic.updateValue(tagTextCount, forKey: "tag_text_count")
        
        if let cid = cid {
            dic.updateValue(cid, forKey: "cid")
        } 
       
        let urlString = "https://onevoca.org/ws/comment_add.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)

                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error))
                        return
                    }

                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    }
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""))
            }
        })
        task.resume()
    }
    
    func isLikeThisPost(result: @escaping (_ error: String?, _ isLike: Bool) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""), false)
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        dic.updateValue(id, forKey: "pid")
        
        let urlString = "https://onevoca.org/ws/like_check_post.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", false)
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, false)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)

                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error), false)
                        return
                    }

                    if let isLike = resultData["is_like"] as? Bool {
                        result(nil, isLike)
                    }
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""), false)
            }
        })
        task.resume()
    }
    
    func like(state: LikeState, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        dic.updateValue(id, forKey: "pid")
        
        switch state {
        case .like:
            dic.updateValue(1, forKey: "like")
        case .unlike:
            dic.updateValue(-1, forKey: "like")
        }
        
        let urlString = "https://onevoca.org/ws/like.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)

                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error))
                        return
                    }

                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    }
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""))
            }
        })
        task.resume()
    }
    
    func edit(newContent: String?, newHashtags: [String]?, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        dic.updateValue(id, forKey: "id")
        
        if let newContent = newContent {
            dic.updateValue(newContent, forKey: "new_content")
        }
        
        if let newHashtags = newHashtags {
            dic.updateValue(newHashtags, forKey: "new_hashtags")
        }
        
        let urlString = "https://onevoca.org/ws/mycollection_edit.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)

                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error))
                        return
                    }

                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    }
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""))
            }
        })
        task.resume()
    }
    
    func delete(result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        dic.updateValue(id, forKey: "id")
        
        let urlString = "https://onevoca.org/ws/mycollection_delete.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)

                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error))
                        return
                    }

                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    }
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""))
            }
        })
        task.resume()
    }
    
    static func getMyList(result: @escaping (_ error: String?, _ data: [Post]?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""), nil)
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        
        let urlString = "https://onevoca.org/ws/mycollection_get_my_list.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", nil)
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, nil)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error), nil)
                        return
                    }

                    if let _ = resultData["no_data"] as? Bool {
                        result(nil, [])
                        return
                    }
                }
                
                if let arr = jsonData as? Array<NSDictionary> {
                    var dataList: [Post] = []
                    
                    for value in arr {
                        let id = value["id"] as? String ?? ""
                        let uid = value["uid"] as? String ?? ""
                        let uname = value["uname"] as? String ?? ""
                        let content = value["content"] as? String ?? ""
                        let likes = value["likes"] as? Int ?? 0
                        let date = value["date_reg"] as? String ?? "unknown"
                        var tags: [String] = []
                        
                        if let hashtags = value["hashtags"] as? Array<NSDictionary> {
                            for value in hashtags {
                                tags.append(value["value"] as? String ?? "")
                            }
                        }
                        
                        let d = Post(content: content, hashtags: tags, id: id, uid: uid, likes: likes, username: uname, date: date)
                        dataList.append(d)
                    }
                    result(nil, dataList)
                } else {
                    result("json data error.", nil)
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""), nil)
            }
        })
        task.resume()
    }
    
    func upload(result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        dic.updateValue(content, forKey: "content")
        
        if let hashtags = hashtags {
            dic.updateValue(hashtags, forKey: "hashtags")
        }
        
        let urlString = "https://onevoca.org/ws/mycollection_new_add.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)

                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error))
                        return
                    }

                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    }
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""))
            }
        })
        task.resume()
    }
}
