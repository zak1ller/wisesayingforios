//
//  PlaygroundPost.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/12/31.
//

import Foundation

class PlaygroundPost {
    var id: String
    var uid: String
    var uname: String
    var content: String
    var date: String
    var replyCount: Int = 0
    var likesCount: Int = 0
    var isShowAllText = false
    var isLike = false
    
    enum PostMode {
        case create, edit
    }
    
    init(id: String, uid: String, uname: String, content: String, date: String, replyCount: Int, likesCount: Int = 0, isLike: Bool = false) {
        self.id = id
        self.uid = uid
        self.uname = uname
        self.content = content
        self.date = date
        self.replyCount = replyCount
        self.likesCount = likesCount
        self.isLike = isLike
    }
    
    static func likePost(item: PlaygroundPost, isLike: Bool, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        dic.updateValue(item.id, forKey: "pid")
        dic.updateValue(isLike, forKey: "is_like")
        
        let urlString = "https://onevoca.org/ws/playground_like.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error))
                        return
                    }

                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    }
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""))
            }
        })
        task.resume()
    }
    
    static func delete(item: PlaygroundPost, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        dic.updateValue(item.id, forKey: "pid")
        
        let urlString = "https://onevoca.org/ws/playground_delete.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error))
                        return
                    }

                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    }
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""))
            }
        })
        task.resume()
    }
        
    static func post(item: PlaygroundPost?, content: String, result: @escaping (_ error: String?, _ blockString: String?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""), nil)
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        dic.updateValue(content, forKey: "content")
        
        if let item = item {
            dic.updateValue(item.id, forKey: "pid")
            dic.updateValue(true, forKey: "is_edit")
        }
      
        let urlString = "https://onevoca.org/ws/playground_post.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", nil)
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, nil)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)

                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        if error == ServerErrorCode.postBlockString {
                            result(ServerErrorCode.convertErrorCode(errorCode: error), resultData["block_text"] as? String)
                        } else {
                            result(ServerErrorCode.convertErrorCode(errorCode: error), nil)
                        }
                    }

                    if let _ = resultData["success"] as? Bool {
                        result(nil, nil)
                    }
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""), nil)
            }
        })
        task.resume()
    }
    
    static func getSinglePost(pid: String, result: @escaping (_ error: String?, _ data: PlaygroundPost?) -> ()) {
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        dic.updateValue(pid, forKey: "pid")
        
        let urlString = "https://onevoca.org/ws/playground_get_single.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", nil)
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, nil)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error), nil)
                        return
                    }

                    if let value = resultData["list"] as? NSDictionary {
                        let id = value["id"] as? String ?? ""
                        let uid = value["uid"] as? String ?? ""
                        let uname = value["uname"] as? String ?? ""
                        let content = value["content"] as? String ?? ""
                        let date = value["date"] as? String ?? ""
                        let likes = value["likes_count"] as? Int ?? 0
                        let isLike = value["is_like"] as? Bool ?? false
                        result(nil, PlaygroundPost(id: id, uid: uid, uname: uname, content: content, date: date, replyCount: 0, likesCount: likes, isLike: isLike))
                    } else {
                        result(nil, nil)
                    }
                } else {
                    result("Unknown error.", nil)
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""), nil)
            }
        })
        task.resume()
    }
    
    static func getList(page: Int, result: @escaping (_ error: String?, _ data: [PlaygroundPost]?, _ isLsatPage: Bool) -> ()) {
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        dic.updateValue(page, forKey: "page")
        
        let urlString = "https://onevoca.org/ws/playground_get_list.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", nil, true)
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, nil, true)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error), nil, true)
                        return
                    }

                    if let _ = resultData["no_data"] as? Bool {
                        result(nil, [], true)
                        return
                    }
                    
                    if let list = resultData["list"] as? Array<NSDictionary> {
                        var returnList: [PlaygroundPost] = []
                        for value in list {
                            let id = value["id"] as? String ?? ""
                            let uid = value["uid"] as? String ?? ""
                            let uname = value["uname"] as? String ?? ""
                            let content = value["content"] as? String ?? ""
                            let date = value["date"] as? String ?? "unknown"
                            let replyCount = value["reply_count"] as? Int ?? 0
                            let likesCount = value["likes_count"] as? Int ?? 0
                            let isLike = value["is_like"] as? Bool ?? false
                            returnList.append(PlaygroundPost(id: id, uid: uid, uname: uname, content: content, date: date, replyCount: replyCount, likesCount: likesCount , isLike: isLike))
                        }
                        result(nil, returnList, resultData["is_last_page"] as? Bool ?? true)
                    }
                } else {
                    result("Unknown error.", nil, true)
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""), nil, true)
            }
        })
        task.resume()
    }
    
}
