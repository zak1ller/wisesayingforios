//
//  FCM.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2021/01/26.
//

import Foundation
import UIKit

class FCM {
    static func sendLikeNotification(pid: String, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(pid, forKey: "target_post_id")
        
        if let uid = FirebaseManager.shared.getUserData(dataType: .uid) {
            dic.updateValue(uid, forKey: "uid")
        }
        
        let urlString = "https://onevoca.org/ws/fcm_like_notice.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error))
                        return
                    }

                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    }
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""))
            }
        })
        task.resume()
    }
    
    static func registerToken(token: String, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(token, forKey: "token")
        
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            dic.updateValue(uuid, forKey: "uuid")
        }
        
        if let uid = FirebaseManager.shared.getUserData(dataType: .uid) {
            dic.updateValue(uid, forKey: "uid")
        }
        
        let urlString = "https://onevoca.org/ws/fcm_update_token.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error))
                        return
                    }

                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    }
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""))
            }
        })
        task.resume()
    }
}
