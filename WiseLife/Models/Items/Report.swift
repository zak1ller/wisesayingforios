//
//  Report.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/12/08.
//

import Foundation

class Report {
    enum ReportGroup: String {
        case reply = "reply"
        case post = "post"
        case user = "user"
    }
    
    enum ReportContent: Int {
        case OffensiveComment = 1
        case AbusiveComment = 2
        case SensationalComment = 3
        case RepetitiveComment = 4
        case OffensivePost = 5
        case AbusivePost = 6
        case SensationalPost = 7
        case NoSourcePost = 8
        case SensationalUser = 9
        case NoSourcePostUser = 10
        case DirectFeedback = 11
        case InappropriateUsername = 12
    }
    
    static func send(pid: String?, cid: String?, tuid : String?, group: ReportGroup, content: ReportContent, directMessage: String? = nil, result: @escaping (_ error: String?) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""))
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        dic.updateValue(group.rawValue, forKey: "group")
        dic.updateValue(content.rawValue, forKey: "content")
        
        if let pid = pid {
            dic.updateValue(pid, forKey: "pid")
        }
        
        if let cid = cid {
            dic.updateValue(cid, forKey: "cid")
        }
        
        if let tuid = tuid {
            dic.updateValue(tuid, forKey: "tuid")
        }
        
        if let directMessage = directMessage {
            dic.updateValue(directMessage, forKey: "direct_message")
        }
    
        let urlString = "https://onevoca.org/ws/report.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)

                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }

                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error))
                        return
                    }

                    if let _ = resultData["success"] as? Bool {
                        result(nil)
                    }
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""))
            }
        })
        task.resume()
    }
}
