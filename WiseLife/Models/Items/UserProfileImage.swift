//
//  UserImage.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/12/19.
//

import Foundation
import RealmSwift

class UserProfileImage: Object {
    @objc dynamic var uid = ""
    @objc dynamic var picture: Data?
    @objc dynamic var lastUpdate: String?
    
    enum UserProfileImageError {
        case dbError
        case succcess
        case foundNot
        case found
    }
    
    static func checkDbSize() {
        do {
            let realm = try Realm()
            let size = realm.objects(UserProfileImage.self).count
        } catch {
            
        }
    }
    
    func updateCacheData() -> UserProfileImageError? {
        do {
            let realm = try Realm()
            let predicate = NSPredicate(format: "uid = %@", uid)
            let data = realm.objects(UserProfileImage.self).filter(predicate)
            if data.count != 0 {
                try realm.write({
                    realm.delete(data)
                })
            }
            
            try realm.write({
                realm.add(self)
            })
            
            return .succcess
        } catch {
            return .dbError
        }
    }
    
    static func deleteCacheData() {
        print("캐시 데이터를 제거합니다.")
        guard let uid = FirebaseManager.shared.getUserData(dataType: .uid) else {
            return
        }
        
        do {
            let realm = try Realm()
            let predicate = NSPredicate(format: "uid = %@", uid)
            let data = realm.objects(UserProfileImage.self).filter(predicate)
            if data.count != 0 {
                try realm.write({
                    realm.delete(data)
                })
                print("캐시 데이터를 제거했습니다.")
            } else {
                print("삭제할 캐시 데이터가 없습니다.")
            }
        } catch {
            print("캐시 데이터 제거에 실패했습니다.")
        }
    }
    
    static func findCacheData(uid: String, lastUpdateDate: String) -> UserProfileImage? {
        do {
            let realm = try Realm()
            let predicate = NSPredicate(format: "uid = %@ AND lastUpdate = %@", uid, lastUpdateDate)
            let data = realm.objects(UserProfileImage.self).filter(predicate)
            if data.count == 0 {
                return nil
            } else {
                return data.first
            }
        } catch {
            return nil
        }
    }
    
    static func downloadProfileImage(uid: String, result: @escaping (_ error: String?, _ image: UIImage?) -> ()) {
        User.getProfileImageLastUpdate(uid: uid, result: {
            error, date in
            if let error = error {
                result(error, nil)
                return
            }
            
            if let date = date {
                if let cache = findCacheData(uid: uid, lastUpdateDate: date) {
                    // 캐시 데이터가 있을 경우 캐시 데이터를 가져온다
                    DispatchQueue.global().async(execute: {
                        UserProfileImage.checkDbSize()
                    })
                    
                    if let imageData = cache.picture {
                        result(nil, UIImage(data: imageData))
                    } else {
                        result(nil ,nil)
                    }
                } else {
                    // 캐시 데이터가 없을 경우 서버에서 가져온다.
                    User.getProfileImage(uid: uid, result: {
                        error, image in
                        if let error = error {
                            result(error, nil)
                        } else {
                            // 서버에서 가져온 정보를 캐시 데이터에 등록한다.
                            let cData = UserProfileImage()
                            cData.uid = uid
                            cData.lastUpdate = date
                            cData.picture = image?.jpegData(compressionQuality: 1)
                            
                            var error: String?
                            
                            switch cData.updateCacheData() {
                            case .succcess:
                                error = nil
                            case .dbError:
                                error = "db error."
                            default:
                                break
                            }
                            
                            if let error = error {
                                result(error, nil)
                            } else {
                                result(nil, image)
                            }
                        }
                    })
                }
            } else {
                result(nil, nil)
            }
        })
    }
}
