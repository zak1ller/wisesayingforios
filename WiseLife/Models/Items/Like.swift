//
//  Like.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2021/01/25.
//

import Foundation

class Like {
    var uid: String
    var tuid: String
    var pid: String
    var uname: String
    var date: String
    var isRead: Int
    
    init(uid: String, tuid: String, pid: String, uname: String, date: String, isRead: Int) {
        self.uid = uid
        self.tuid = tuid
        self.pid = pid
        self.uname = uname
        self.date = date
        self.isRead = isRead
    }
    
    static func getLikes(page: Int, result: @escaping (_ error: String?, _ list: [Like]?, _ isLastPage: Bool) -> ()) {
        if FirebaseManager.shared.getUserData(dataType: .uid) == nil {
            result(NSLocalizedString("UidErrorMessage", comment: ""), nil, true)
            return
        }
        
        var dic: [String: Any] = [:]
        dic.updateValue(FirebaseManager.shared.getUserData(dataType: .uid) ?? "", forKey: "uid")
        dic.updateValue(page, forKey: "page")
        
        let urlString = "https://onevoca.org/ws/playground_get_like_list.php"
        let encodedURL = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encodedURL!)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        
        do {
            let json_data = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            request.httpBody = json_data
        } catch  {
            result("data error", nil, true)
            return
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: {
            data, response, error in
            if let error = error {
                result(error.localizedDescription, nil, true)
                return
            }

            do {
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let resultData = jsonData as? NSDictionary {
                    if let query = resultData["query"] as? String {
                        print("Query error: \(query)")
                    }
                    
                    if let error = resultData["error"] as? Int {
                        result(ServerErrorCode.convertErrorCode(errorCode: error), nil, true)
                        return
                    }

                    if let _ = resultData["no_data"] as? Bool {
                        result(nil, [], true)
                        return
                    }
                    
                    if let list = resultData["list"] as? Array<NSDictionary> {
                        var returnList: [Like] = []
                        for value in list {
                            let uid = value["uid"] as? String ?? ""
                            let tuid = value["tuid"] as? String ?? ""
                            let pid = value["pid"] as? String ?? ""
                            let uname = value["username"] as? String ?? ""
                            let date = value["date"] as? String ?? ""
                            let isRead = value["is_read"] as? Int ?? 0
                            returnList.append(Like(uid: uid, tuid: tuid, pid: pid, uname: uname, date: date, isRead: isRead))
                        }
                        result(nil, returnList, resultData["is_last_page"] as? Bool ?? true)
                    }
                }
            } catch {
                result(NSLocalizedString("NetworkErrorMessage", comment: ""), nil, true)
            }
        })
        task.resume()
    }
}
