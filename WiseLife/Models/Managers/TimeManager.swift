//
//  TimeManager.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/28.
//

import Foundation
import UIKit

class TimeManager {
    static func getCurrentMilliseconds() -> Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    
    static func makePastTimeString(str: String) -> String{
        let dateFormat = DateFormatter()

        dateFormat.dateFormat = "YYYY-MM-dd HH:mm:ss"
        if let date = dateFormat.date(from: str) {
            return getPastTimeString(date: date)
        }

        dateFormat.dateFormat = "YYYY-MM-dd_HH:mm:ss"
        if let date = dateFormat.date(from: str) {
            return getPastTimeString(date: date)

        }

        dateFormat.dateFormat = "yyyy-MM-dd a h:mm:ss"
        if let date = dateFormat.date(from: str) {
            return getPastTimeString(date: date)

        }

        dateFormat.dateFormat = "yyyy-MM-dd a hh:mm:"
        if let date = dateFormat.date(from: str) {
            return getPastTimeString(date: date)

        }

        return str
    }
    
    static func getPastTimeString(date: Date) -> String {
        var str = ""
        let interval = date.timeIntervalSinceNow
        let res = abs(Int(interval))

        if interval > -60 {
            if res == 1 {
                str = NSLocalizedString("JustBefore", comment: "")
            } else {
                str = NSLocalizedString("JustBefore", comment: "")
            }
        } else if interval > -3600 {
            if res/60 == 1 {
                str = "\(res/60)\(NSLocalizedString("MinuteAgo", comment: ""))"
            } else {
                str = "\(res/60)\(NSLocalizedString("MinutesAgo", comment: ""))"
            }
        } else if interval > -86400 {
            if res/3600 == 1 {
                str = "\(res/3600)\(NSLocalizedString("HourAgo", comment: ""))"
            } else {
                str = "\(res/3600)\(NSLocalizedString("HourAgo", comment: ""))"
            }
        } else if interval > -(86400*7) {
            if res/86400 == 1 {
                str = "\(res/86400)\(NSLocalizedString("DayAgo", comment: ""))"
            } else {
                str = "\(res/86400)\(NSLocalizedString("DayAgo", comment: ""))"
            }
        } else {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "YYYY. MM. dd"
            str = dateFormatter.string(from: date)
        }
        return str
    }
}
