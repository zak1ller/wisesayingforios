//
//  KeyManager.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/21.
//

import Foundation

class KeyManager {
//    private let keyPlaygroundListUpdate = "playgroundListUpdate"
//    var playgroundListUpdate: Bool {
//        get {
//            return UserDefaults.standard.bool(forKey: keyPlaygroundListUpdate)
//        } set(v) {
//            UserDefaults.standard.setValue(v, forKey: keyPlaygroundListUpdate)
//        }
//    }
    
    private let keyUpdateUsername = "updateUsername"
    var isUpdateUsername: Bool {
        get {
            return UserDefaults.standard.bool(forKey: keyUpdateUsername)
        } set(v) {
            UserDefaults.standard.setValue(v, forKey: keyUpdateUsername)
        }
    }
}
