//
//  StringManager.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/09.
//

import Foundation

class StringManager {
    static func removeMultipleLine(text: String) -> String {
        var v = text
        var i = 50
        
        while i >= 0 {
            var j = i
            var s = "\n\n\n"
            while j > 1 {
                v = v.replacingOccurrences(of: s, with: "\n")
                s += "\n"
                j -= 1
            }
            i -= 1
        }
        return v
    }
    
    static func removeDoubleLine(text: String) -> String {
        var v = text
        var i = 50
        
        while i >= 0 {
            var j = i
            var s = "\n\n"
            while j > 1 {
                v = v.replacingOccurrences(of: s, with: "\n")
                s += "\n"
                j -= 1
            }
            i -= 1
        }
        return v
    }
    
    static func getMentionUsers(text: String) -> [String]? {
        let mentionUsers = text.getArrayAfterRegex(regex: "@[^ ]+")
        if mentionUsers.count != 0 {
            return mentionUsers
        } else {
            return nil
        }
    }
    
    static func makeHashtagString(hashtags: [String]) -> String {
        var str = ""
        var i = 0
        for value in hashtags {
            if i == hashtags.count-1 {
                str += "#\(value)"
            } else {
                str += "#\(value) "
            }
            i += 1
        }
        return str
    }
    
    static func findString(str: String, findStr: String) -> Bool {
        let range = str.range(of: findStr)
        if range == nil {
            return false
        }
        if range!.isEmpty {
            return false
        } else{
            return true
        }
    }
    
    static func remove_space(text: String) -> String {
        return String(text.filter { !" ".contains($0) })
    }
    
    static func remove_newlines(text: String) -> String {
        return String(text.filter { !"\n\t\r".contains($0) })
    }
}

extension String{
    func getArrayAfterRegex(regex: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: self,
                                        range: NSRange(self.startIndex..., in: self))
            return results.map {
                String(self[Range($0.range, in: self)!])
            }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
}
