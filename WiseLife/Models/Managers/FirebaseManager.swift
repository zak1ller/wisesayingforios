//
//  FirebaseManager.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/09.
//

import Foundation
import Firebase
import FirebaseAuth

class FirebaseManager {
    enum UserDataType {
        case uid
        case name
        case email
        case provider
    }
    
    enum SignInState {
        case success
        case failure
        case noauth
    }
    
    enum CheckSignInResult {
        case needSignIn
        case noProblum
    }
    
    static let shared = FirebaseManager()
    
    func checkSignInState(result: @escaping (_ error: String?, _ res: CheckSignInResult?) -> ()) {
        if FirebaseManager.shared.isSignedIn() == .failure {
            result(nil, .needSignIn)
            return
        }
                
        // 중간에 어떠한 의도하지 않은 이유로 인해 비밀번호가 설정되지 않은 계정일 경우(또는 비밀번호 설정 전에 단계가 종료된경우 삭제함.
        FirebaseManager.shared.deleteUserForPassword(password: "wswsws", result: {
            error, isDelete  in
            if let error = error {
                if error == NSLocalizedString("DidNotMatchPasswordErrorMessage", comment: "") {
                    result(nil, .noProblum)
                } else if error == NSLocalizedString("TooManyRequestErrorMessage", comment: "") {
                    result(nil, .noProblum)
                } else {
                    result(error, nil)
                }
            } else {
                if isDelete {
                    result(nil, .needSignIn)
                } else {
                    result(nil, .noProblum)
                }
            }
        })
    }
    
    func createEmailAccount(email: String, password: String, result: @escaping (_ error: String?) -> ()) {
        Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
            if let error = error {
                result(FirebaseErrorMessage.convertErorMessage(error: error as NSError))
            } else {
                result(nil)
            }
        }
    }
    
    func signIn(email: String, password: String, result: @escaping (_ error: String?) -> ()) {
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if let error = error {
                result(FirebaseErrorMessage.convertErorMessage(error: error as NSError))
            } else {
                result(nil)
            }
        }
    }
    
    func setPassword(password: String, result: @escaping (_ error: String?) -> ()) {
        if let user = Auth.auth().currentUser {
            user.updatePassword(to: password, completion: {
                error in
                if let error = error {
                    result(FirebaseErrorMessage.convertErorMessage(error: error as NSError))
                } else {
                    result(nil)
                }
            })
        } else {
            result("Unknown error.")
        }
    }
    
    func signOut() -> String? {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            return nil
        } catch let signOutError as NSError {
            return signOutError.localizedDescription
        }
    }
    
    func sendAuthEmail(result: @escaping (_ error: String?) -> ()) {
        Auth.auth().languageCode = Locale.current.languageCode
        Auth.auth().currentUser?.sendEmailVerification { (error) in
            if let error = error {
                result(FirebaseErrorMessage.convertErorMessage(error: error as NSError))
            } else {
                result(nil)
            }
        }
    }
    
    func sendUpdatePasswordEmail(email: String, result: @escaping (_ error: String?) -> ()) {
        Auth.auth().sendPasswordReset(withEmail: email, completion: {
            error in
            if let error = error {
                result(FirebaseErrorMessage.convertErorMessage(error: error as NSError))
            } else {
                result(nil)
            }
        })
    }
    
    func isSignedIn() -> SignInState {
        if let _ = Auth.auth().currentUser {
            if isVerificEmail() {
                return .success
            } else {
                return .noauth
            }
        } else {
            return .failure
        }
    }
    
    func reload(result: @escaping (_ error: String?) -> ()) {
        if let user = Auth.auth().currentUser {
            user.reload(completion: {
                error in
                if let error = error {
                    result(error.localizedDescription)
                } else {
                    result(nil)
                }
            })
        } else {
            result(nil)
        }
    }
    
    func isVerificEmail() -> Bool {
        if getUserData(dataType: .provider) == "password" {
            return Auth.auth().currentUser?.isEmailVerified ?? false
        } else {
            return true
        }
    }
    
    func getUserData(dataType: UserDataType) -> String? {
        if let user = Auth.auth().currentUser {
            switch dataType {
            case .uid:
                return user.providerData[0].uid
            case .name:
                return user.displayName
            case .email:
                return user.providerData[0].email
            case .provider:
                var provider = ""
                for value in user.providerData {
                    provider = value.providerID
                }
                return provider
            }
        } else {
            return nil
        }
    }
    
    func deleteUserForPassword(password: String, result: @escaping (_ error: String?, _ isDelete: Bool) -> ()) {
        if let user = Auth.auth().currentUser {
            if getUserData(dataType: .provider) != "password" {
                result(nil, false)
                return
            }
            
            let credential = EmailAuthProvider.credential(withEmail: getUserData(dataType: .email) ?? "", password: password)
            
            user.reauthenticate(with: credential, completion: {
                res, error in
                if let error = error {
                    result(FirebaseErrorMessage.convertErorMessage(error: error as NSError), false)
                } else {
                    user.delete(completion: {
                        error in
                        if let error = error {
                            result(FirebaseErrorMessage.convertErorMessage(error: error as NSError), false)
                        } else {
                            result(nil, true)
                        }
                    })
                }
            })
        } else {
            result(NSLocalizedString("UidErrorMessage", comment: ""), false)
        }
    }
}

class FirebaseErrorMessage {
    static func convertErorMessage(error: NSError) -> String {
        var message: String

        switch AuthErrorCode(rawValue: error.code) {
        case .networkError:
            message = NSLocalizedString("NetworkErrorMessage", comment: "")
        case .userNotFound:
            message = NSLocalizedString("DidNotMatchPasswordErrorMessage", comment: "")
        case .tooManyRequests:
            message = NSLocalizedString("TooManyRequestErrorMessage", comment: "")
        case .invalidEmail:
            message = NSLocalizedString("InvaildEmailErrorMessage", comment: "")
        case .wrongPassword:
            message = NSLocalizedString("DidNotMatchPasswordErrorMessage", comment: "")
        case .emailAlreadyInUse:
            message = NSLocalizedString("EmailAlreadyUseErrorMessage", comment: "")
        case .weakPassword:
            message = NSLocalizedString("WeakPasswordErrorMessage", comment: "")
        case .userDisabled:
            message = NSLocalizedString("DisabledUserMessage", comment: "")
        default:
            message = error.localizedDescription
        }
        return message
    }
}
