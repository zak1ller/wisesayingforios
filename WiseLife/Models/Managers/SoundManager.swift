//
//  SoundManager.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/25.
//

import Foundation
import UIKit

class SoundManager {
    static func vibrate(intensity: UIImpactFeedbackGenerator.FeedbackStyle) {
        let lightImpactFeedbackGenerator = UIImpactFeedbackGenerator(style: intensity)
        lightImpactFeedbackGenerator.impactOccurred()
    }
}
