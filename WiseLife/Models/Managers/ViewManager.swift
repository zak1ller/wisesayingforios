//
//  ViewManager.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/11.
//

import Foundation
import UIKit

class ViewManager {
    enum UnderLineSize {
        case shallow, normal, bold
    }
    
    static func actionSheetForiPad() -> UIAlertController.Style {
        if StringManager.findString(str: UIDevice.current.modelName, findStr: "iPad") || StringManager.findString(str: UIDevice.current.modelName, findStr: "Simulator") {
            return .alert
        } else {
            return .actionSheet
        }
    }
    
    static func appendTopLine(toView: UIView, backgroundView: UIView, size: UnderLineSize, color: UIColor = Colors.shared.line, space: CGFloat = 0) {
        let line = UIView()
        line.translatesAutoresizingMaskIntoConstraints = false
        line.backgroundColor = color
        backgroundView.addSubview(line)
        
        let heightValue: CGFloat
        
        switch size {
        case .shallow:
            heightValue = 0.5
        case .normal:
            heightValue = 0.75
        case .bold:
            heightValue = 1
        }
        
        let height = NSLayoutConstraint(item: line, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: heightValue)
        let leading = NSLayoutConstraint(item: line, attribute: .leading, relatedBy: .equal, toItem: toView, attribute: .leading, multiplier: 1, constant: space)
        let trailing = NSLayoutConstraint(item: line, attribute: .trailing, relatedBy: .equal, toItem: toView, attribute: .trailing, multiplier: 1, constant: -space)
        let top = NSLayoutConstraint(item: line, attribute: .top, relatedBy: .equal, toItem: toView, attribute: .top, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([height, leading, trailing, top])
    }
    
    static func appendUnderLine(toView: UIView, backgroundView: UIView, size: UnderLineSize, color: UIColor = Colors.shared.line, leading: CGFloat = 0, trailing: CGFloat = 0) {
        let line = UIView()
        line.translatesAutoresizingMaskIntoConstraints = false
        line.backgroundColor = color
        backgroundView.addSubview(line)
        
        let heightValue: CGFloat
        
        switch size {
        case .shallow:
            heightValue = 0.5
        case .normal:
            heightValue = 0.75
        case .bold:
            heightValue = 1
        }
        
        let height = NSLayoutConstraint(item: line, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: heightValue)
        let leading = NSLayoutConstraint(item: line, attribute: .leading, relatedBy: .equal, toItem: toView, attribute: .leading, multiplier: 1, constant: leading)
        let trailing = NSLayoutConstraint(item: line, attribute: .trailing, relatedBy: .equal, toItem: toView, attribute: .trailing, multiplier: 1, constant: -trailing)
        let bottom = NSLayoutConstraint(item: line, attribute: .bottom, relatedBy: .equal, toItem: toView, attribute: .bottom, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([height, leading, trailing, bottom])
    }
}
