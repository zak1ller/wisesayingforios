//
//  AppDelegate.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/07.
//

import Foundation
import UIKit
import Firebase
import FirebaseMessaging

@main
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    var window: UIWindow?
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

        completionHandler([.alert, .badge, .sound])
    }

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        // 토큰 정보가 업데이트 될 때 (토큰은 기기당 primary)
        let dataDict:[String: String] = ["token": fcmToken ?? ""]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        
        // 토큰 정보가 업데이트 될 때마다 서버에 업데이트 한다.
        DispatchQueue.global().async(execute: {
            FCM.registerToken(token: fcmToken ?? "", result: {
                error in
                if let error = error {
                    print("FCM Error (Register token): \(error)")
                }
            })
        })
    }
  
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        guard let data: [String: Any] = userInfo as? [String: Any] else {
            return
        }
        
        let pid = data["pid"] as? String ?? ""
        let type = data["type"] as? String ?? ""
        if type == "playground_like" {
            let vc = SinglePlaygroundViewController()
            vc.modalPresentationStyle = .fullScreen
            vc.pid = pid
            
            if let window = self.window, let rootViewController = window.rootViewController {
                var currentController = rootViewController
                while let presentedController = currentController.presentedViewController {
                    currentController = presentedController
                }
                currentController.present(vc, animated: true, completion: nil)
            } else {
                print(222)
            }
        }
        
        completionHandler()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // 앱이 백그라운드에있는 동안 알림 메시지를 받으면
        // 이 콜백은 사용자가 애플리케이션을 시작하는 알림을 탭할 때까지 실행되지 않습니다.
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // APN 토큰과 등록 토큰 매핑
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // 파이어베이스 세팅
        FirebaseApp.configure()
        
        // 유저 정보 서버에 업데이트
        User.signinUpdate(result: {
            error in
            if let error = error {
                print("Use, Sign In Update Error Message: \(error)")
            }
        })
        
        // FCM 원격 알림 등록
        UNUserNotificationCenter.current().delegate = self
    
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization( options: authOptions,completionHandler: {_, _ in
            
        })
        
        application.registerForRemoteNotifications()
        
        // FCM 토큰 등록
        Messaging.messaging().delegate = self
        Messaging.messaging().token(completion: { token, error in
            if let error = error {
                print("Error fetching FCM registration token: \(error)")
            }
        })
        return true
    }
}

