//
//  Colors.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/07.
//

import Foundation
import UIKit

class Colors {
    static let shared = Colors()
    
    private var vc: UIViewController?
    
    var background: UIColor {
        if isDarkMode() {
            return UIColor(red: 22/255, green: 22/255, blue: 22/255, alpha: 1)
        } else {
            return UIColor(hexString: "#f2f2f6")
        }
    }
    
    var backgroundSub: UIColor {
        if isDarkMode() {
            return UIColor(hexString: "#202124")
        } else {
            return UIColor.white
        }
    }
    
    var text: UIColor {
        if isDarkMode() {
            return UIColor.white
        } else {
            return UIColor.black
        }
    }
    
    var textSub: UIColor {
        if isDarkMode() {
            return UIColor(hexString: "#939395")
        } else {
            return UIColor(hexString: "#939395")
        }
    }
    
    var invisible: UIColor {
        if isDarkMode() {
            return UIColor(hexString: "#616161")
        } else {
            return UIColor(hexString: "#9e9e9e")
        }
    }
    
    var point: UIColor {
        if isDarkMode() {
            return UIColor(hexString: "#3C8FF7")
        } else {
            return UIColor(hexString: "#007AFF")
        }
    }
    
    var pointSub: UIColor {
        if isDarkMode() {
            return UIColor(hexString: "#599dc9")
        } else {
            return UIColor(hexString: "#599dc9")
        }
    }
    
    var line: UIColor {
        if isDarkMode() {
            return UIColor(hexString: "#424242")
        } else {
            return UIColor(hexString: "#E8E8E8")
        }
    }
    
    var border: UIColor {
        if isDarkMode() {
            return UIColor.lightGray
        } else {
            return UIColor(hexString: "#D7DBDD")
        }
    }
    
    var backgroundAlpha: UIColor {
        return UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5)
    }
    
    func setVc(vc: UIViewController) {
        self.vc = vc
    }
    
    private func isDarkMode() -> Bool {
        if #available(iOS 13, *) {
            if let vc = vc {
                return vc.traitCollection.userInterfaceStyle == .dark
            } else {
                return false
            }
        } else {
            return false
        }
    }
}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}

