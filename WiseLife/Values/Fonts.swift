//
//  Fonts.swift
//  WiseLife
//
//  Created by Min-Su Kim on 2020/11/07.
//

import Foundation
import UIKit

class Fonts {
    static let normal = UIFont.systemFont(ofSize: 16, weight: .medium)
    static let button = UIFont.systemFont(ofSize: 14, weight: .bold)
}
